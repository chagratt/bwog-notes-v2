+++
Author = "Chagratt"
title = "Les groupes"
date = 2020-02-25T20:00:00+02:00
draft = false
show_toc = true
+++

Sur cette page vous trouverez d'abord la liste de « mes » groupes,
puis la liste de tous ceux pour lequels il y a un article sur ce site.

## « Mes » groupes

Derrière ce titre quasi piège à clics se cache une démarche personnelle.
C'est pour avoir ma liste de groupes facilement accessible[^1].
J'ai aussi la flemme de me créer une BDD ou d'utiliser une appli pour ça.
Et si ça peut faire découvrir des noms au personnes qui passent ici, c'est cool !

_Remarque_ : S'il y a un lien sur un nom de groupe, celui-ci  vous mènera à la liste des articles à son sujet.

{{< groupes >}}

Les plus patient·e·s auront remarqué qu'il n'y a pas que du pur métal ici. :p

## Tous les groupes du site

Pour ne pas être vache, vous trouverez ci-dessous la liste de tous les groupes apparaissant dans au moins un article.

{{< tous_les_groupes >}}

[^1]: Elle n'est pas forcément bien tenue à jour cela dit...

