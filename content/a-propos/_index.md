+++
Author = "Chagratt"
title = "À propos"
date = 2019-10-02T14:52:09+02:00
draft = false
+++

Ce petit blog me sert de bloc notes en ligne sur l'administration de systèmes Lignux, en principe.

J'y partage également des rétrospectives de concerts Métal,
des groupes qu’on me conseille et que je découvre,
mon avis sur des CD de groupes que je connais depuis longtemps,
et mes pérégrinations à la basse (ça c’est pour moi, c’est cadeau !).
Le tout sans prétention, et avec humour, si possible.

Le rythme de publication est totalement aléatoire,
et le type des sujets n'est pas figé dans le marbre.

Notez que toutes les images sont cliquables, afin d'avoir la version en taille réelle au besoin.

_Attention_ : Si vous venez pour de l'UNIX et que je n'en fais pas une mention explicite dans un article,
je n'ai pas testé.
Donc fuyez et tournez-vous vers le manuel ou une autre source d'aide.

## L'envers du décor

Ce blog est généré avec Hugo, un générateur de sites statiques en Go.
Le tout mis en page par un thème fait main, écrit en Markdown et versionné à l'aide de Git.
Les sources de mon thème ainsi que des articles sont librement accessibles.

Dans la version d'avant, ce blog était généré avec Pelican, un autre générateur, mais écrit en python.
Pour plus de détails sur les raisons du changement, et quelques observations bonus vous pouvez [lire cet article]({{< ref "migration-pelican-hugo.md">}})

- [Les sources de mon thème](https://framagit.org/chagratt/hugo-themes/-/tree/master/little-dreamer)
- [Les sources des articles](https://framagit.org/chagratt/chagratt-site)
- [Le site de Hugo](https://gohugo.io/)
- [Le site de Pelican](https://blog.getpelican.com/)

## Lignux ?

Je manipule au quotidien des systèmes basés sur le noyau Linux.
Souvent il y a les outils GNU avec, mais parfois non.

Il s'agit juste d'une petite blague envers le fameux « On dit GNU/Linux » qui est trop souvent pris au sérieux,
et qui n'est pas systématique.

## Vie privée

Ici, vous n'êtes pas traqués.
Le blog n'est pas — et ne sera jamais — monétisé.
Il fonctionne sans cookie ni javascript.

Mais si ce dernier est activé, vous pourrez profiter de la page de recherche (qui fonctionne en local sur votre navigateur),
et des commentaires sur certains articles.
La seule connexion vers l'extérieur concerne les commentaires, qui sont chargés à la demande (en cliquant sur un bouton).
Cette action volontairement manuelle interrogera ma propre instance Pleroma.

Plus de détails sur les commentaires sont [disponibles à cet endroit]({{< ref "003-commentaires-via-fedi.md">}}).

Les seules données récupérées le sont par le serveur web et comprennent la date, l’heure, l’adresse IP, la page demandée et la signature de votre navigateur.
Ces données n'iront jamais plus loin que les logs, et ne seront jamais diffusées ni vendues.
Je les consulte juste de temps à autre via GoAccess.

## Partage

Comme indiqué sur chaque page,
le contenu de ce blog, sauf mention contraire,
est sous licence [Creative Commons BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

## Contact

Des questions, remarques ou suggestion ?
En discuter par mail est une bonne solution : chagratt chez chagratt point site (mon pseudo @ ce domaine),
même si je risque de mettre du temps à répondre.

Vous pouvez également me retrouver par là :

- [Le fediverse]({{< param pleroma_profile >}}) (J'y suis plus réactif que pour les mails)
- [IRC sur Geeknode](ircs://irc.geeknode.org) (Des fois, mais pas tout le temps. Mais c'est cool IRC ! Viendez-y !)
- [Framagit](https://git.framasoft.org/chagratt)

## Autres petites choses

Pour le code couleur des barres de progression à la basse je me suis fixé les règles suivantes :

* Vert : {{< progressbar type="success" value="70" >}} Pas/plus de difficultés particulières sur ce morceau.
* Orange : {{< progressbar type="warning" value="60" >}} Il y a quelques passages qui me donnent du mal techniquement, mais globalement ça ne me gêne pas dans la progression.
* Rouge : {{< progressbar type="danger" value="50" >}} J'en chie sur ce morceau qui est potentiellement trop technique pour moi.

Le remplissage de la barre correspond, pifométriquement, à l'avancement de mon apprentissage des plans et de leur enchainement.
Si ça semble trop abscons / obscur, je suis preneur de suggestion pour améliorer ça. :)

