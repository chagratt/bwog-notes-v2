+++
Author = "Chagratt"
categories = ["basse"]
title = "Septembre À La Basse"
date = 2019-09-12T14:28:47+02:00
draft = false
+++

Et si je me mettais à faire un article sur l'avancement par mois ?
Je ne sais pas pourquoi je pose la question comme ça, puisque l'objectif est bel et bien de m'y tenir !

Alors, productif ce mois d'aout, ou pas ?

<!--more-->

## Depuis 1 mois ##

Bah ... pas autant de temps que je l'espérais !
Mois d'aout oblige, plein de choses mieux à faire que s'enfermer chez soi, des sorties à faire, des gens à voir, des bières à voir (#schblam !), etc ...
En bref, pas la défaite ... mais peu d'évolution. Heureusement, ça n'a pas régressé non plus.

## Les morceaux ##

La liste n'a pas changé, ce sont toujours les 3 mêmes :

* Old Man : {{< progressbar type="success" value="100" >}} Toujours quelques petits pains qui trainent. Mais bon, ça pourrait être pire.
* Little Dreamer : {{< progressbar type="success" value="96" >}} Plus de difficultés majeures. Mais il me reste encore un peu d'efforts à fournir pour reternir l'enchainement.
* Heathen Horde : {{< progressbar type="warning" value="65" >}} J'en chie toujours, mais un peu moins que le mois dernier, et ça progresse un peu.

Peu de choses à dire, mais en résumé : pas assez de temps alloué à la basse ! Petite "erreur" par rapport à l'envie de progresser.
Mais bon, en été, il y a des choix à faire, et je ne regrette rien (mon foie lui par contre ....) !

A la prochaine !

