+++
categories = ["basse"]
title = "Recap Basse d'Octobre"
date = 2019-10-06T09:57:25+02:00
draft = false
+++

C'est l'heure de l'exercice mensuel de récap' à la basse !
Alors, que s'est il passé depuis le mois dernier ?
Indice, chez vous : c'est pas pire ! Mais en vrai, c'est à peine mieux.

<!--more-->

## Le mois de Septembre ##

C'est un beau mois, il fait moins chaud, les gens rentrent de vacances, on a encore la forme pour faire autre chose, et aussi,
il y a des potes qui tombent malades.
Ainsi que certains membres du groupe.
Difficile de leur en vouloir, ce sont des choses qui arrivent, mais du coup, quand les répètes sautent, forcément de mon côté,
la progression s'en ressent.

Je n'ai pas régressé, c'est déjà ça, j'ai même un tout petit peu avancé.
Pas des masses, certes, mais c'est toujours mieux que rien.

## Les morceaux ##

On continue toujours avec les 3 mêmes, on ne change pas une équipe qui gagne !

* Old Man : {{< progressbar type="success" value="100" >}} Là, c'est bon, il est enfin plié ! Sauf que je suis un peu crevé, là, ça peut vite devenir la chasse aux canards ! M'enfin comme tout musicien je suppose. En tout cas je suis content et je me fais plaisir quand je le joue.
* Little Dreamer : {{< progressbar type="success" value="100" >}} Et zou ! Un second ! Enfin ! Pareil, je me fais plaisir quand je le joue, surtout quand j'enchaine avec Old Man.
* Heathen Horde : {{< progressbar type="warning" value="80" >}} Réécriture de la tab à l'aide de mon guitariste[^1] pour réorganiser certaines notes sur le manche. Pari réussi, ça rentre bien mieux. Il reste juste un passage technique où je dois placer des hammers-on qui est un peu coton. Mais avec du boulot, ça va le faire.

## Et la suite ? ##

C'est bien beau tout ça, mais que fera-t-on une fois les 3 morceaux pliés ?
Et bien déjà on cherche un 4è à commencer, histoire de ne pas être pris au dépourvu.
Ensuite on commence à chercher un local, parce que c'est bien mignon de répéter dans le home studio,
mais avec rarement la chanteuse et jamais le batteur (on a pas de batterie), forcément on se retrouve vite coincé.

Et puis bon, après ça, un 5è et commencer à démarcher des bars pour jouer devant du public ?
Sait-on jamais, ça serait marrant, en tout cas il semble que ça soit un objectif commun et ça c'est plutôt cool !

Allez, je vous laisse tranquille !
A bientôt pour le récap de Novembre !


[^1]: En vrai je voulais le faire moi même, tester et lui montrer, qu'on en discute. Mais ce mec est tellement fou/passionné qu'il a pensé (à raison je me dis maintenant) qu'il allait ré-écrire lui même.
