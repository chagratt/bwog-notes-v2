+++
Categories = ["basse"]
title = "Janvier à la basse"
date = 2020-02-04T07:25:38+01:00
draft = false
+++

Peu d'activité sur le blog en janvier et faut bien admettre que musicalement parlant, c'était pas fou fou non plus.
Quoique, c'était toujours plus qu'en décembre, c'est déjà ça et il y a eu _un peu_ de progression.
En plus d'une reprise sur les chapeaux de roues niveau concerts (vache, voilà que je me mets à faire des accroches maintenant ...).

<!--more-->

Quand je dis un peu, c'est vraiment pas beaucoup, mais une petite progression vaut mieux qu'une stagnation.

## L'avancement sur les morceaux

Depuis le temps que je dis que je dois prendre plus de temps pour progresser, il serait de bon ton de *réellement* le faire !

* Old Man : {{< progressbar type="success" value="100" >}} C'est toujours bon.
* Little Dreamer : {{< progressbar type="success" value="100" >}} Pareil !
* Heathen Horde : {{< progressbar type="success" value="94" >}} Bientôt plié !
* Stand Ablaze : {{< progressbar type="danger" value="15" >}} Ça va demander du boulot mais ça sera marrant !

## Tout ça c'est bien, mais les autres membres ils s'ennuient un peu non ?

C'est sur qu'ils sont plus en avance que moi.
Du coup, on commence à se cherche un 5è morceau.
On se dirige probablement sur du DevilDriver[^1]

Allez, profitez bien et à bientôt !

[^1]: Ce qui me ferait extrêmement plaisir !
