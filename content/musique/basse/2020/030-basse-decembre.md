+++
Categories = ["basse"]
title = "La basse en décembre"
date = 2020-12-31T09:10:02+02:00
draft = false
+++

Après la grosse reprise du mois dernier, cette fois, c'est un peu plus léger, mais c'est toujours là.

Sans pression, un peu tous les deux jours, pour refaire la corne, et continuer de se motiver tranquillement.

<!--more-->

Les gestes sont presque revenus car je ne pousse pas encore trop non plus, mais je m'approche du niveau que j'avais avant.
Je suis donc content.

Je n'ai pas encore touché aux morceaux à côté par contre[^1].

Un peu plus d'intensif à prévoir, mais je ne voulais pas forcer en cette fin d'année.

## La situation après 2 mois de reprise tranquilles

* Old Man : {{< progressbar type="success" value="96" >}} Quelques loupés quand je ne me concentre pas beaucoup.
* Little Dreamer : {{< progressbar type="success" value="96" >}} Pareil.
* Heathen Horde : {{< progressbar type="warning" value="45" >}} Je n'ai pas tellement forcé, mais ça reviendra.
* Stand Ablaze : {{< progressbar type="danger" value="5" >}} Toujours rien, je n'ai pas essayé non plus.


[^1]: Des morceaux de Final Fantasy (dans le désordre) : battle theme du 4, boss theme du 6, boss theme du 7.
