+++
Categories = ["basse"]
title = "La basse en novembre"
date = 2020-12-01T09:10:02+02:00
draft = false
+++

Après de **très nombreux** mois sans jouer, suite à ce qu'on pourrait appeler une déprime musicale,
me voici de nouveau sur la brèche.

Ça fait plaisir de s'y remettre après tant de temps sans y avoir touché.
Par pure envie (manque même) plutôt que par une sorte d'auto obligation que je commençais à ressentir.
Je suis d'ailleurs impressionné par deux choses : la mémoire musicale, et la qualité de mon instrument qui ne s'est pas déréglé d'un poil.
J'ai juste dû la réaccorder un brin.

En avant pour une reprise en fanfare !

<!--more-->

Ou presque, car je me suis vite heurté à deux soucis.
D'abord la fameuse corne qui était partie en totalité.
Soit on la perd facilement (bon 6 mois c'est long en fait), soit elle n'était pas bien faite à la base.
Ensuite, l'entrainement.
Hé oui.
J'ai beau me souvenir de comment jouer certains morceaux, le geste est plus ou moins perdu.

L'objectif est donc de me refaire tranquillement la corne, et de réentrainer les gestes.
Pour ça je me concentre surtout sur Old Man et Little Dreamer, très bien conservés dans ma tête
(le côté impressionnant de la mémoire dont je parlais plus tôt).
J'ai quand même dû relire mes tablatures pour certains passages.

Pour ne pas trop tourner en rond, car avec le groupe on ne va pas pouvoir reprendre les répètes tout de suite,
je me prévois deux ou trois morceaux en mode plaisir personnel.
Des morceaux de Final Fantasy (dans le désordre) :

- battle theme du 4
- boss theme du 6
- boss theme du 7

Normalement, y'a de quoi faire là.

À raison d'une demi-heure voire une heure tous les deux jours (et parfois même quotidiennement), je pense que je tiens un petit rythme de reprise acceptable.
Le tout sans pression, et en laissant les séances augmenter naturellement avec l'endurance et le nombre de morceaux.

## La situation à la reprise

Au niveau des anciens morceaux, ça a régressé.
Mais après un petit mois, je peux tout de même fièrement afficher ceci :

* Old Man : {{< progressbar type="success" value="95" >}} C'est surtout l'endurance qui fait défaut, et quelques loupés mineurs qui vont vite se rattraper.
* Little Dreamer : {{< progressbar type="success" value="95" >}} Pareil !
* Heathen Horde : {{< progressbar type="warning" value="40" >}} J'ai beaucoup perdu, du travail s'impose
* Stand Ablaze : {{< progressbar type="danger" value="5" >}} Il va falloir tout reprendre de zéro.

Cette fois, on y croit ! Ça va durer. :)

