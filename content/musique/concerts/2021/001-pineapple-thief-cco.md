+++
title = "The Pineapple Thief et Alex Henry Foster au CCO (Villeurbanne)"
date = 2021-10-21T18:00:48+02:00
bands = ["Alex Henry Foster", "The Pineapple Thief"]
+++

Après une période peu encline à cette activité qu'est le concert, petit à petit les revoilà.
Pourvu que ça dure !

Et on recommence avec un petit article, pour une petite soirée placée sous le signe du rock Prog'.

<!--more-->

Un petit article donc, histoire de se remettre en jambes, mais aussi parce qu'en vérité, je ne connais pas le Prog',
ni aucun des deux groupes qui sont passés hier soir.

Par contre, beaucoup de gens connaissaient eux, et les fans de la scène et du style musical sont venus en masse.
Avec la jauge abaissée de la salle, cela nous a fait un concert complet, ce qui faisait extrêmement plaisir.
À tout le monde d'ailleurs car la bonne humeur était largement au rendez-vous.
Que ce soit côté public, organisation, et même artistes.

Cette bonne humeur s'est largement ressentie pendant les concerts.
Les musiciens nous ont offert un très beau jeu, et ont rapidement mit une bonne ambiance dans la salle qui était très réceptive.
Les échanges entre la scène et le public étaient chaleureux.

Pour les deux groupes le son était très bien fait et il faut avouer que c'était pas gagné vu la quantité de matériel déployé.
Ça faisait longtemps qu'on en avait pas vu autant sur scène, juste pour deux groupes.
Cependant, il faut rappeler qu'il y avait un paquet de musiciens ce soir, ça joue forcément.
Mais du coup, on a pu profiter du spectacle dans d'excellentes conditions.

Au niveau des morceaux, on avait là de la bonne ambiance, entrainante, et très bien jouée.
Impossible pour moi de parler du contenu des sets par contre, ne connaissant pas les groupes.
Par contre j'ai fait une excellente découverte ce soir-là.

Si vous aimez le rock progressif ou êtes ne serait-ce qu'un peu curieux, je recommande.
Sinon, en fond sonore dans playlist, ça passera crème aussi.

Vivement les prochains concerts !

## Liens

- [The Pineapple Thief (site officiel)](https://www.pineapplethief.com/)
- [Alex Henry Foster (site officiel)](https://alexhenryfoster.com/)

