+++
Categories = ["concerts"]
title = "Swarm, BlightMass et Hysteria au Rock N Eat (Lyon)"
date = 2020-02-03T18:06:53+01:00
draft = false
bands = ["Swarm", "BlightMass", "Hysteria"]
+++

Après un début d'année bien calme, voici l'ouverture de la saison 2020 des concerts !
Et on commence directement à froid par petit concert bien sympa avec du gros son !
Ça s'est passé Samedi 1er Février au Rock N Eat.
Petit concert implique par contre une petite rétrospective.

Spoiler : le son était bon cette fois !
Du death, parfois groovy, souvent brutal !
En avant !

<!--more-->

Au programme de la soirée :
Swarm qui vient tout droit d'Antibes,
BlightMass qui est un groupe Lyonnais avec un chanteur de Floride
et Hysteria, un autre groupe de Lyon qui officie dans le milieu depuis 1996.

Et on dirait que le sous-sol est taillé pour ce genre d'affiche.
Quand on a une formation réduite au strict minium, il y a beaucoup moins de problèmes de concurrence entre les fréquences du son.
Accessoirement, j'étais très proche de la scène cette fois ... ça a du jouer.

Car oui, et c'est le seul point noir de la soirée : l'absence globale de monde.
Le Rock N Eat était très peu rempli.
Bon, faut avouer qu'il y avait de la concurrence (Gloryhammer ... hem ... pas certain que ça impacte en vrai), mais aussi un manque de comm' de la part de la salle.
Dommage pour les groupes, presque tant mieux pour nous finalement, on a pu se secouer
et faire gicler un peu de bière au passage histoire de se la jouer à l'ancienne sans que ça ne gêne trop de monde. 

En tout cas, les groupes ont assuré et nous ont donné tout ce qu'ils avaient.
C'était la dernière date de la tournée, ils étaient épuisés, mais ils étaient là !
Et la sonorisation aussi, précise, percutante !

D'excellentes découvertes pour ma part et un très bon concert aussi.

## Des liens sur les groupes

- [Hysteria](https://www.metal-archives.com/bands/Hysteria/13973)
- [BlightMass](https://www.blightmass.com/)
- [Swarm](https://www.metal-archives.com/bands/Swarm/3540437964)

