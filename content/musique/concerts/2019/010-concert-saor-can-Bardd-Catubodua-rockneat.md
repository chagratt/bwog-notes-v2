+++
categories = ["concerts"]
title = "Saor, Cân Bardd et Catubodua au Rock N Eat (Lyon)"
date = 2019-10-28T18:20:00+02:00
draft = false
bands = ["Saor", "Cân Bardd", "Catubodua"]
+++

Cette fois, c'est du Black / Folk, on est beaucoup plus dans mon registre !
En étant spectateur uniquement, dans un bar à concerts bien plus petit et intimiste que les salles que j'ai l'habitude de fréquenter.

Il y avait pas mal de choses niveau concerts sur Lyon ce dimanche 27 Octobre, mais j'ai choisi cet événement,
car ça faisait longtemps que je voulais voir Saor.
C'était aussi l'occasion de découvrir deux nouveau groupes.

Alors, déçu ou ravi ?

<!--more-->

## Le déroulement de la soirée

En premier se produisait Catubodua, qui vient de Lyon, et propose du Pagan Black Metal.
Le groupe a sorti un EP l'an dernier et semble préparer un album pour bientôt.
J'aurais pu vous en décrire plus, mais l'organisation de la soirée les a fait commencer 20 minutes plus tôt que prévu,
du coup, je n'ai eu droit qu'à 2 chansons et demi.
Pas terrible pour se faire un avis correct.

### Petit aparté

Il faut savoir que le Rock N Eat, même si le bar et la salle sont sympa et très intimistes (on est jamais loin du groupe),
ont un cruel défaut : c'est dans une cave voutée, relativement basse de plafond[^1].
Cette disposition particulière est compliquée pour bien gérer le son qui va vite être déformé dès que ce n'est pas des grosses basses bien grasses.
L'ingé son a fort à faire dans cette salle.

### Retour à Catubodua

Ce problème de son typique de la salle a donc rendu particulier mon premier contact avec ce groupe.
La composition était correcte, le groupe était motivé, énergique, mais il y avait un rendu final étrange.

Il faudra que j'écoute l'EP et leur album (quand il sortira) pour me faire une meilleure opinion du groupe.
Mais même si ce n'est pas le groupe du siècle (pour l'instant, qui sait), ça reste une première partie convaincante.
Et on en a pas tout le temps des comme ça !

## 2è groupe : Cân Bardd

Groupe Suisse proposant un son croisant Folk / Ambiant / Black, Cân Bardd entre sur scène avec un sourire juste aux oreilles
(surtout le batteur), et le gardera tout le long de son set.
Sauf le chanteur, mais lui il a le droit, je vous rappelle qu'il a un micro à malmener !

Le groupe a déjà sorti 2 album en 2 ans, et ça se sent.
Ambiance qui arrive dès les premières notes, et qui ne quitte pas le set.
Un bon équilibre entre les guitares, la batterie, la basse et la voix ... sauf ... pour le chant clair.
Il rendait bien sur les albums, mais était très déformé dans salle, rendant ces passages désagréables.
Vraiment dommage car le groupe était vraiment appréciable, et on se laissait porter facilement.

Petite mention pour le violoniste de Saor qui est venu faire une apparition sur scène pour les accompagner sur une chanson.

Au niveau de l'ambiance générale, la foule se chauffait petit à petit, et pas seulement à cause de la configuration de la salle.

## Saor

Point culminant de la soirée, le groupe nous a porté tout le long de son set.
Des compos maitrisées, un son très correct (pour ce que la salle peut produire) et une foule vraiment réceptive aux chansons.
C'était limite trop court !

Après, et vous avez le droit de dire que je pinaille, mais j'ai trouvé que le groupe avait finalement assez peu d'interactions avec le public.
Il y avait l'habituel « Merci beaucoup ! » mais à peine plus.
Le groupe avait pourtant l'air content d'être là, mais peut-être qu'ils étaient fatigués, ou bien que ça fait partie de leur show,
pour rester dans l'ambiance qu'ils essaient de distiller.
Je ne sais pas.
Mais ça reste un peu dommage de ne pas se sentir un peu plus impliqué dans le concert.

### Mention spéciale

Le violoniste Lyonnais, qui annonce que c'est l'anniversaire d'un des guitaristes.
Toute la salle a commencé à lui souhaiter en chantant, et les musiciens ont suivi, en jouant l'air de « joyeux anniversaire » au débotté.
Sympa !

## Le mot de la fin

Pour moi, un bon concert, car j'ai **enfin** eu l'occasion de voir Saor.
J'ai aussi pu découvrir deux autres groupes franchement sympathiques.
Cependant l'expérience à été, comme souvent au Rock N Eat, entachée par ses problèmes de son.
Dommage, mais heureusement par rapport à la plupart des groupes qui passent, c'est gênant, mais pas rédhibitoire.

## Des liens sur les groupes

- [Saor](https://www.metal-archives.com/bands/Saor/3540373234)
- [Cân Bardd](https://www.metal-archives.com/bands/C%C3%A2n_Bardd/3540438906)
- [Catubodua](https://www.metal-archives.com/bands/Catubodua/3540440256)

## D'autres liens

- [Le Rock N Eat](https://www.rockneat.com/)

[^1]:C'est pas non plus l'horreur, mais j'ai des potes de plus d'1m80 qui se cognent régulièrement la tête à certains endroits, genre aux fumoirs.

