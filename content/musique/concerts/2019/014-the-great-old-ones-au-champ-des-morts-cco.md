+++
categories = ["concerts"]
title = "The Great Old Ones et Au Champ Des Morts au Rock N Eat (Lyon)"
date = 2019-11-27T18:10:12+01:00
draft = false
bands = ["The Great Old Ones", "Au Champ Des Morts"]
+++

Hier, dans les tréfonds du Rock N Eat s'est tenu un concert que j'attendais tout particulièrement.
Malgré ça, quelques appréhensions car cette salle n'est pas réputée pour la qualité sonore.
Surtout à cause de la cave voutée et basse de plafond.

Heureusement, tout n'était pas aussi noir.
Sauf peut être le style musical. :)

<!--more-->

## Au Champ Des Morts

Je n'ai pas pu assister à leur prestation.
J'étais dehors en pleine séance de distribution de flyers.
Mais au vu du remplissage de la salle quand je suis enfin rentré, je dirais d'instinct que ça a beaucoup plu et que c'était donc au moins bien.
On pourra me rétorquer que la quantité de personnes dans le public à ce moment là ne vaut rien, mais j'y reviendrai.

## The Great Old Ones

La lumière s'éteint, le sample d'intro se lance, le groupe débarque sur scène, le public growle et ... le show commence !

Pas comme on pourrait l'entendre au début par contre.
Le son était horrible pendant la première chanson ... ben ouais on est au Rock N Eat et hélas, la cave n'est pas géniale pour ça.
Heureusement, le sondier a fait un excellent boulot.
Il a réussi à faire en sorte que le son soit bon à partir de la 2è / 3è chanson malgré la configuration de la salle !
Passé ces quelques minutes de désarroi, le show pouvait vraiment commencer.
C'était un régal pour les oreilles et l'ambiance au top !

Le groupe distillait son univers sombre au fil du set, accompagné d'un éclairage minimaliste.
Principalement bleu, parfois vert (Cthulhu évidemment !) et rouge sur une des chansons.
Au delà de la lumière et du son, il n'y avait cependant pas réellement de jeu de scène, mais c'est le genre qui veut ça.
Et puis il faut dire que vu la taille réduite de la scène, difficile de bouger à 5 avec la batterie au milieu.

Pendant le concert, je me suis aperçu que la salle était moins remplie que pour le 1er groupe ! AH !
Comme quoi, le nombre d'entrée ne signifie rien.
Où sont passés les gens ?
Aucune idée.
M'enfin au moins, on était pas serrés comme des sardines.
Dommage pour le groupe ... Tant mieux pour moi !

Mention spéciale aux machines à fumée dont on entendait parfaitement bien le déclenchement !

## Le mot de la fin

Si vous êtes fan et pouvez aller les voir près de chez vous, n'hésitez pas !

## Des liens vers les groupes

- [The Great Old Ones](https://www.metal-archives.com/bands/The_Great_Old_Ones/3540343608)
- [Au Champ Des Morts](https://www.metal-archives.com/bands/Au_Champ_des_Morts/3540413393)

