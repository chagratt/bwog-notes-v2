+++
categories = ["concerts"]
title = "Mayhem, Gaahls Wyrd et Gost au CCO (Villeurbanne)"
date = 2019-11-08T12:10:24+01:00
draft = false
bands = ["Mayhem", "Gaahls Wyrd", "Gost"]
+++

La noirceur et l'enfer étaient de rigeur au CCO hier soir, avec de gros noms présents sur scène.
Mayhem, qu'on ne présente plus.
Gaahls Wyrd, un autre projet du mytique Gaahl.
Et Gost, qui n'a rien à voir avec le groupe Suédois, ce n'est pas une faute de frappe.

J'étais personnellement très impatient d'assister à un tel concert et j'étais loin d'être le seul !
La salle était pleine et le concert sold-out. Ce qui fait 500 personnes.

Avec un tel programme, impossible de s'ennuyer !

<!--more-->

Ce n'est pas tous les jours que le CCO est totalement rempli.
Mais il faut dire qu'avec une telle affiche, l'événement allait forcément ramener beaucoup de monde.
Et oulà, il y en avait du monde !

## Avant l'ouverture des portes

J'ai pu arriver en avance par rapport à ma prise de poste prévue.
Ça m'a permis de pouvoir prendre la température par rapport à la préparation de la journée et le moral global de l'équipe.
Tout semblait prêt, l'ambiance était détendue.
Les différents responsables des stands de merch s'installaient tranquillement.
Les chanteurs préparaient des CDs et vinyles dédicacés dans leur coin, sans spécialement s'isoler ou montrer d'envie d'avoir une paix royale.
Ce dernier point est assez rare pour être signalé.
Régulièrement des artistes font leurs star capricieuses.
Là, non.

Le petit détail qui tue, c'est Gaahl qui signait ses vinyles sur les tables où on allait installer le food corner.
Il avait prit le plus de place possible pour s'organiser des piles, le temps de faire ce qu'il voulait.
Quand il a vu qu'on commençait à avoir besoin de finir de préparer et commencer à s'installer, il a poliment libéré le plus d'espace possible.
Sans grogner ni s'énerver.
Il a même précisé à la régisseuse qu'il avait bientôt fini et serait prêt avant l'ouverture des portes.

## L'évènement en lui même

Pour rappel donc, on a fait salle comble.
Et je faisais partie de l'équipe au bar.
Ça s'est donc enchaîné très vite pour nous, étant donné que le public black métal, fidèle à sa réputation, descend masse binouzes.

### L'ouverture des portes et Gost

Le public est arrivé au compte-gouttes.
Nul doute que la plupart des gens n'étaient pas là pour voir spécifiquement ce groupe.
Faut aussi dire qu'à 19h il y a encore des restes de bouchons et les spectateurs venant d'autres villes n'ont sans doute pas pu arriver aussi tôt non plus.
Ça nous a permis de commencer tranquillement.

Le groupe Gost en lui même ressemble à un mélange de black métal et d'électro / synthwave.
Un croisement perturbant je dois dire.
Les parties typées black métal sont cools, et les parties synthwave intéressantes.
Mais quand ils mélangent des deux, ça laisse pantois.

Visiblement ça produisait le même effet au niveau du public qui ne remplissait pas beaucoup la salle[^1],
préférant venir nous voir au bar ou trainer aux stands de merch.

C'était aussi l'occasion de voir ces groupes de fans habituels dans les concerts :
les inconditionnels du premier rang qui se relaient au bar pour ne pas perdre leur place.

Et puis, soudain, plein de monde qui arrive, la dernière chanson du groupe, les lumière qui se rallument.
Bref, l'inter groupes.
Le moment où le changement de plateau s'opère, et surtout, celui ou tout le monde à soif.
Et là, on se prend une première pilule !

### Gaahls Wyrd

Au bout d'un moment, on se rend compte que les lumières ce sont éteintes, et qu'il y a des gens sur scène qui sont en train de jouer.
Ah oui, y'a un concert c'est vrai !
Et pourtant, côté bar, on était toujours sous l'eau (bah oui, la bière part dans le gosier des spectateurs, faut pas déconner).
Habituellement, à ce moment là ça se calme direct.
Mais cette fois, il y a eu tellement de monde d'un coup (la salle s'étant presque intégralement remplie), il restait du monde à servir.

Heureusement ça n'a pas duré et les demandes se sont étalées sur la durée du set, nous permettant une première rotation des fûts vides,
un premier lavage des premiers retours de verres, et aussi de voir un peu ce que ce nouveau projet de Gaahl donnait.

Un mélange de black moderne avec des sonorités et constructions qui n'étaient pas sans me rappeler Primordial, et d'autres groupes qu'on peut typer « pagan black ».
La présence et le charisme de Gaahl se faisant toujours autant sentir.
La température commençait à monter dans la salle, aussi bien niveau ambiance que sur le thermomètre !

Franchement, dans ces moments là, c'est cool d'être au bar.
On peut voir ce qui se passe, on est dans une équipe qui se soutient et déconne bien.
Par contre, niveau son, impossible d'avoir un truc parfait.
Au CCO comme on est bien sur le côté, forcément, c'est déformé.
Du coup je ne pourrai pas vous parler de la qualité sonore de la soirée, mais visiblement le public avait l'air très satisfait.

### Mayhem

La légende du black Norvégien qui a rassemblé la plupart des gens venus ce soir là.
Là pour le coup, on a pu profiter un peu plus !
Enfin, après la seconde pilule du changement de plateau, qui elle a duré quasi 30 minutes.

Lors de leur set, côté bar on avait tombé la plupart des fûts qu'on allait faire dans la soirée, et côté public ça se tassait bien devant la scène.
Il faisait vraiment chaud, et tout le monde était grave à fond.
On a eu très peu de demande comparé à d'autres moments ou autres têtes d'affiches.

Il faut dire que c'est un groupe qui passe assez rarement, alors forcément, il faut en profiter !
L'ambiance, le son[^2], le jeu de scène, tout était là.

Tout ? A priori pas pour tout le monde.
On a croisé des personnes assez déçues des chansons proposées.
C'est vrai que par rapport aux débuts, on a une différence notable.
Mais quand on voit l'histoire du groupe et les changements dans le line-up, forcément c'est plus pareil.
Personnellement ça ne m'a pas dérangé vu que je préfère un groupe qui tente d'évoluer plutôt que de stagner de devenir fade.
La prestation en tout cas était de qualité.


## L'après concert

Ca prend du temps de ranger quand on a tombé 15 fûts.
Mon record jusque là, probablement celui de l'asso globalement.
Surtout au niveau des verres, vu qu'il n'y avait plus trop de bière en réserve à aller ranger.

Fait amusant, pour les retours des consignes sur les verres, on a pas eu tant de rush que ça[^3].
Le plupart des gens étant passés en petits nombres qui s'étalaient sur les 3 dernières chansons.

Avant la fermeture définitive au public de la salle, on a même eu droit à plus de remerciements que d'habitude.
Et ça fait chaud au cœur.
On avait même deux des filles du bureau vers qui on a pu rediriger les compliments.
Après tout, nous on ne fait que venir quelques heures, mais pour l'orga globale le boulot à abattre est bien plus intense.
Le mérite doit donc revenir globalement à l'asso, et pas juste à l'équipe du bar[^4].

## Et c'est tout ?

Malheureusement, non.
Au delà des habituels gratteurs de binouzes, on a eu un gars plus gênant que d'habitude qui s'est mit à menacer plusieurs personnes au bar à la fin :
« Y'en a qui vont moins rigoler dans les semaines à venir ! ».
Mouais.
On a du faire venir la sécurité pour désamorcer la situation.
À priori il y a eu aussi une baston dans la fosse à un moment.
Problèmes inhérents à la scène extrême forcément, mais il ne faut pas laisser ça comme si c'était normal.

Aller, histoire de finir sur une note positive : les artistes.
Necrobutcher, Hellhammer et Gaahl notamment.
Mais des dires des autres bénévoles, tous les artistes ont été ravis de l'accueil et l'ont fait savoir après de plusieurs membres de l'équipe.
Malgré la fatigue.
C'est pas toujours comme ça ! Et c'est assez chouette pour être signalé !


## Des liens vers les groupes

- [Mayhem](https://www.metal-archives.com/bands/Mayhem/67)
- [Gaahls Wyrd](https://www.metal-archives.com/bands/Gaahls_Wyrd/3540454158)
- [Gost](https://gost1980s.bandcamp.com/music)

[^1]: Comme dit plus haut, plus à cause de l'heure et des bouchons sur les routes à mon avis.
[^2]: Ouaip, j'ai été faire un petit tour dans la fosse vu comment c'était calme au bar.
[^3]: Par rapport aux autres concerts que j'ai pu faire om là c'est vraiment toute la salle qui vient une  fois que les lumières se rallument.
[^4]: Forcément, on est en première ligne par rapport au public. Alors oui ça fait bisounours, mais ça serait de l'imposture que de prendre tout le mérite.

