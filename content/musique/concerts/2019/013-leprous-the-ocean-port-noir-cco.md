+++
categories = ["concerts"]
title = "Leprous, The Ocean et Port Noir au CCO (Villeurbanne)"
date = 2019-11-15T17:35:12+01:00
draft = false
bands = ["Leprous", "The Ocean", "Port Noir"]
+++

Après la chaleur et la noirceur de l'enfer de la dernière fois, l'ambiance était radicalement différente mercredi au CCO.
Un son placé sous plusieurs signes.
Avec une catégorisation compliquée d'ailleurs.
Les mélanges post-métal, avec de gros bouts de progressifs dedans, le tout saupoudré d'atmosphérique ont fait mouche.

Et globalement, c'était la claque !

Nous avons eu une excellente surprise pour nos yeux et surtout nos oreilles avec cette affiche.

<!--more-->


## Port Noir

Excellente surprise aussi ?
Aucune idée !
J'ai totalement loupé ce groupe car je suis arrivé en retard et surtout pile au moment où ils terminaient leur set.
Pas de bol.
Mais de ce que j'ai pu voir, la salle était déjà bien remplie, avec peu de gens de train de trainer au merch ou dehors dans l'espace fumeurs.
C'est plutôt bon signe.

J'ai aussi eu de bons échos en parlant avec plusieurs personnes, mais je ne pourrais pas vous en dire plus.

## The Ocean

On attrape vite une bière, et hop, on va se placer le mieux possible.
Les 3/4 de la salle ayant judicieusement choisi de rester sur place.
The Ocean était pourtant déjà passé ici en début d'année (le 15 Mars pour être précis), mais visiblement ils étaient de nouveau très attendus[^1].

Globalement, la prestation a été très bien reçue par le public ... En tout cas le peu que j'ai pu en observer.
Pour ma part j'étais totalement absorbé par leur univers.
J'aime beaucoup ce qu'ils font et eux aussi car ils transmettaient cette aura des musiciens très heureux d'être présents.
Le son était excellent tout comme la setlist et le jeu de lumières.

Mention spéciale au guitariste qui s'est visiblement pété le nez en se cognant contre son micro.
Malgré la flaque qu'il a crée à ses pieds et ses 2 allers retours à l'arrière pour se mettre des mouchoirs dans la narine,
il a continuné à jouer, comme si de rien n'était (les headbangs en moins).

Un groupe à citer absolument quand on demande des exemples de post-métal.

## Leprous

En dernier, voilà un groupe que je ne connaissais absolument pas.
Très content d'avoir pu voir The Ocean, je me cale dans un coin et viens tout de même jeter un œil à la prestation de Leprous par curiosité.
Et là, excellente surprise !
Je ne connaissais pas du tout, mais je découvre un excellent groupe, avec des musiciens de haut niveau visiblement très contents d'être présents eux aussi.
L'expérience visuelle accompagnait parfaitement bien le ton que le groupe cherche à transmettre, et la sonorisation était elle aussi excellente[^2].
On pouvait parfaitement ressentir la mélancolie inhérente à leurs compositions.

Un bémol pour moi par contre.
Une petite frustration personnelle.
Au niveau de la construction des morceaux, et même si c'est la patte du groupe, j'ai l'impression que ça se termine là où ça devrait partir.
Comme si l'ambiance montait, montait, montait, et PAF ! Fin !
Remarque à prendre avec des pincettes, ce ressenti est totalement lié à ce que j'aime écouter.
Cela n'enlève en rien la qualité des morceaux et le niveau du groupe.

Je recommande chaudement !

## Des liens vers les groupes

- [Leprous](https://www.metal-archives.com/bands/Leprous/106322)
- [The Ocean](https://www.metal-archives.com/bands/The_Ocean/25189)
- [Port Noir](https://www.spirit-of-metal.com/en/band/Port_Noir)

[^1]: 3615 Mavie : j'étais côté bénévole lors de leur date du 15 Mars, alors forcément j'étais _totalement_ dans le lot des impatients.
[^2]: On est rarement déçu au CCO de toute manière.

