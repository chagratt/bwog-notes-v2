+++
title = "Evergrey, Fractal Universe et Virtual Symmetry au CCO (Villeurbanne)"
date = 2022-10-19T14:10:17+02:00
bands = ["Evergrey", "Fractal Universe", "Virtual Symmetry"]
+++

Vous aimez toujours le prog' ? 
Ça tombe bien ! 
Il y en avait encore hier soir au CCO !
Mais situé cette fois la branche métal plutôt que rock.
Avec la vente des tickets pour le Hellfest 2023 qui a eu lieu ce même jour,
restait-il assez d'énergie et de sous aux fans pour venir le soir ?

<!--more-->

## Virtual Symmetry

Un tout petit set de batterie, mais une efficacité maximale !
Même si je pouvais très nettement entendre l'influence de Dream Theater dans le son,
le groupe était parfait pour commencer ce concert.
À une chose près : le chant.
Non pas que le chanteur soit mauvais, bien au contraire !
Simplement, je ne m'attendais pas du tout à du chant clair.
Et ça m'a donc complètement sorti du truc,
malgré les très bonnes compos du groupe.
J'ai totalement accrochés aux moments sans chant.

Détail amusant, le guitariste possède une guitare qui change de couleur ! Oui oui !
C'était probablement difficile à voir en plein concert, mais quand elle était posée sur scène,
avec les lumières de la salle allumées,
on pouvait voir en se déplaçant un superbe effet de peinture qui changeait donc de couleur en fonction de l'angle.
Cela allait du vert au violet en passant par le bleu.

## Fractal Universe

Alors là, j'ai pris une bonne claque !
Contrairement au groupe précédent où il y avait un chant clair,
ici point de vocalises !
Orientation Death Metal oblige, place aux growls gutturaux.
Alourdissez également la batterie.
Saupoudrez d'influences directes venant d'Opeth,
et ajoutez un soupçon de passages post-black façon Regarde Les Hommes Tomber.
Mixez le tout pour que le groupe ait tout de même sa propre identité sonore.
Voilà la recette de ce fabuleux set.

## Evergrey

Le retour du chant clair.
Dommage pour moi.
Mais on monte encore d'un cran au niveau musical.
Le niveau est élevé, et le groupe montre une véritable aisance sur scène
ainsi qu'un bon sens du spectacle.
En vrai, ça prend bien.
Le rendu est excellent, et l'ambiance globale est à son plus haut niveau dans la salle.

## Et le public dans tout ça

Je n'en ai pas parlé avant exprès car pour tous les groupes, ce fut la même chose :
tout le monde était devant la scène à profiter à fond.
Il faut toutefois noter une progression dans l'agitation et la concentration qui augmentait au fur et à mesure que la soirée avançait.
Mais avec seulement 160 personnes ce soir-là, ça fait plaisir pour les groupes autant de motivation alors qu'il y avait peu de personnes.

En bref, une petite date, mais qui a été très appréciée par les fans qui ont fait le déplacement.

## En coulisses

Les musiciens et les techniciens étaient très agréables avec nous.
Il y en a même qui nous ont aidé à ranger leurs loges pour qu'on finisse plus vite !

## Liens

- [Evergrey](https://www.metal-archives.com/bands/Evergrey/1388)
- [Fractal Universe](https://www.metal-archives.com/bands/Fractal_Universe/3540392749)
- [Virtual Symmetry](https://www.metal-archives.com/bands/Virtual_Symmetry/3540408404)

