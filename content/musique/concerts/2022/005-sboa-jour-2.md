+++
title = "Summer Breeze - jour 2"
date = 2022-09-07T20:11:16+02:00
bands = ["Mass Hysteria", "Gutalax", "Beast in Black", "Finntroll", "Electric Callboy", "Arch Enemy", "Avatar", "Dark Tranquillity"]
header_img = "sboa.png"
+++

Après une première journée pas si chargée que ça en concert, mais qui nous en a quand même mis plein les pattes.
La nuit fut bonne (merci les bouchons d'oreilles !) et la météo semble clémente.
En tout cas pour une majeure partie de la journée.

En avant pour ce 2e jour, jeudi 18 !

<!--more-->

Le premier groupe de la journée est annoncé sur la T-stage à partir de 11 h 30,
mais nous ne décollons que vers midi pour Mass Hysteria, prévu sur cette même scène.

## En passant par là : Métal Yoga

Ce n'est pas une blague !
À la Ficken Party Stage, il y avait réellement une séance de yoga en plain air,
animée par une entraîneuse sur la scène.
Le tout sur fond de musique métal, bien évidemment.
Du death, au moment où l'on est passés si je me souviens bien.
Ça a brassé beaucoup de monde, qui est venu en profiter sérieusement : tenue de sport, tapis de sol, etc.
J'ai trouvé ça impressionnant et génial à la fois toute cette foule faisant les exercices proposés.

Mention spéciale à la personne portant un costume de t-rex gonflable qui participait également.

## Mass Hysteria

La dernière fois que je les ai vus, c'était au Download Festival à Paris, en 2016.
C'était littérallement le feu !
Est-ce que ça sera pareil ici aussi ?

Hmmm, difficile à dire car le souvenir me rend totalement subjectif.
Du coup entre mon impression qu'il y avait moins de monde (il n'était que 13 h) et le nombre réel de spectateurs, impossible de juger correctement.

En tout cas, le groupe débordait d'énergie et tentait de la communiquer aux spectateurs.
En vrai, ça a bien prit.
Le set était énergique, la foule réceptrice et le son très correct.
Le chanteur et le guitariste, comme à leur habitude sont descendus dans la foule et ont lancé un circle pit autour d'eux.
Bon, là, force est d'admettre : il y a bien moins de monde.
Mais c'était quand même bien.
Par contre, dans l'agitation, le guitariste s'est blessé au genou.
Cela dit, ils ont quand même terminé le concert.
Chapeau !

Maintenant, il ne me reste plus qu'à refouler le souvenir de 2016 pour conclure que c'était un très bon moment, même s'il y avait moins de monde.
Hé oui, c'est le jeu !

Bon allez, on laisse ces considérations de côté, on espère que c'est pas trop grave pour le genou du guitariste,
et on se cherche un coin à l'ombre pour attendre le prochain groupe qui passera sur la même scène dans 30 minutes.

## Gutalax

Vous aimez le caca ? Non ?
C'est dommage car c'est le fond de commerce de Gutalax,
un groupe de goregrind, tout droit venu de République tchèque.
Avec des titres poétiques tels que « Smoked Ass with Diarrheal Sauce », « Fart and Furious » ou encore « Toi Toi Story », on voit directement où ça nous mène.
Enfin, sur le papier.
Car qui dit grind, dit technique de chant spécifique rendant incompréhensible les paroles.
Mais ça à la limite, on s'en fout totalement.
Les vraies questions sont : Pourquoi aller les voir ? Pourquoi rester ?

Pourquoi aller les voir ?
La curiosité, vu que plusieurs personnes me l'ont chaudement recommandé, en mode « tu vas voir, c'est marrant ».
Je ne connaissais pas du tout ce groupe à l'origine,
mais un subtil [lien vers un épisode de Tracks qui leur est dédié](https://www.arte.tv/fr/videos/095360-000-A/gutalax-la-solution-miracle-aux-constipations-passageres-tracks-arte/)
m'a donné le minimum à savoir.

Pourquoi rester ?
La mauvaise foi me fera écrire qu'on a réussi à bien se placer pour profiter de l'ombre de la régie, alors flemme de bouger pour le moment.
La vérité est plus simple : on s'est placé là pour avoir un bon point de vue sur la scène ET sur le public.
Ce choix fut des plus judicieux.

On commence à voir arriver en masse des gens affublés de combinaisons antibactériennes, une brosse à chiotte à la main, des ceintures de rouleaux de PQ en mode cartouchière autour de la taille.
Le ton est donné.
Le groupe arrive sur scène avec le même genre de combinaison.
Le batteur a retroussé les jambes et les manches.
Le chanteur porte de grosses lunettes de protection (on dirait des lunettes de soudeur),
le guitariste porte un .... masque à gaz ?!!
Tout ce petit monde doit avoir bien chaud.

L'ambiance était dingue.
C'était le gros bordel, mais tout le monde s'est bien amusé.

Entre le chanteur qui se dandine sur scène de manière comique,
le public à fond,
les lancers de rouleaux de PQ dans tous les sens,
l'armée de brosses à chiottes tendues en l'air en lieu et place des cornes habituelles
les slams, les pogos... Oui, c'était fou.

Il n'y a pas eu de slam de toi toi, mais une grosse poubelle s'est mise à voguer sur la foule malgré tout. :D

## Beast in Black

Encore du power ? 
Hélas oui.
Le but était d'être proche de la main stage pour être bien placés pour Finntroll.
Aller plutôt voir Omnium Gatherum (T-Stage) ou Haggefugg (Wera Tool Stage) n'était pas jouable vu leurs horaires.
Dommage.

En vrai, je ne peux pas spécialement décrire.
N'étant pas fan, ça ne m'a pas laissé un souvenir impérissable.

## Finntroll

Victoire ! Nous avons une bonne place !
Pour une fois, j'avais l'occasion de les voir ailleurs qu'en petite salle, je n'allais donc pas me priver !
Ça fait super plaisir, et je pense que le groupe se disait la même chose.
Le set était très énergique, les musiciens aussi.
Du coup, l'ambiance folle dans la foule, et je peux vous dire, au vu du nuage de poussière, qu'il y a eu d'énorme moshpits.
Le chanteur a d'ailleurs blagué à l'occasion :

« I can taste the moshpit from here » Hé ouais, tellement de poussière !

En vrai, j'aurais du mal à développer plus, car j'étais tellement pris dedans.
Un excellent moment pour moi.

## Electric Callboy

Alors là, ça a rassemblé tellement, mais _tellement_ de monde à la main stage.
Devant, derrière au niveau du « battlefield », sur les côtés.
Impossible de s'approcher ni de s'éloigner en vérité.

N'étant pas fan du groupe, mais n'ayant pas pu m'éloigner de toute façon, j'ai pu voir de quoi il en retournait, et pourquoi ça rassemblait autant de monde.
Au-delà de l'aspect festif (énergique et dansant surtout) et du mélange métal - électro qui semble avoir beaucoup de succès en Allemagne,
force est de reconnaitre que ce sont de vraies bêtes de scènes.

Les musiciens ont sauté partout sur la scène pendant l'heure du concert.
Il y a eu plusieurs changements de costumes, pour coller à certains clips très spécifiques visuellement.
En conséquence, il y a eu d'intenses pogos, ainsi qu'une mer de slams !
Tellement d'ailleurs qu'il y avait embouteillage aux barrières de sécurité.
Quelques blessé(e)s à déplorer par contre.
Sur les 4 jours, je n'ai jamais autant vu passer les secours.

Et toujours énormément de poussière devant la scène principale. :D

## Arch Enemy

Étrangement, l'endroit se vide.
Lentement, mais sûrement.
En même temps, il est presque 21 h (le concert a terminé à 20 h 30, mais le temps que tout le monde se casse ...) et je pense que les gens ont faim.
Ça nous laisse donc le champ libre pour bien se placer en face de la scène.
Puis ça se remplit petit à petit juste après.
Moins que pour Electric Callboy, mais on est bien serrés tout de même.

Et là, le début du déluge !
Enfin, plutôt une espèce de crachin, qui sera ininterrompu et deviendra plus puissant petit à petit.
Mais tellement de foule, d'énergie (et d'effets pyrotechniques) que ça va, on reste au chaud et ça s'évapore plus que ça ne mouille .... pour le moment.
Dans tous les cas, c'est la fin de la poussière.

Bien qu'étant millimétré, le show dépote.
Malgré la promotion du nouvel album en cours, le groupe est là pour jouer, pas juste pour son cachet.
Les musiciens sont au top, Alissa est à fond !
J'avais un peu peur en ayant juste écouté les singles d'une oreille, mais sur le concert ça rendait bien.
Beaucoup de nouveaux morceaux donc, mais aussi pas mal de morceaux créés depuis l'arrivée d'Alissa.
Une excellente set-list.

## Avatar

Dernier concert en date d'Avatar pour moi, en mars 2018 au Transbordeur à Lyon.
J'avais hâte de les revoir !

Au niveau du placement, on espère que ça va passer, parce qu'après Arch Enemy,
on a commencé à avoir un coup de barre et un peu froid.
Faut dire, la foule a commencé à se disperser un peu, et nous étions trempés.
Du coup, nous sommes allés chercher un petit repas chaud pas trop loin.

Le temps de revenir, le concert avait déjà commencé.
Fort heureusement, nous avons pu nous faufiler facilement pour nous placer proche de la scène.

On n'a pas du louper grand-chose, une chanson, peut-être deux.
Nous sommes arrivés sur la fin de Colossus.
Pour cette intro de concert, le groupe s'était placé en demi-cercle sur le devant de la scène, 
et restait stoique au possible ; exception faite des mouvements obligatoires pour pouvoir jouer, bien entendu.

Par rapport au dernier concert que j'ai fait, là, le décor était totalement dépouillé.
Il n'y avait, en guise de backdrop que le logo lumineux du groupe.
Cela dit, comme ce sont les musiciens les vraies vedettes, ça permettait de bien se concentrer sur eux.
Car oui, on dirait une grosse pièce de théatre musical.
Entre les gestes, les mimiques exagérées, le tout dans leurs costumes ressemblant à ceux des parades et fanfares, il y a largement de quoi faire niveau imagerie.
Cela rend le show très spécial, mais super à regarder.
En contrepartie, cela demande un déploiement d'énergie incroyable de la part du groupe.
Notamment Joannes le chanteur qui, comme d'hab a terminé liquéfié au bout de trois chansons seulement.
Cependant, le batteur n'est pas en reste.

Tiens, d'ailleurs le chanteur parle couramment allemand.
Dommage ça, car nous forcément, on a rien pigé.
Mais il échangeait énormément avec le public.
Ces instants servaient souvent de transition entre les chansons.
On voyait clairement que c'était également une tactique pour reprendre son souffle entre certaines chansons,
mais à force je pense que ça a débordé (ou alors c'était prévu ? Ça serait bizarre) car le groupe a terminé avant l'heure prévue.
Malgré tout, c'était énorme.
Encore mieux qu'en 2018 pour moi !

Mention spéciale au jerrycan d'essence qui était en fait la réserve d'eau du chanteur.

Bon par contre là, on commence vraiment à être mouillés.
Mais pas question de rentrer tout de suite.
Il reste un dernier groupe qu'on a prévu de voir.

## Dark Tranquillity

J'aime beaucoup Dark Tranquillity.
Pour moi c'est toujours un plaisir de les voir, et pas question de laisser la pluie gâcher ça.
Je fais le fier, et encaisse, ça vaudra forcément le coup.
Enfin ... j'espère.

Aie, terrible échec sur le pari : encore des problèmes de son sur la main stage !
Trop.
Beaucoup trop de basses, un volume trop faible sur la voix du chanteur.
Des fois on ne l'entendait carrément plus.

On a tenu jusqu'à la moitié du set, mais finalement la pluie,
la fatigue et le mauvais rendu sonore ont eu raison de nous.

C'est vraiment dommage.
L'énergie dégagée par les musiciens n'aura pas suffi à supplanter les désagréments techniques.

## Fin de journée

Arf, c'était bien, mais on est trempés, ça fatigue bien et en plus on a froid.
Et puis la boue, la bonne grosse gadoue bien lourde a fait son apparition un peu partout,
rendant la marche plus compliquée, demandant donc plus d'énergie encore.
Pourvu que ça se calme demain et qu'on arrive à faire sécher pompes, pantalon et pulls (oui, on avait laissé nos impers à la tente) !

## Liens

- [Mass Hysteria](https://fr.wikipedia.org/wiki/Mass_Hysteria)
- [Gutalax](https://www.metal-archives.com/bands/Gutalax/3540310608)
- [Beast in Black](https://beastinblack.com/)
- [Finntroll](https://fr.wikipedia.org/wiki/Finntroll)
- [Electric Callboy](https://www.electriccallboy.com/)
- [Arch Enemy](https://archenemy.net/en/)
- [Avatar](https://avatarmetal.com/)
- [Dark Tranquillity](https://darktranquillity.com/)
- [Les photos officielles des concerts](https://www.summer-breeze.de/en/bilder/festival/summer-breeze-2022/)

