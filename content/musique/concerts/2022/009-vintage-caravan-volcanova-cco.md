+++
title = "The Vintage Caravan et Volcanova au CCO (Villeurbanne)"
date = 2022-10-06T10:20:22+02:00
bands = ["The Vintage Caravan", "Volcanova"]
+++

Hier soir, il n'y avait pas de gros métal qui accroche au fond de la pôele,
mais un mélange au croisement du prog' et du stoner, le tout avec une bonne base rock,
et en provenance direct d'Islande.

<!--more-->

## Volcanova

Le groupe Islandais n'a pas trainé à mettre l'ambiance.
Et j'exagère à peine.
Dès la première chanson le public s'est emballé !

Il faut dire que malgré sa dénomination Stoner/Desert Rock,
Volcanova rajoute quelques éléments du rock des années 70,
mélange le tout et sort un pur produit fun et dansant.
Parfaitement adapté à la scène.
Et ce n'est pas le monde dans la fosse plutôt que dehors qui me contredira.

En plus, le groupe était parfaitement à l'aise sur scène et savait très bien échanger avec le public.
Des interactions franches et joyeuses.
Tout comme leur jeu de scène qui montrait clairement leur joie d'être présents.

Les 45 minutes du set sont passées toutes seules !

## The Vintage Caravan

Trio de Prog/Rock, avec des bouts de hard, blues, stoner, psyché, et j'en passe.
La description du groupe ne manque pas d'étiquettes musicales.
Dans tous les cas il en résulte un rock mélangeant énormément d'influences 
mais qui est diablement efficace sur scène.

Bien que la première chanson ne payait pas de mine,
dès la seconde la salle était encore plus chauffée que pour Volcanova.
Ajoutez à cela un plaisir non dissimulé de la part du groupe,
ainsi que beaucoup d'échanges avec le public,
et vous obtenez là encore un set au poil qui va passer tout seul.

D'autant que chaque morceau se détache bien du précédent,
afin de proposer une expérience à large spectre.
Ça donne presque l'impression de redécouvrir le groupe à chaque nouveau morceau.
Et de ce fait, leur set d'une heure et demie est passé tout seul.

On avait même un arrière-gout de pas assez tiens !

## Après-concert et coulisses

Les membres des deux groupes étaient des personnes très accessibles.
Ils ont passé beaucoup de temps à échanger avec leurs fans au stand de merch après le concert.
Avec de grands sourires malgré la fatigue du concert.

Ils étaient également très polis et agréables avec l'équipe technique de la salle,
ainsi que l'ensemble des bénévoles.
Tout au long de la journée, et jusqu'à leur départ.
Des musiciens un peu exubérants, mais chaleureux.
C'est assez rare pendant les tournées pour être mis en avant.

## Le point bonus

Je disais à mes collègues à la buvette que c'étaient là des groupes calibrés pour les concerts.
Que j'avais peur de m'ennuyer en CD après.
J'ai peut-être eu tort, les écoutes faites pendant la rédaction m'ont convaincu du contraire : 
je vais me repasser leurs morceaux avec plaisir !
Et je ferai tourner en soirée !

Les influences évidentes des groupes 70-80 n'y sont peut-être pas pour rien.

## Liens

- [Volcanova](https://volcanova.bandcamp.com/)
- [The Vintage Caravan](https://en.wikipedia.org/wiki/The_Vintage_Caravan)

