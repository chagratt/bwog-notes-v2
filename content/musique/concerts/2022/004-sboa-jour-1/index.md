+++
title = "Summer Breeze - jour 1"
date = 2022-08-29T10:21:56+02:00
bands = ["Blasmusik Illenschwang", "Feuerschwanz", "Eisbrecher", "Korpiklaani"]
header_img = "sboa.png"
+++

Mercredi 17, première journée de concerts qui s'annonce.
Mais rien avant 15 h.

On est frais et dispos, la météo s'annonce belle quoique potentiellement chaude,
avec une légère brise.
Tout se présente bien pour réellement débuter le festival !

<!--more-->

Avec autant de temps libre, une _petite_ glandouille bien tranquillou le matin s'impose.
On se motive quand même assez rapidement pour aller tôt sur le site du fest,
afin de prendre du merch histoire de se débarrasser de ça.
Et là, c'est l'échec.
Le site n'ouvre pas avant 13 h 30.
Tant pis, on retourne à la tente pour manger mais en faisant le grand tour.
Pour se balader, et voir si c'est plus rentable de passer de l'autre côté.
Spoiler : non.

Pendant le repas, on discute un peu avec nos voisins (en anglais, _of course_) qui nous conseillent vivement d'aller voir le tout premier groupe annoncé.
Pourquoi pas ?
Après tout, on est là aussi pour découvrir des choses.
Et de la musique en fest, c'est toujours mieux que de rester le cul posé devant la tente.

Direction la T-Stage pour se placer.

## Blasmusik Illenschwang

Nous découvrons un groupe traditionnel dont tous les membres sont en tenue bavaroise,
avec des cuivres, plein !
L'ambiance est festive, les musiciens semblent très heureux d'être là.
En vrai c'est sympa, même si on ne s'attend pas du tout à ça dans un gros festival de métal.
Le public est à fond.
Nous un peu moins.
Il faut dire que l'on ne connait pas et qu'en plus ils chantent en allemand.
Forcément, ça rend la chose un peu hermétique.

Cela dit, l'heure pendant laquelle le groupe a joué est passée toute seule.

## Petit moment de flottement

Après cette sympathique fanfare, on avait rien de prévu.
On en profite donc pour flâner et faire un peu le tour du site,
reconnaitre les lieux, voir comment c'est fait côté main stage,
repérer les points importants (eau, bière, wc, ...)
en passant au milieu de l'espace merch et boutiques.

Effectivement, pour aller entre la T-Stage (ou la Wera Tool stage) et la main stage,
il y a tout un parcours à effectuer.

{{< image src="sboa2022_plan_scenes.jpg" caption="Plan resserré du site des concerts. En vrai, c'est assez grand." >}}

Ça ne se voit pas sur le plan, alors voici une description de la topologie du terrain.
La zone de la T-Stage forme un espace plat,
mais tout le T-square jusqu'à la zone où se trouve Rock Antenne est en pente descendante
(donc toutes les boutiques sont penchées).
Puis ça remonte (un peu fortement) jusqu'au début du battlefield. 
Là, toute la zone est un plateau, sauf au niveau des WC tout au nord où ça se met à redescendre.
Ces détails auront leur importance plus tard. :D

Une fois notre petit tour terminé,
on retourne à la tente se faire un bon repas avant d'enchainer sur la soirée qui se fera d'un seul bloc.
Se planquer un peu du reste du soleil aussi, parce qu'il a fait bien chaud sur cette journée.

## Feuerschwanz

Du power.
En costumes wish.
Meh.
Le chanteur est un piquet, et le guitariste qui fait les voix additionnelles en fait beaucoup, mais alors beaucoup trop.
Le jeu de la violoniste est chouette par contre.
Il y a aussi deux danseuses qui agitent des accessoires, ou des drapeaux, ou font un numéro avec des flammes.
Les flammes à la limite, ça a un rendu visuel sympa, et ça colle avec la chanson qui passe.
Le reste du temps, on a l'impression qu'ils avaient juste envie de claquer du budget dans le salaire de deux danseuses pour aguicher les badauds.
Les fans apprécieront sûrement, mais nous, pas trop.
Même ma dame qui normalement aime le power est longtemp restée de marbre, avant d'aller sautiller avec des gens en se tenant les épaules.

On s'est infligés ça pour être bien placés pour la suite.
Enfin, à la base, parce que finalement, on a été prendre un petit remontant pour s'en remettre.

## Eisbrecher

Visiblement c'est un gros nom du métal indus, en tout cas en Allemagne.
Vu qu'habituellement ce n'est pas mon truc, je ne connaissais pas du tout.
Et, franchement, même si au début je pensais passer 1 h 30 à m'ennuyer, et bien pas du tout.

Même si on est un peu loin, les écrans géants font super bien le boulot.
Le groupe rend bien, on les sent investis dans leur prestation et le son est bon.
Le chanteur a un sacré charisme sur scène, ce qui fait que la sauce prend.
On s'étonne même à la fin, comme s'ils avaient terminé plus tôt que prévu, mais pas du tout.
Chouette découverte !
Je ne suis pas sûr d'écouter de moi-même en CD, mais en concert ça rend super bien.

Il est déjà près de 23 h, l'espace devant la scène se vide bien, les stands de bouffe se remplissent.
Je crois que le public a un petit creux.
On a bien fait de manger à la tente et de prendre une petite collation à la fin de Feuerschwanz.

Il est temps de trouver une bonne place pour le concert suivant.

## Korpiklaani

On se faufile assez facilement et on arrive à être devant la 1ère barrière.
Le placement nous semble bon, plus qu'à attendre encore un peu.

Dès le début du concert on peut sentir que le groupe est en forme.
Le batteur est tout sourire tout le long et se donne à fond.
De mémoire d'ailleurs, il ne l'a jamais été autant.
Le reste du groupe également mais forcément par rapport au batteur, ça transparait moins.

Tiens, le bassiste a rasé sa glorieuse barbe.
Et Jonne est sobre, wouahou ! Incroyable !
On profite bien du concert donc, avec une bonne ambiance globalement dans le public.

Tout à l'air bien ? Et bien presque.
Sur ce concert, nous avons pu commencer à ressentir ce qui allait être récurrent sur la main stage : les problèmes de son.
C'était rempli de craquements dans les enceintes, et souvent, la voix devenait si faible par rapport aux instruments qu'on l'entendait pas.
Mince, c'est dommage, car il y avait une très bonne ambiance dans la foule.
Mais à la longue, ça fait retomber la pression et casse un peu le truc.

## Fin de cette journée

Pfouh, on a énormément marché aujourd'hui, alors que c'était sensé être la plus petite journée.
Ma PineTime m'indique entre 20 000 et 21 000 pas.
C'est vrai que la promenade du matin et l'aller-retour dans l'après midi n'ont pas aidé.
Erreur due à la perte d'habitude des anciens fests.
On en a plein les pattes.
Pourvu que la nuit nous laisse récupérer correctement.

## Liens

- [Korpiklaani](https://korpiklaani.com/)
- [Eisbrecher](https://fr.wikipedia.org/wiki/Eisbrecher)
- [Blasmusik Illenschwang](https://blasmusik-illenschwang.de/)
- [Feuerschwanz](https://www.metal-archives.com/bands/Feuerschwanz/3540470153)
- [Les photos officielles des concerts](https://www.summer-breeze.de/en/bilder/festival/summer-breeze-2022/)

