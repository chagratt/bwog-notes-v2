+++
title = "Summer Breeze - jour 0"
date = 2022-08-26T10:11:16+02:00
bands = ["Voodoo Kiss","Crack Up","Apophis","Fleshcrawl","End Of Green"]
header_img = "sboa.png"
+++

Cette année, le Summer Breeze Open Air (que l'on peut astucieusement abréger en SBOA)
s'est tenu du mercredi 17 au samedi 20 août 2022, à Dinkelsbühl en Bavière.

Après 2 ans d'attente à cause des reports, j'ai enfin pu y aller.

{{< image src="sboa2022_flyer.jpg" caption="Affiche du festival. Beaucoup de groupes. 4 jours. Du vert. Des cranes à cornes." >}}

L'affiche envoyait du gros, et quatre jours ininterrompus  de concerts promettaient de compenser ces dernières années totalement creuses en matière de festivals.

<!--more-->

## Jour zéro : on débarque sur les lieux

Hé oui, avant de commencer à parler des concerts,
il faut bien parler de l'arrivée sur le site.
Il était possible d'arriver dès mardi 16, chose que l'on a faite.

L'idée était de pouvoir s'installer tranquillement, faire un peu le tour du camping pour repérer les endroits stratégiques (je ne vous fais pas un dessin),
et le meilleur chemin pour accéder au site du festival.
La photo d'en-tête est d'ailleurs celle que j'ai prise à ce moment-là.

Or donc, mardi nous faisons la route, et, aux portes de Dinkelsbühl, un immense bouchon.
Celui-ci durera jusqu'à l'arrivée sur le fest.
Ces derniers kilomètres furent les plus longs, non pas en termes de distance, mais en termes de temps.
Il y avait en effet déjà énormément de gens en train d'arriver sur le fest.

Une fois aux portes du camping, ça va mieux.
La zone de contrôle de la voiture est grande, il y a plusieurs files, et ça avance relativement vite.
Contrôle du matériel donc car en Allemagne tu poses ta caisse dans le champ du camping et monte ton matos directement à côté.
Mais comme le verre est totalement interdit, une petite vérification s'impose.
Il ne faut pas oublier que même si l'on a le trajet dans les pattes, l'euphorie est là.
Donc, on patiente, on se fait vérifier le matériel,
on choppe nos bracelets d'accès,
on se fait apposer sur le pare-brise de la voiture un bel autocollant qui sera super chiant à retirer
et en avant pour le camping !

{{< image src="sboa2022_bracelet.jpg" caption="Un bracelet de festival, ok, mais c'est celui du Summer Breeze 2022 !" >}}

 {{< image src="sboa2022_sticker.jpg" caption="Un de ces auto-collants d'excellente qualité qui vont être durs à retirer." >}}

## Aparté : les trucs nazes

On va évacuer immédiatement les points négatifs, histoire de pouvoir se concentrer sur l'essentiel par la suite.

Déjà, en arrivant la veille, même si c'est indiqué et un peu normal, il faut payer un surcout par personne présente.
Ensuite, quand on est placés sur le camping par les bénévoles, on est vachement serrés avec ses voisins.
Alors oui, il va y avoir du monde, donc il faut un peu tasser, mais vu la taille totale du camping, je pense qu'il y a exagération, et un mètre de plus entre chaque voiture aurait été beaucoup mieux.
Fort heureusement, nos voisins directs étaient vachement sympas.

Ensuite, les sanitaires. Bon sang, il n'y en a vraiment pas assez.
On ne l'aura pas trop senti mardi soir ni mercredi matin, mais le reste du temps, pouah !
Fallait pas être pressé.
Que ce soit d'ailleurs au niveau des sanitaires payants[^1] ou des toy-toy disposés un peu partout sur le camping.
Ce souci a beaucoup été reproché à l'organisation qui s'est fendue d'une communication à ce sujet et promet de faire mieux la prochaine fois.

Un choc culturel bruyant également : beaucoup d'allemands se ramènent avec de petits générateurs électriques à essence.
Visiblement, un paquet de gens restent bien plus au camping que sur le site.
Certains ramènent même frigos et tireuses, oui.
Bon, là où on était, c'était raisonnable en termes de machines déployées et surtout elles étaient éteintes assez tôt.
Cela dit, ça ne semble pas être le cas partout, et dans certaines parties spécifiques du camping où le bruit doit être cessé tôt selon un règlement spécial, cela n'a pas été respecté.
Là aussi des gens se sont plaints à l'orga.

En trucs qui pètent bien l'immersion, les soucis techniques sur la scène principale.
Autant de soucis cumulés sur quatre jours, c'est limite inacceptable. On parle là de la plus grosse scène du festival, celle qui brasse le plus de monde.
De nombreuses personnes l'ont remonté à l'orga.
Et non, on ne peut pas juste taper sur un sondier ou deux à ce niveau, car même si pour certains groupes le souci était très largement mitigé,
des groupes ayant habituellement un son irréprochable se sont retrouvé avec des soucis allant de craquements dans les enceintes à une coupure totale.
Mais ça, on y reviendra groupe par groupe.

Et enfin, le pire : la sortie du fest dimanche 21.
Quatre heures pour pouvoir se barrer !
Non seulement, là où on était, c'était chiant de rejoindre le chemin principal,
mais en plus avec un seul point de sortie, je ne vous raconte pas les bouchons.
Un enfer ! Littéralement !
Et, autant il y avait plein de bénévoles pour fluidifier l'entrée et garer les véhicules à l'arrivée,
autant au départ : personne.
Démerdez-vous !
Sauf à la sortie, y'en avait juste deux pour te dire que non, le chemin direct, c'est mort,
prend la déviation de plusieurs kilomètres.
Avec la fatigue des quatre jours, les nerfs étaient à vif.

Dans notre malheur, nous avons eu de la chance. Non seulement il ne faisait pas trop chaud,
mais surtout, le bouchon sur la départementale s'était bien résorbé quand on a enfin pu y accéder.

Voilà, tout est dit, on peut passer à la suite : le fest en lui-même.

## On commence à profiter

Une fois la voiture parquée,
on déballe le matos,
on monte la tente,
on déplie les fauteuils,
et on se prend une bière bien méritée en faisant connaissance avec les voisins.
Prost !

{{< image src="sboa2022_plan_global.jpg" caption="Le plan du site. Ça en fait des tentes par rapport à l'espace des scènes." >}}

Après ça, 
direction la Ficken Party Stage située au milieu du camping,
pas très loin de l'entrée du fest.
Une petite scène pas dégueu du tout en vérité.
Oufti, y'a déjà du monde et du son dites donc !
Et des stands.
De la bouffe, et, bien évidemment, de la bière !
Il y a même une supérette pour acheter un peu de bouffe, de bière,
ainsi que des articles de première nécessité.

Pour les groupes qui passaient ce soir là,
on n'a pas trop fait gaffe, on a profité sans y prêté spécialement attention.
Je met tout de même les liens des groupes en bas.
A priori, des groupes qui étaient là lors de la première édition si j'ai bien suivi.

Après un certain temps passé à flâner et faire le tour, puis revenir, et bien il fait nuit.
Donc, retour au camping, et nouvelles bières avec les voisins puis direction le dodo.
Même si le premier jour va être calme (concerts qu'à partir de 15 h) il s'agit de ne pas être trop fatigué d'entrée de jeu.

La double bonne idée niveau équipement : le matelas gonflable, le vrai.
Et surtout, des bouchons d'oreilles !

La suite au prochain article. :)

## Liens

- [Voodoo Kiss](https://www.metal-archives.com/bands/Voodoo_Kiss/3540511454)
- [Crack Up](https://www.metal-archives.com/bands/Crack_Up/3393)
- [Apophis](https://apophis-band.de/)
- [Fleshcrawl](https://fr.wikipedia.org/wiki/Fleshcrawl)
- [End Of Green](https://www.youtube.com/user/endofgreen)
- [Les photos officielles des concerts](https://www.summer-breeze.de/en/bilder/festival/summer-breeze-2022/)

[^1]: C'est plus le prix du forfait (15€) que le principe qui sera choquant, même si on l'a largement rentabilisé.

