+++
title = "Finntroll, Skálmöld et Brymir au Ninkasi Kao (Lyon)"
date = 2022-12-03T18:00:00+01:00
bands = ["Finntroll", "Skálmöld", "Brymir"]
+++

La soirée du jeudi 1er décembre a été placée sous le signe du métal venant du Nord.
Nous avons eu droit à deux groupes venant de Finlande et un d'Islande,
le tout oscillant entre le folk et le death mélo,
avec des bouts importés du black métal.

Autant dire que le programme s'annonçait mouvementé, surtout vu la tête d'affiche !

<!--more-->

## Brymir

Pas le temps de s'échauffer que c'était directement la grosse claque !
Je ne connaissais pas du tout le groupe et ce fût pour moi une excellent découverte.

Brymir nous sert un death-mélo à tempo très rapide, avec un excellent son.
Ce n'est pas courant pour une première partie (même si ça arrive de plus en plus, c'est cool),
surtout au Kao.
Le groupe débordait également d'énergie, en arborant un sourire jusqu'aux oreilles.
Les musiciens occupaient bien l'espace disponible sur la scène,
avaient une très bonne synergie et jouaient autant entre eux qu'avec le public.
C'était une ambiance incroyable, et ça a vraiment bien prit avec la foule.
Leurs compos sont entrainantes et puissantes à la fois.

Il va falloir que je me penche sérieusement sur leur cas.

Point faible du groupe : le temps de jeu qui leur a été accordé. Seulement 35 minutes.
C'est habituel, mais fortement décevant quand on prend autant de plaisir avec un groupe.
Un set malheureusement bien court par rapport à leur niveau et l'énergie transmise.
C'est passé extrêmement vite.

## Skálmöld

Ça va être rapide : j'ai snobé.
Cependant, je dois admettre qu'ils avaient un bon son (confirmé par les potes qui étaient dans la salle),
et ça reste un groupe où les musiciens sont bons.
Mais ce n'est pas mon truc.

Du coup, pour laisser la place aux fans, j'ai glandé autour du bar, un peu dehors,
discuté avec des gens,
et surtout j'en ai profité pour aller parler avec le chanteur de Brymir.
Ce dernier était au stand de merch pour rencontrer le public.
Un chouette moment d'ailleurs.
Il était super content qu'on vienne le voir pour dire qu'on a aimé leur set, tout en gardant un côté humble.
Ça confirme cette impression sur scène : ils étaient réellement heureux de jouer.


## Finntroll

Que dire en vérité ?
Ça fait des années que je suis fan et que je vais régulièrement les voir quand ils passent à côté de chez moi.
À la limite, je peux dire que ce concert tombait à pic : j'avais une revanche à prendre sur le son infâme du Summer Breeze,
et en plus cette fois j'allais pouvoir m'en donner à cœur joie dans la fosse (je ne pogotte pas ou très peu en festival).

Le son donc : il était impeccable, à part deux ou trois moments pendant le concert où la voix du chanteur s'est effacés pendant quelques mots.
Un phénomène bizarre, mais rien d'alarmant, surtout que c'est arrivé quand la foule hurlait les paroles.

En parlant de la foule, nous étions déchainés dans le pit.
Enfin je dis « nous » mais en vérité je n'ai été que pendant la première moitié du set dans le cœur de la tempête.
En effet, avec ma compagne, nous avions négocié pour profiter chacun d'une moitié dans les pogos pendant que l'autre portait les affaires.
Hé oui, il n'y avait pas de vestiaire.
C'était d'ailleurs une terrible erreur de ma part : la seconde moitié était bien plus endiablée que la première !
Flûte !

La playlist était incroyable, l'énergie du groupe également.
Et ne parlons pas de la fosse qui, même en étant très petite, a fourni un maelström des plus vigoureux.

Un moment hors du temps et du quotidien qui fait beaucoup de bien au moral,
mais vachement moins aux cordes vocales et à la nuque.
Je ne vous parlerai pas de la collection de bleus que j'ai récoltés dans la fosse. :p

## Liens

- [Finntroll](https://www.metal-archives.com/bands/Finntroll/95)
- [Skálmöld](https://www.metal-archives.com/bands/Sk%C3%A1lm%C3%B6ld/3540318962)
- [Brymir](https://www.metal-archives.com/bands/Brymir/3540325883)

