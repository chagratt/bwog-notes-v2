+++
title = "Eluveitie, Amorphis, Dark Tranquillity et Nailed to Obscurity au Radiant-Bellevue (Caluire Et Cuire)"
date = 2022-11-29T09:39:54+01:00
bands = ["Eluveitie", "Amorphis", "Dark Tranquillity", "Nailed to Obscurity"]
+++

Vendredi 18, pour une fois depuis un moment, j'étais dans la fosse !
Et dans une salle où je n'avais jamais mit les pieds : le Radiant-Bellevue.

Ce billet sera très court car la soirée l'était tout autant.

<!--more-->

Comment ça une soirée courte ? 
Et l'amour de la musique alors ?
Malgré la passion, il y a des choses contre lesquelles on ne peut lutter :
l'horaire d'ouverture des portes.
Et là surprise ! Ça ouvrait à 17 heures.
C'est tôt.
Trop tôt.
À cause de ça, forcément, j'ai loupé la 1ère partie.
Au revoir Nailed to Obscurity !

En calculant un peu, avec le début programmé à 18h, normalement pour Dark Tranquillity, ça devait passer.
Encore raté ! Je suis arrivé pour les 2 dernières chansons.
Les concerts ont commencé plus tôt.

Autant pour Nailed to Obscurity, c'était pas grave pour moi, autant là, j'étais plus que dégouté.
Bon, au moins, petite revanche par rapport au Summer Breeze,
car là le son était nickel.
Et le groupe a toujours autant d'énergie et même de synergie (entre eux, et avec le public).
C'était toujours ça de sympa à voir.

Vint ensuite Amorphis,
mais là je n'ai pas été voir car je ne suis pas spécialement fan,
et j'ai été causer avec un groupe de potes pas vus depuis longtemps et croisés par hasard.

Allez,
ne soyons pas venus pour rien, Eluveitie.
Je ne les ai pas vu en concert depuis que j'ai quitté Grenoble (une salle sympa la belle électrique),
et je n'ai pas été super fan de leurs albums depuis Origins.
Mais quitte à être sur place, autant jeter une oreille.

Un très bon point pour le groupe, le son était très bien géré.
C'est toujours une bonne nouvelle vu le nombre d'instruments différents qui jouent en même temps.
Par contre, à l'inverse de Dark Tranquillity, le jeu de scène et les interactions entre les membres et avec le public est bien plus froid.
C'est propre et professionnel, mais pas passionné.

La « nouvelle » chanteuse remplace bien Anna Murphy, **sauf** quand le groupe joue ses anciens morceaux.
Là, elle a beaucoup de mal à tenir quand elle pousse dans les aigus, et ça s'entend énormément.
On sent que ces vieilles chansons ont été composées pour une chanteuse précise.
C'est vraiment dommage.
Cela dit, quand ce sont les morceaux qui ont été composés depuis sont arrivée,
le problème ne se pose plus,
et l'on peut apprécier pleinement son talent.
On peut préférer Anna Murphy, ce qui est mon cas (probablement par nostalgie ? Allez savoir ...),
mais on ne peut pas renier Fabienne Erni, pour peu qu'on prenne un référentiel correct.
C'est à dire des chansons composées pour chacune.

Par contre, on va encore mettre un carton rouge pour la pelle de la montagneuh, pardon, l'appel des montagnes.
Je n'aime déjà pas la chanson de base, mais alors la chanter traduite pour le fan service, c'est encore pire à mes oreilles ...
Quand bien même le public n'en veut pas, le groupe la fait à chaque fois.

Et on termine par Inis Mona en conclusion.

Bon, finalement, c'était plutôt positif.
Même si l'horaire incroyable me reste en travers de la gorge.
Ainsi que le prix de la bière ! ARGH !

