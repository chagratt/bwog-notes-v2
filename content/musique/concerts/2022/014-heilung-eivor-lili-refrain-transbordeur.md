+++
title = "Heilung, Eivør et Lili Refrain au Transbordeur (Villeurbanne)"
date = 2022-12-18T10:30:37+01:00
bands = ["Heilung", "Eivør", "Lili Refrain"]
+++


Point de métal à proprement parler lundi 12 décembre au Transbordeur mais un mélange
pagan / folk faisant la belle part aux contes et légendes nordiques, mais pas que.
Et là encore, ça reste très réducteur par rapport à la réalité du concert.

Embarquons donc pour une expérience aussi bien visuelle que sonore.

<!--more-->

_Ce billet a été co-rédigé avec ma Dame, puisque nous y étions tous les deux et qu'elle a été très forte pour m'aider à mettre les bons mots._

## Lili Refrain

Nous avons loupé cette artiste qui joue en solo, mais d'après les potes qui l'ont vue, c'était incroyable.
Pour ne pas la laisser de côté malgré tout, voici une traduction de son texte de présentation issu de bandcamp :

Lili Refrain est une musicienne basée à Rome, Italie.
Depuis 2007, elle a un projet solo dans lequel elle utilise la guitare électrique,
le chant, les percussions, le synthé et les boucles en temps réel,
sans utiliser d'ordinateur ou de pistes préenregistrées.
Sa technique magistrale et son goût raffiné conduisent l'auditeur dans le dédale d'un acte inoubliable au-delà des frontières de tout genre.

## Eivør

Une installation minimaliste : un pied de micro au milieu de la scène et en arrière plan, quelques instruments.
De quoi accueillir deux personnes.

Ça tombe bien, il n'y aura qu'Eivør et un musicien.
Elle occupera le centre tandis que celui qui l'accompagne est en retrait, à gauche.
C'est un peu dommage, car il est un peu caché par ses arrangeurs,
et j'aurais bien aimé mieux le voir lorsqu'il jouait du violoncelle (si ce n'en était pas un, ça s'en rapprochait beaucoup).

Cela dit, peu importe.
Malgré le minimalisme, Eivør remplit l'espace de par sa voix et sa prestance lorsqu'elle chante et joue.
C'est une toute autre personne qui est là, à jouer des cordes vocales, par rapport à celle qui échange avec le public.
Il semble qu'Eivør vit sa musique, et en ça, je lui aie trouvé un petit côté proche de ce que Djerv est capable de donner lors de ces concerts.

D'ailleurs, les morceaux joués (synthés, boites à rythmes, violoncelle, et guitare électrique) sont réarrangés par rapport à ce qu'on connaissait en CD.
Ce n'est pas un mal, c'est même une bonne idée, afin d'offrir quelque chose d'adapté et de différent,
mais je dois admettre que ça ne m'a pas intéressé pour tous les morceaux.
Même mené avec brio, c'est toujours un exercice difficile.
Cela dit, il faut saluer l'effort.

Quoi qu'il en soit, c'était un excellent moment.
Nous avions là une chanteuse avec une voix incroyable (elle fait des écarts, et des changements de type de chants impressionnants)
qui arrivait à bien prendre possession de la scène à elle toute seule.

## Heilung

Globalement, c'est pour eux que nous sommes venus.
Et nous n'avons pas été déçus.
Dès l'extinction des lumières, le groupe a commencé à distiller son ambiance ;
et ça sera le cas jusqu'à la fin.

Le concert débute par un des membres qui est encapuchonné.
Celui-ci parcourt la scène en faisant brûler des herbes dans un encensoir,
le tout dans une démarche cassée, saccadée, énigmatique, proche d'une transe.
Viennent ensuite d'autres membres, également en tenue, qui se placent en cercle.
Ils déclament alors un mélange de chant et de d'incantation, dans une ambiance de rite chamanique païen, avant de prendre place sur scène afin enchaîner sur leur premier morceau.

À partir de là, nous étions dans un autre temps, voire un autre monde.
Et c'était incroyable.
Déjà au niveau musical.
Le son était impeccable, ce qui nous a permis de profiter de la puissance vocale de chaque personne qui chante :
- la chanteuse principale, avec sa voix cristalline, mais qui sait également descendre très rapidement dans les graves.
- le chanteur principal, avec sa capacité à émettre des sons gutturaux impressionnants, même quand on est habitué aux growls.
- les trois chanteuses de soutien, avec leur timbre qui complète parfaitement le spectre sonore.

Rajoutez à cela les percussions, énormément de percussions, avec de gros tambours, mais aussi d'autres instruments ou objets insolites (une réplique d'épée viking),
et vous avez là une base solide et puissante pour porter le concert.

Nous pouvions tout entendre distinctement, et c'était un régal.
Les voix déjà énumérées étaient parfois épaulées par un dernier groupe au chant : des danseuses et danseurs qui faisaient des allers et venues (on va y revenir)
dont deux avaient parfois un micro pour pousser encore plus la puissance sonore.

Nous venons donc de voir que le son était impeccable, mais ce n'était que la moitié de la claque prise pendant ce concert.
Car pour le coup, le spectacle était également visuel, et je peux dire qu'on en a pris plein les mirettes !
Il y a déjà la chanteuse et le chanteur principal qui ont leurs tenues d'inspiration chamanique avec leur couvre-chef à cornes de cerf.
Ensuite, chaque musicien sur scène a une tenue d'inspiration historique.
Enfin, les danseuses et danseurs qui pour leur part étaient torses nus, badigeonnés de peinture foncée (couleur noire ou terre, difficile à dire),
avec comme accessoires une lance et un bouclier rond.
Tout ce beau monde représentait, au plus fort de l'occupation de la scène, la bagatelle de 17 personnes.
C'est pas rien !
Et pourtant, la troupe se déplaçait avec aisance, avait des chorégraphies avec les musiciens et chanteurs, tapaient parfois en rythme sur le sol avec leurs lances.
L'ensemble rendait très bien visuellement et accompagnait parfaitement la musique.
Chaque chanson ayant son propre thème, sa propre couleur, sa propre ambiance, et donc, sa propre mise en scène avec à chaque fois une chorégraphie bien spécifique.

En parlant de couleur,
le dernier point qui enfonce le clou de l'excellent spectacle : parlons de la lumière.
Nous avons l'habitude d'en voir sur les concerts de métal, et elle est là pour souligner l'ambiance d'un morceau.
Ici c'est presque l'inverse.
Le jeu de couleur propulse carrément le jeu de scène à un niveau supérieur tant tout semble parfaitement relié.
Pendant tout le long du concert, il y a eu d'habiles jeux afin de faire de beaux contre-jours.
Cela amenait des ombres et des découpages visuels parfaitement dans le ton.
Et à l'inverse, il y avait aussi par moment des mises en avant de certains musiciens, danseurs ou chanteuses,
pour nous aider à regarder où il faut pendant la chanson.

Ajoutons aussi que l'ambiance était également portée par le public qui, de temps à autre, répondait au groupe en poussant des cris gutturaux parfaitement raccords avec le moment où ils étaient poussés.
Cela rajoute encore un niveau à l'atmosphère incroyable qui était installée dans la salle.

C'était vraiment une expérience complète, totalement en dehors de mes habitudes.
Malgré le pavé, j'ai l'impression d'oublier des choses, ou de ne pas avoir correctement décrit le concert tant c'était différent et incroyable.

Un moment hors du temps et du monde dans lequel j'ai hâte de replonger.

## Liens

- [Lili Refrain (bandcamp)](https://lilirefrain.bandcamp.com/)
- [Eivør (Wikipedia Fr)](https://fr.wikipedia.org/wiki/Eiv%C3%B8r_P%C3%A1lsd%C3%B3ttir)
- [Heilung (bandcamp)](https://heilung.bandcamp.com/)


