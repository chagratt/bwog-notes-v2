+++
title = "Summer Breeze - jour 3"
date = 2022-09-15T21:35:16+02:00
bands = ["Djerv", "Alestorm", "Cytotoxin", "Jinjer", "Within Temptation", "Insomnium"]
header_img = "sboa.png"
+++

Après une fin de jeudi humide, nous nous réveillons sous un ciel très menaçant.
Ce vendredi 19 sera-t-il aussi rempli que la veille ?

Pas plus de suspense : non.
Il a beaucoup plus ce jour là, et ça nous a pas mal refroidis, même si les fringues de la veille ont seché.
Sauf mes pompes. Argh !

<!--more-->

## Un début de journée morose

Le vendredi commence donc comme la fin de jeudi, à un détail près :
ce n'est plus un crachin mais une bonne grosse pluie qui nous tombe dessus.
Avec quelques accalmies passagères.
Mais en toute honnêteté, même si on avait prévus quelques groupes assez tôt,
la motivation elle, est resté au sec sous la tente.
Heureusement que nous avions de quoi nous abriter pour ne pas rester enfermés dans cette dernière.

Quasi toute la journée de glande au camping donc.
Fort heureusement, ça _semble_ se calmer sur les cours de 17 h.
Ni une ni deux, on en profite pour se motiver en se disant que, au pire, la Wera Stage est abritée.
Et ça tombe bien, c'est là qu'est le prochain concert que l'on veut aller voir.

## Djerv

Après la traversée boueuse des allées du camping, nous voici enfin devant la Wera Tool Rebel Stage.
Enfin, on essaie, car il y a pas mal de monde sous les abris.
Heureusement, on l'a jouée fine.
L'endroit étant en pente, on a réussi à se placer en « hauteur », ce qui nous laisse une vue imprenable sur la scène.
Tout en étant abrités.
Ça fait du bien !

Et maintenant, profitons de Djerv et de son énergie,
bien trop grande pour cette petite scène.
Elle donne de la voix bien comme il faut, occupe l'espace et a bien sélectionné ses morceaux.

Une fois qu'elle a terminé son set,
on se dit que le temps est passé vachement vite.
C'est ça, le talent.
Mais, il y a comme un gout de « trop tôt ».
On regarde donc sa montre, et là, c'est le drame.
Djerv n'a joué que 30 minutes !
À peine le temps de se mettre dans l'ambiance qu'elle doit arrêter.
Les boules !

M'enfin au moins, j'ai enfin pu la voir en concert.
Ça reste donc une victoire.

## Alestorm

Au début, j'étais parti pour aller voir Napalm Death sur la T-Stage,
mais ça aurait été de loin uniquement vu le monde déjà sur place.
J'ai donc plutôt été voir Alestorm sur la Main Stage.

La pluie se fait un peu plus clémente à ce moment,
mais la boue est, et sera toujours là désormais.
Cela dit, sa consistance a plus ou moins changé.
Elle est lourde en plus d'être visqueuse.
L'épreuve de la traversée est donc plus complexe que la veille.
Mais ce n'est pas grave, nous arrivons sans encombres au lieu prévu.

Le gros canard gonflable est là, et la partie pavée devant la scène aussi,
ce qui apporte une stabilité bienvenue.

Le set commence et le groupe envoie directement l'ambiance.
Ou bien le public.
Dans tous les cas, pas besoin d'échauffement ici, c'est directement le feu,
et ça le restera tout du long.

La set-list contient très bon mélange entre anciens morceaux (que j'adore),
plus récentes (auxquels je n'ai pas accrochés) ainsi que du dernier que je n'ai pas écouté.
Sur scène, avec l'ambiance explosive, l'ensemble passe super bien.
Le groupe est communicatif et semble très content d'être là,
ainsi que de voir autant d'agitation malgré la mare (voire le lac) de boue.

Même si ce concert est chouette, pas le temps de niaiser, 
Jinjer est pour bientôt, il faut y aller pour être bien placés.

## Cytotoxin

Sur le chemin de la T-Stage, il y a toujours la Wera, et au moment où nous passons,
c'est Cytotoxin qui déploie toute son énergie sur scène.
Hop, on gratte une petite place et on s'arrête là pour le moment !

Le son est bon, le groupe tabasse.
On en attend pas moins et on est servis.
Même s'il n'y a pas grand monde, ça gigote bien et surtout devant la scène.

Le groupe a même du mal à canaliser le pogo pour le transformer en circle-pit.
Et c'est là que le chanteur a sorti ...
[un panneau de rond-point](https://www.summer-breeze.de/wp-content/uploads/2022/08/20/Cytotoxin_20220819_SS_008.jpg) qu'il agitait pour donner un effet de rotation aux flèches !
Et ça a marché !
À plusieurs reprises !

Je suis super content de les revoir en tout cas !

## Jinjer

Après Arch Enemy hier,
retrouvons une autre chanteuse de death qui envoie du lourd !

Malgré la halte devant Cytotoxin,
nous trouvons une bonne place car le concert d'Alestorm (qui a drainé pas mal de monde)
vient juste de se terminer.

Le groupe n'a pas pu jouer depuis un bon moment,
et on sent bien qu'être enfin là les motive plus que jamais.
La dernière fois que je les ai vus, c'était au Plane R Fest en 2019.
C'était déjà énorme, et maintenant, c'est encore plus fort !
Sentiment renforcé par le fait que je ne les écoute pas en CD, du coup aucun a-priori sur les morceaux ni la set-list.

Morceaux qui s'enchainent très bien, avec un jeu de scène qui respire le groupe qui peut faire comme bon lui semble.
Un très bon point par rapport à Arch Enemy, il faut le signaler.
Ce fut donc un excellent moment.

## Within Temptation

On retourne du côté de la main stage, principalement parce que c'est là qu'il y a le plus de stands de bouffe.
Manger avec Sharon en fond sonore, c'est quand même pas si mal.
La set-list comprend énormément de morceaux que je connais.
Ça fait un peu bizarre, d'autant que j'ai l'impression que son chant est moins puissant qu'avant.
Problèmes de santé ?
Souvenirs qui me mentent ?
Soucis de son (encore) sur la main stage ?
Je n'aurais jamais la réponse.

Une fois nos estomacs remplis, la pluie décide de s'inviter de nouveau. Mince.

On reste encore un peu devant Within, mais on se rend vite compte que ça va bientôt être l'heure d'Insomnium.
Allez zou ! Tirons-nous d'ici.
Le sympho c'est cool en fond sonore, mais ce n'est pas mon style favori.

## Insomnium

Après une énième traversée des champs de boue, nous voici de retour devant la T-Stage,
et un set death-mélo fortement teinté de pluie.

Le concert est chouette, j'aime beaucoup le groupe et le son rend super bien.
Plus que la dernière fois que je les ai vus.
Mais, honnêtement, le souvenir n'est pas aussi bon que j'espérais.
Le moment passé sur place non plus malgré l'énergie du groupe.
Je suis trempé du caleçon aux orteils, mes chaussettes sont des éponges, mon pantalon et mes jambes ruissellent.
L'énergie et la motivation ne sont plus là.

Je tente de profiter un peu, mais je n'ai pas pu tenir jusqu'au bout.
Je suis dégouté.

## Une journée qui tourne court

Quand bien même il n'est pas encore minuit, que nous avions nos impers sur le dos toute la journée,
et que la pluie se calme enfin, nous n'en pouvons plus.
Nos pantalons sont trempés, mes pieds également.

Et la marche dans les lacs de boue n'aide pas à économiser son énergie.
C'est bien dépités, mais aussi très fatigués que nous décidons d'arrêter là.
Avec l'espoir que ça s'arrête une bonne fois pour toutes.

## Liens

- [Djerv](https://www.djervmusic.com/)
- [Alestorm](https://alestorm.net/)
- [Cytotoxin](https://www.metal-archives.com/bands/Cytotoxin/3540325917)
- [Jinjer](https://fr.wikipedia.org/wiki/Jinjer)
- [Within Temptation](https://www.within-temptation.com/)
- [Insomnium](https://fr.wikipedia.org/wiki/Insomnium)
- [Les photos officielles des concerts](https://www.summer-breeze.de/en/bilder/festival/summer-breeze-2022/)

