+++
title = "Regarde les hommes tomber et Dysylumn au Marché-Gare (Lyon)"
date = 2023-05-20T10:10:29+02:00
bands = ["Regarde Les Hommes Tomber", "Dysylumn"]
+++

Vendredi 12 mai se déroulait un concert que j'avais très envie de voir,
dans une salle où je n'avais encore jamais mis les pieds.

L'asso de concert n'ayant pas eu besoin de beaucoup de bras (on ne tient pas le bar là-bas),
j'y suis allé en tant que spectateur.
C'est parti pour une soirée d'un point de vue purement visiteur !

<!--more-->

## Le marché gare

Tout d'abord, un petit aparté sur la salle de concert.
Située au cœur du quartier de La Confluence côté Rhône,
elle a été rénovée récemment et dispose maintenant d'une capacité d'accueil de 414 personnes.
L'ensemble est donc tout neuf et nous y mettons les pieds pour la première fois.

La salle de concert est à l'étage, c'est curieux dit ainsi,
mais le bâtiment étant un immense porche, ça fait sens.

Ça fait plaisir d'avoir une salle « neuve » depuis le temps, mais il faut avouer que quelque part,
c'est limite _trop_ neuf.
Pour l'instant.
Au moins, du côté des artistes, c'est spacieux, confortable et correctement organisé.

Petit bémol toutefois :
la circulation des personnes est mal pensée au niveau de la salle qui contient la scène.
En effet, il y a de nombreuses portes tout le long de la pièce.
Si vous avez eu le malheur d'arriver un peu tard et que vous vous retrouvez proche de ces portes,
vous ne serez jamais tranquille et ça vous sortira du spectacle constamment.
L'espace derrière la régie est aussi bizarrement fichu, avec un petit escalier de quelques marches,
et un replat qui ne permet pas spécialement de voir mieux depuis le fond de la salle.
Coup dur pour les personnes de moins d'1m65.


## Dysylumn

Groupe lyonnais de Black/Death atmosphérique, Dysylumn a ouvert le bal à 20h30.
Quand je suis monté, la salle était déjà bien remplie, ce qui m'a permis de déceler les défauts cités précédemment.
Pas grave, j'ai trouvé une bonne place derrière la régie.

Le groupe a proposé d'entrée un set solide avec un très bon son et des jeux de lumières froids collant parfaitement à l'ambiance proposée par les morceaux.
Cependant, je n'ai pas réussi à accrocher.
En effet dans leurs morceaux de début de concert, il y a des arpèges et accords extrêmement dissonants.
Je comprends totalement le but recherché avec cette composition,
et d'un point de vue objectif cela entre complètement dans leurs thèmes,
mais je suis obligé d'abdiquer : cela ne fonctionne pas sur moi et me sort complètement du concert.
Je sors donc au bout de 20 minutes, et ne pourrais pas en dire plus.

Je tenterai probablement de les ré-écouter en CD pour être certain de mon impression.

Cependant, la salle est restée bien remplie[^1] pendant leur set,
ce qui fait plaisir pour une première partie.

## Regarde Les Hommes Tomber

Maintenant que la salle est bien vidée, il est temps de se placer correctement.
Je me faufile donc au 3e rang pendant le changement de plateau.
Le créneau prévu était de 30 minutes, mais ça a été plié en moins de 15, la classe !
Et le petit détail qui tue, juste avant de commencer, le guitariste allume les candélabres à gros coup de chalumeau de cuisine, mwahahaha !

Débute enfin le set, qui sera porté tout au long par une lumière principalement tamisée, blanche ou jaunâtre,
avec parfois un éclairage puissant venant de derrière le batteur.
Je n'ai pas grand-chose à dire par rapport au jeu de scène et aux échanges avec le public, 
ce genre de métal ayant un code un peu triste consistant à balancer son univers sans en sortir une seule seconde.
C'est un peu dommage je trouve, d'un point de vue humain, mais d'un point de vue spectacle, avec le jeu de lumière et l'ambiance globale, ça passe bien.
Le jeu de scène minimaliste colle parfaitement à l'ambiance du groupe.

Par contre, c'est là que je comprends ma remarque de tout à l'heure sur la difficulté des petites personnes à voir depuis le fond de la salle : 
la scène est très peu surélevée finalement.
En tout cas, pour moi, qui était très proche (entre le guitariste et le chanteur),
ça faisait quand même son petit effet grâce à la lumière et à leurs peintures sur le visage[^2].

Au niveau du son, je l'ai trouvé très équilibré entre les instruments,
mais la voix était très souvent en dessous.
Je me suis surpris à me dire régulièrement « mince, mais en fait il chante là .... Où est sa voix ? ».
C'est vraiment dommage car ça sort complètement de l'ambiance et il faut s'y remettre à chaque fois lorsque le chanteur est de nouveau audible.
Visiblement, depuis le fond de la salle c'était plutôt les aigus qui étaient énormément atténués.
Globalement quand j'ai discuté avec des gens après le concert, le son leur a semblé moins bon que pour le premier groupe.

En détail intéressant que j'ai pu remarquer, c'est au niveau de la batterie.
Comparé à la plupart des groupes de black et death,
le batteur a un très petit kit de batterie : seulement deux grosses caisses en plus des toms classiques et 7 cymbales au total (en comptant le charley).
Et pourtant, il envoyait vraiment du lourd dans son jeu ! Impressionnant !
Vers les deux tiers du set, des pogos se sont lancés, ce qui avait l'air de ravir les musiciens.
Enfin je crois, difficile à dire vu leur jeu de scène consistant à ne pas échanger avec le public.

Avant de conclure sur cette soirée, je donne tout de même un carton rouge pour les deux fois où le chanteur s'est amusé à remplir sa bouche d'eau pour ensuite la cracher sur le public.
Perso, je trouve ça dégueulasse.
Asperger depuis une bouteille (neuve) ou avec un petit pistolet à eau est mieux de mon point de vue.

## Le mot de la fin

Malgré les petits problèmes de son probablement inhérents à la salle, je suis très content d'avoir pu assister au concert de Regarde Les Hommes Tomber,
depuis le temps que je voulais les voir.

Pour un prochain concert au Marché Gare, je me placerais probablement ailleurs, devant ou à côté de la régie si j'y arrive.
Sinon, dans le pit, en fonction des groupes, ah ah !

## Liens

- [Dysylumn](https://dysylumn.bandcamp.com/)
- [Regarde Les Hommes Tomber](https://rlht.bandcamp.com/album/ascension)

[^1]: Estimation totalement pifométrique réalisée en voyant la masse de gens sortir de la salle lors de l'inter-groupes.
[^2]: On ne peut pas réellement parler de corpse-paint ici.
