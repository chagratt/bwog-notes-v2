+++
title = "Swallow The Sun, Avatarium et Shores Of Null au CCO (Villeurbanne)"
date = 2023-04-26T20:46:00+02:00
bands = ["Swallow The Sun", "Avatarium", "Shores Of Null"]
+++

La soirée de lundi 24 avril était placée sous le signe du doom métal,
mélangé à d'autres genres.
Une programmation intéressante,
surtout que Swallow The Sun était déjà passé au CCO en tant que première partie.
Cette fois, ils étaient en tête d'affiche.

Malgré une salle loin d'être comble, ce qui est toujours dommage pour les artistes,
il y a eu de chouettes moments.

<!--more-->


## Shores Of Null

Croisant du black mélodique avec du doom métal et nous venant tout droit d'italie,
les cinq membres de ce premier groupe nous ont délivré un début de concert tout à fait pertinent pour débuter la soirée.
Les deux premiers morceaux ont étrangement sonné, le temps que le sondier peaufine les réglages.
Cela a un peu ralenti l'entrée dans l'ambiance, mais après quelques chansons, elle a commencé à prendre.
Même si la salle était peu remplie à ce moment-là.

On sent dans le jeu sur scène et les compos que le groupe n'a sorti que 4 albums,
mais honnêtement, ce fut une découverte très prometteuse.
J'ai personnellement beaucoup aimé.

On avait donc là une très bonne entrée en matière, avec un rythme plus rapide que du doom classique et des accords de guitare typiques du black, quoiqu'un peu allégés.

## Avatarium

On continue la soirée avec un mélange de rock et de doom qui nous vient de Suède.
Un croisement osé et intéressant sur le papier.
La recette est simple :
on prend une chanteuse avec une belle voix, qui joue aussi des fois de la guitare folk sur certains morceaux,
puis on ajoute le combo classique composé de guitare, basse et batterie qui dépotent.
On mixe les deux ... eeeeeet .... ça tombe à plat !

Le souci ne vient pas des artistes.
Les parties où la chanteuse est seule avec sa guitare sont bonnes,
elle a une très belle voix,
l'instrumentale métal est chouette,
mais le mélange des deux ne passe pas.
Il y a une forte dissonnance pour mes oreilles entre la chanteuse et le métal proposé par le groupe.
Dommage. Frustrant même de mon point de vue.

Au niveau du public, la tension est d'ailleurs quelque peu redescendue, traduisant une surprise générale sur ce concept déroutant.

## Swallow The Sun

Pour terminer, nous avons eu un groupe mélangeant allègrement le death mélo et le ... oui,
toujours le doom !

La dernière fois, c'était [en première partie de Primordial]({{< ref "001-primordial-swallow-sun-rome-cco.md" >}}),
on peut dire qu'ils n'ont pas volé leur tête d'affiche.
Le mélange proposé par le groupe est extrêmement efficace, même (surtout ?) lorsque l'on aime pas spécifiquement le doom.
Le public a été très réceptif et a immédiatement plongé dans l'ambiance, et ce, sur toute la durée du set.
Le temps a d'ailleurs filé extrêmement vite.

Moi qui m'en fichais à la base, je me suis surpris à écouter d'une oreille très attentive.
Je n'ai pas tellement fait attention au jeu de scène.
Cependant, je peux dire que l'éclairage jouait dans les tons froids,
ce qui collait parfaitement à l'ambiance proposée par le groupe.

## Le mot de la fin

Avant-dernière date au CCO Jean-Pierre Lachaize pour l'asso.
Après tous ces conerts qu'on a passés là-bas, ça fait un petit quelque chose ...
Rendez-vous pour Eisbrecher ?[^1]

## Liens

- [Swallow The Sun](https://www.metal-archives.com/bands/Swallow_the_Sun/12613)
- [Avatarium](https://www.metal-archives.com/bands/Avatarium/3540369803)
- [Shores Of Null](https://www.metal-archives.com/bands/Shores_of_Null/3540378671)

[^1]: Je ne devrais pas faire une promesse que je ne suis pas certain de tenir ...
