+++
Categories = ["découvertes"]
title = "Nytt Land"
date = 2020-04-24T14:13:48+02:00
draft = false
bands = ["Nytt Land"]
+++

Parfois on regarde les recommandation des sites d'écoutes / vidéos.
D'autres fois ce sont des connaissances qui lancent le nom d'un groupe.
A certaines occasions, c'est une première partie de concert.
Et puis pour ce groupe c'est l'affiche d'un festival.

Une typographie pas banale, un arrière plan intriguant.
Voici les éléments qui m'ont fait aller découvrir ce groupe plutôt qu'un autre.
Je sais, je me gâche **totalement** la surprise.
Peut-être la vôtre aussi d'ailleurs !

Bienvenue dans le monde du pagan chamanique venu tout droit de Sibérie.
J'ai nommé : Nytt Land.

<!--more-->

Mélange de folk Nordique aux ambiance chamaniques, le tout largement inspiré de l'edda Islandais[^1], voici une formation pas banale du tout.
D'autant plus que les membres du groupe sont originaires de Russie.

Avant d'aller plus loin, une petite remarque sur les sonorités du groupe.
Le traditionnel Nordique est occupé par beaucoup de groupes sur un segment qui laisse peu de place aux changements radicaux.
C'est pour cela qu'on aura parfois des sonorités proches de ce que Fejd et autres groupes connus font.
Heureusement, dès que le chant se lance, la comparaison s'arrête là et le plaisir auditif débute.


Le groupe nous distille une ambiance onirique.
On laisse son esprit voyager à travers les étendues glacées, ou on s'imagine en pleines cérémonies païennes.
La contemplation semble être le maitre mot dans leurs morceaux.
Avec toutefois des compositions plus festives, qui pourraient illustrer une scène de récit poétique accompagné d'une danse voire d'une fête.
Notons que le rythme est lent.
Ça peut vite être un frein pour apprécier le groupe.
Pour pallier à cela je dirais qu'en musique de fond ça passera très bien.
Ou bien en concert.
Expérience à venir ... J'espère !

Il y a également un petit quelque chose d'intrigant dans la voix de la chanteuse quand elle la pousse.
Sur certains phonèmes ça s'entend bien.
Je ne saurais dire si ça vient de sa technique de chant ou non, mais je trouve que ça ajoute un petit quelque chose d'unique, agréable.

En bref :
Vous avez aimé la B.O de la série Vikings ? Foncez !
Skáld ça déboite mais vous voulez un groupe de plus dans votre liste ? Ajoutez y Nytt Land.
Vous aimez Heilung mais cherchez une ambiance moins sombre ? Allez y, écoutez Nytt Land.
Vous appréciez beaucoup Omnia mais cherchez quelque chose de plus historique ? Nytt Land est fait pour vous.

Est-ce que je recommande ? Fortement !
Sauf si vous n'aimez pas ce style, évidemment ...

## Liens

- [Bandcamp](https://nyttland.bandcamp.com/)
- [Wikipedia (En)](https://en.wikipedia.org/wiki/Nytt_Land)
- [Description sur le site du Summer Breeeze](https://www.summer-breeze.de/en/bands/nytt-land-214744/) (en Anglais).


[^1]: Ce n'est pas moi qui le dit, ça vient d'eux.

