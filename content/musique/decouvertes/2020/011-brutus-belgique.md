+++
Categories = ["découvertes"]
title = "Brutus (Belgique)"
date = 2020-03-27T16:00:54+01:00
draft = false
bands = ["Brutus (Belgique)"]
+++

Petite entorse à mon habitude de sélection de groupes du blog, je vais vous aujourd'hui vous présenter un groupe qui oscille entre post-rock, punk et métal[^1].
Découvert grâce à un pouet de [@arofarn](https://mamot.fr/@arofarn/103799587263902954),
voici un power trio Belge en apparence assez classique : batterie, basse, guitare.

Mais si je vous en parle, c'est parce que ce groupe est un peu plus que ça.
Un peu comme une Rangers : vu comme ça, c'est une chaussure.
Mais en travers de la gueule ça change tout.
Pour ce groupe, c'est un peu pareil : une fois dans les oreilles c'est plus pareil !

<!--more-->

Pour découvrir ce groupe, je suis donc d'abord passé par l'album intitulé « Nest ».
Et je dois dire qu'il est passé tout seul !
J'ai ensuite écouté le reste de la discographie.
Ça tabasse, y'a rien à dire.
Et ce n'est pas juste bêtement bourrin : on trouve des variations au sein d'un même morceau.
D'ailleurs les albums distillent des chansons plus calmes de temps à autre.
Le groupe sait bourriner et se la jouer plus « aérien ».
Je trouve que c'est bien construit, le mélange énergique (voire violent) piqué au death/black métal avec la légèreté du post-rock ça passe crème !

La compo instrumentale, c'est bien. Mais la voix, ça donne quoi ?
Et bien déjà, surprise ! Au chant, c'est la batteuse !
En vrai, ça ne change qu'une chose : ça impressionne de pouvoir tenir autant au chant et à la batterie[^2].
Le grain de sa voix est agréable et le groupe arrange bien ses morceaux autour de son timbre.
Un petit point négatif toutefois : il y a un effet lointain sur la voix de la chanteuse.
Peut être pour ne pas surprendre le jour où ça sera en concert ?
Il faut dire que sonoriser une batterie et une personne au chant c'est déjà chiant, mais alors les deux en même temps au même poste ...

Ce que je retiens de ce groupe globalement :
ce ne sont pas des débutants et ça se sent, on est sur une œuvre complète, bien construite, qui pourra en offrir encore plus dans le futur !

Je recommande !


## Liens

- [BandCamp](https://wearebrutus.bandcamp.com/album/nest)

[^1]: Le groupe a plus d'étiquettes que ça (par exemple math-rock). Je vous laisse seul(e)s juges. Je ne donne que les plus facilement reconnaissables.
[^2]: Et à titre totalement personnel, ça me rend vraiment curieux de voir ça en vrai : la performance artistique, et la perf au niveau du réglage son.

