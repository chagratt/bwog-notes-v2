+++
Categories = ["découvertes"]
title = "Haken"
date = 2020-06-30T18:30:13+02:00
draft = false
bands = ["Haken"]
+++

J'avais dit que je vous ferai une écoute de Haken, et c'est maintenant chose faite.
Un bon gros Prog pour commencer la semaine c'est osé.
Est-ce que ça valait le coup ?
Si on aime le prog, carrément !
D'ailleurs, c'est n'est pas totalement complet comme étiquette ...

Suivez le guide pour mon ressenti complet.
Bonne lecture.

<!--more-->

Haken est un groupe originaire de Londres composé de 6 personnes.
Leur carrière sous ce nom semble avoir commencé en 2007, et le premier album est sorti en 2010.
Depuis, le groupe en a sorti 5 au total.
Notons que j'ai tout écouté dans le désordre le plus complet.

Quel que soit le morceau on sent directement la maitrise.
Et quand ce ne sont pas des ponts planant, on a régulièrement des petites démonstrations techniques vraiment agréables à l'oreille,
sans avoir de solos interminables.
Et ça, j'apprécie beaucoup.
Ce sont surtout les ponts entre deux parties qui sont composés de sorte à allonger l'expérience.
Là encore, rien d'interminable.
On a plus l'impression de prendre une pause contemplative lors d'une randonnée plutôt qu'un trajet en bus qui n'en finit plus.
Cela dit, de temps à autre, les morceaux sont parfois longs, et les structures déroutantes quand on n'est pas habitué.

Au niveau des sonorités du groupe,
quand on connait aussi peu le prog que moi, ça complète bien le paysage musical de ce côté-là :
ce n'est pas du Tool, ce n'est pas du Dream Theater, ni du Myrath par exemple.
Attention d'ailleurs, ce n'est pas du pur métal comme on peut le lire à certains endroits (quoi que ça dépend des albums).
Nous avons ici un mélange complexe avec des bouts de jazz, de (post-)rock, des sonorités électroniques étranges ...
Avec tout ça, les surprises auditives seront au rendez-vous !
Même les voyages temporels !
Malgré leur dénomination << moderne >> (il y a beaucoup d'inspirations Djent dans le son),
on retrouve dans certains morceaux un côté décalé, voir psyché façon années 60-70.

Un petit bémol pour le chant toutefois :
même si la voix est maitrisée, j'ai trouvé qu'il n'y avait pas beaucoup de modulations dans la hauteur développée par le chanteur.
Dommage à côté de tout le travail déployé sur les instruments.

Ça serait osé comme pari, mais pour faire découvrir le progressif à quelqu'un, je ne recommande pas en premier groupe.
Pour oreilles averties ou curieuses par contre, il y a de quoi bien s'amuser !
Surtout avec ces impressions de faire des voyages dans le temps et les styles.

Très bonne découverte pour ma part.
Hop, j'ajoute dans ma liste de groupes !

## Liens

- [Site officiel](https://www.hakenmusic.com/)
- [Page Wikipedia (Fr)](https://fr.wikipedia.org/wiki/Haken)

