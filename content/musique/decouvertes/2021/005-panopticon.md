+++
Categories = ["découvertes"]
title = "Panopticon"
date = 2021-07-15T14:40:43+02:00
bands = ["Panopticon"]
+++

À partir d'une chaude recommandation de la part de [Yahiko](https://framapiaf.org/@Yahiko),
me voilà aujourd'hui en train de plonger allègrement dans un mélange atmosphérique black/folk tout droit venu des U.S.

Lorsque l'on me parle black atmosphérique, je pense immédiatement à Saor ou bien Summoning.
Il était grand temps que je lorgne sur un autre groupe ne venant pas d'Europe, pour découvrir ce que ça peut donner ailleurs dans le monde.

<!--more-->

Il vaut mieux que je vous prévienne immédiatement :
si vous êtes là pour chercher du black métal qui tabasse tout le temps sans s'arrêter, il faut passer votre chemin.
Ici ce qui prime, c'est de prendre le temps, de mesurer sa lourdeur et d'aimer la contemplation mélancolique.
Même s'il y a des morceaux bien plus percutants que d'autres, il faut l'avouer, à condition de bien choisir son album.

Le chant est tantôt déchirant, tantôt clair, en fonction des chansons et des effets cherchés par le groupe.
On peut relever de nombreux passages purement instrumentaux qui sont très dissonants dans certains morceaux.
Dans d'autres, c'est plus le décalage entre instruments qui donne cette impression.
Voire, dans certaines chansons, on a des gammes et des effets que la Country et la folk Américaine ne renieraient pas.
Cela donne un effet très intéressant.
D'ailleurs, on a parfois des morceaux avec une simple guitare acoustique accompagnée d'un chant clair mais triste.
C'est en total contre-pied avec le reste, sauf pour un point : la mélancolie.

J'évoquais Saor en intro de cet article, finalement à juste titre, j'ai trouvé quelques morceaux où j'ai pu faire un parrallèle,
mais il faut avouer que c'est un peu facile à faire dès qu'il y a un violon qui se pointe.
Là, il est juste passé faire coucou, c'est loin d'être une constante dans l'univers musical de Panopticon.
Un univers très large d'ailleurs, puisque j'ai surpris des passages très post-black dans l'album _...And Again Into The Light_.

La retranscription de cette découverte semble décousue, mais en vérité, c'est parce que chaque album diffère des autres.
Je me suis presque surpris à me demander si je ne m'étais pas gouré d'artiste tant le décalage de _The Scars of Man on the Once Nameless Wilderness (I and II)_ avec tout le reste est si prononcé.
Mention spéciale aussi à _Scars II (The basics)_ qui est encore plus surprenant ... Mais ... Surprise !

Surprises également au sein d'un seul et même album en vérité et même au sein d'un seul et même morceaux.
C'est assez fort je trouve.

Vous trouverez avec Panopticon une discographie dense mais intéressante, qui va cependant demander du temps, mais qui saura vous convenir en fonction de votre humeur du moment.
Il y aura forcément un morceau ou un album pour vous contenter musicalement.

Tout cela justifie totalement cette découverte et son ajout à ma liste de groupe.

## Liens

- [Page Bandcamp de Panopticon](https://thetruepanopticon.bandcamp.com/)
- [Page de Panopticon sur L’encyclopaedia Metallum](https://www.metal-archives.com/bands/Panopticon/126117)

