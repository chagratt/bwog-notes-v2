+++
title = "Saor : Amidst the Ruins"
date = 2025-02-17T11:25:00+01:00
bands = ["Saor"]
+++

Sorti le 7 février 2025,
Saor nous présente son nouvel album !
Après avoir lamentablement loupé la sortie du précédent, Origins, en 2022,
il est grand temps de me rattraper.

[Sur sa page Bancamp](https://saor.bandcamp.com/album/amidst-the-ruins-2),
Saor nous promet un voyage à travers des vallons érodés et collines éthérées,
une narration parlant de force, de tristesse et de renaissance.

Pour compenser ma traduction libre,
toutefois quelque peu approximative et extrêmement résumée,
voici le texte complet :

{{< details summary="Texte complet (en anglais)" >}}
> Amidst the melancholy ruins and the grandeur of nature, their latest album, Amidst the Ruins, majestically intertwines the past with the present.
> From the haunting echoes of Glen of Sorrow that recounts the betrayal of the Massacre of Glencoe,
> to the forest's whispered secrets in The Sylvan Embrace featuring Jo Quail,
> each track is a portal to Scotland’s rich heritage.
> This album is a tribute to the timeless landscapes and ancestral spirits,
> capturing the soul of a nation in every note.
> With Echoes of the Ancient Land and Rebirth,
> listeners are invited on a journey through weathered glens and ethereal hills,
> where each melody reveals a narrative of strength, sorrow, and renewal.
{{< /details >}}


Cette promesse est-elle tenue ?

Pour le découvrir,
enfilons nos meilleures capes en laine, nos plus solides bottes de marche,
prenons notre bâton de marche fétiche,
et partons ensemble dans ce voyage à travers les _highlands_.

Un bon premier signe concerne le découpage de l'album :
Les morceaux sont longs.
Le plus court fait 8 minutes,
le plus long 14.
Les autres durent autour de 11 minutes.
Le tout réparti en 5 morceaux qui s'étalent sur 59 minutes.
Pour un groupe qui mélange l'atmosphérique le folk et le black métal,
c'est encourageant.
Pour moi cela indique que l'artiste prend son temps pour raconter son l'histoire.

Le périple débute bien,
l'impression d'immensité des paysages,
ainsi que la mélancolie des lieux se dégagent dès les premiers instants d'écoute.
On se laisse facilement bercer par ces 12 premières minutes,
avant de passer brutalement à la suite.

Il y a un changement de ton pour le second morceau.
Il est plus agressif et percutant au début, puis se calme ensuite, pour nous offrir une remontée lente mais inexorable en puissance.
Difficile de parler de diptyque pour ces deux premières chansons,
tant la rupture et la couleur dans la composition diffèrent,
mais l'idée est là.
Un peu comme lorsque l'on gravit un col lors d'une randonnée pour basculer ensuite dans un tout autre paysage.
La sensation diffère du début, pourtant c'est toujours le même voyage qui est en cours.

Le 3è morceau est très complexe et passe par beaucoup de phases et d'émotions.
Il relate un épisode historique complexe de l'Écosse que je ne vous ferai pas l'affront de résumer.
Pour en savoir un peu plus et mieux appréhender la chanson,
[je vous renvoie à cet article wikiepdia](https://fr.wikipedia.org/wiki/Massacre_de_Glencoe).
Avec ces informations en tête, on comprend tout de suite mieux le ton et son évolution.
Cela débute de manière sombre et lente (vous aimez le funeral doom ?), un peu comme un glas.
Il y a ensuite une légère montée épique, avant de rebasculer sur du black plus classique.
Le tout avec une touche légèrement symphonique.
Entrecoupé de passages plus atmosphériques (avec une teinte plaintive pour marquer le tragique dans la composition),
le morceau se déroule en racontant son histoire, et ses émotions qui évoluent.

Le 4è, le plus court, commence très doucement et instille un calme inhabituel, reposant, éthéré.
On peut s'imaginer en train bivouaquer après une randonnée tranquille au travers d'une ancienne forêt calme.
Les chants légers, la douce guitare et les percussions sobres qui viennent ensuite confirment cette impression.
Le reste de la composition instrumentale renforce encore cette impression.

Le morceau final tranche totalement avec cet instant de repos.
Il attaque fort, mais toujours avec cette impression mélancolique typique de Saor dans les accords utilisés.
Toutefois, une certaine ambiance énergique se dégage ensuite.
Venant d'un morceau intitulé « Rebirth », cela semble approprié.
On pourrait s'imaginer une ode au printemps, avec ses paysages qui reverdissent, ses arbres qui reprennent vie, et fleurs qui éclosent un peu partout.
Ce renouveau arrive d'ailleurs de manière franche passé les deux tiers de la chanson,
où la mélancolie disparaît totalement.
Une conclusion en fanfare et au gout positif pour cet album !

Globalement, un très bon album pour moi,
qui propose ce que je recherche et aime quand je veux écouter du Saor.
On reste dans la veine des albums précédents pour moi[^1],
avec toutefois une montée en puissance et en qualité dans la composition et le mixage.
Cela change peu par rapport à ce que l'on connait,
mais ce n'est pas dérangeant de mon point de vue,
et il serait difficile de faire de gros changements tant le style proposé est codifié.
De gros changements pourraient d'ailleurs dénaturer ce que cherche à nous proposer le groupe.
Je ne pense donc pas que ce soit souhaitable.

Si vous aimez Saor et les grands espaces de l'Écosse, foncez !
Car la promesse est tenue.

[^1]: En omettant Origins que j'ai loupé, et pas encore écouté à date de publication de cet article.
