+++
Categories = ["albums"]
title = "Uada : Djinn"
date = 2020-10-06T10:14:03+02:00
draft = false
bands = ["Uada"]
+++

Uada est un groupe qu'on peut qualifier de « melodic black metal ».
À vrai dire, on y trouve aussi beaucoup d'influences du Death, et ce 3e album sorti le 25 Septembre 2020 va nous l'envoyer en pleine poire.

Le phrasé est un peu rustre, mais c'est en fait la force de ce groupe, depuis ses débuts.
Ça n'annonce rien, et ça te balance tout dans un son moderne, propre, accrocheur, avec un gros et magnifique travail sur la batterie.

D'ailleurs le single « Djinn », qui donne son nom à l'album a clairement annoncé la couleur.
Entre continuité de l'œuvre et promesses de nouveautés, où se situe réellement le groupe avec cet album ?

Suivez-moi pour le savoir, mais d'abord échauffez-vous pour le headbang.

<!--more-->

D'emblée, Uada nous annonce que ça ne sera pas exactement comme avant avec ce 1er morceau.
Certains passages font beaucoup penser à The Great Old Ones, et les transitions sont assez atypiques par rapport à l'identité sonore du groupe.
Mais ça sonne.
Terriblement bien.
C'est à mon sens une très bonne idée d'avoir placé ce single en début d'album.
Ça donne une sorte de rappel de ce qui attend à l'écoute, ou bien une excellente mise en bouche pour quand on découvre.

On enchaine avec le second morceau qui commence comme ce qu'on a déjà pu entendre sur les anciens albums.
Mais très vite, le groupe nous propose des ajouts ressemblant furieusement à du post-black.
Il y en avait déjà avant, mais ici c'est plus poussé et travaillé.
Grosse surprise d'ailleurs, il n'y a pas que ça.
En vrai, ça dénote et ça pourrait rebuter pas mal de monde.
Perso j'aime beaucoup et je trouve que ça apporte une belle couleur au morceau.
Et un bel hommage aux morceaux iconiques du métal.
Puis on repart de plus belle en tabassant un max.
C'est Uada après tout !

Suite de l'album.
Pas le temps de niaiser, nouvelle couleur, nouvelle ambiance.
Mais on écoute toujours le même groupe, aucun doute là-dessus.
Détail amusant, on se surprend à croire qu'on écoute plusieurs morceaux, alors que c'est toujours le même[^1].
Intéressant la façon dont ils retombent sur leurs pieds pour rappeler où on en est dans l'album.
En même temps, ce morceau fait plus de treize minutes.
Je n'avais pas fait gaffe, et finalement quand on se pose cette question, on est déjà au 3/4 de la chanson.
C'est franchement bien joué !

Dans ce quatrième morceau, il y a encore une couleur différente.
Décidément, cet aspect est bien plus poussé que dans les anciens albums.
Chapeau au groupe !
Et là aussi, encore un habile mélange de styles pour casser la structure et le rythme du morceau,
afin de forcer l'aspect « découpage en chapitres » de l'histoire.
Ça donne de gros contre-pieds très intéressants même si l'enchainement est doux et bien adapté entre les styles.

L'avant-dernier morceau, le cinquième, est un peu plus « classique » par rapport à ce que le groupe a déjà fait dans ses albums précédents.
Mais il y a un petit quelque chose en plus, une espèce de profondeur dans le son que je n'avais pas remarqué avant.
Une sorte de retour aux sources, mais améliorée.

Et pour finir, le sixième et dernier morceau (quoi ? Déjà ? Snif ...).
Il somme comme une espèce de grosse conclusion : on reprend tout ce qu'on a appris précédemment, et on le mixe.
Le résultat est convaincant.
Tantôt planant, tantôt percutant, mais toujours plaisant.
Une captivante histoire auditive en plusieurs chapitres.
On se surprend même à en redemander à la fin

## Que retenir de cette écoute ?

Globalement il y a toujours cette puissance dans le son.
Les passages un peu plus éthérées sont plus nombreux, et plus travaillés qu'avant.
Il y a un vrai travail de fond sur la superposition des instruments, entre la batterie et les 2 guitares, puis la basse.
Tout en s'améliorant, le groupe se permet de nous apporter aussi du neuf avec du vieux.
En cherchant des sonorités à incorporer pour apporter une touche en plus,
Uada a été piocher dans pas mal de choses.
Que ce soit de l'historique, voire iconique ou du plus récent.

Tout ceci donne un album qui possède la patte du groupe, avec son petit truc en plus par rapport aux précédents.
Et c'est loin d'être désagréable je trouve.
C'est percutant, comme à son habitude mais bien plus surprenant dans  ses enchainements.
Avec des moments plus calmes très bien placés et arrangés.

Uada nous avait déjà prouvé qu'ils savaient tabasser niveau compo.
Et bien ils viennent de montrer qu'ils savent faire plus que ça.

Le groupe propose un album de qualité, foncez, il déchire !


## Liens

- [Page de Uada sur L'Encyclopaedia Metallum](https://www.metal-archives.com/bands/Uada/3540409911)
- [Page bandcamp](https://uada.bandcamp.com/)

[^1]: On ressentira régulièrement cette sensation lors de l'écoute de l'album.

