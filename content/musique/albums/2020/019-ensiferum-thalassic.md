+++
Categories = ["albums"]
title = "Ensiferum : Thalassic"
date = 2020-07-27T18:38:13+02:00
draft = false
bands = ["Ensiferum"]
+++

J'aime beaucoup Ensiferum.
C'est un de mes groupes préférés, et je n'ai jamais vraiment été déçu depuis leurs débuts.
Même le départ de Jari Mäenpää ni le gros changement qui a suivi en 2005 ne m'ont pas gêné.
L'effet pervers inverse, c'est qu'à chaque nouvelle sortie d'album, je serre les dents ... Sait-on jamais.

Alors, cette fois, qu'est-ce que ça va donner ?
Un album résolument tourné vers la mer, ça ne peut pas décevoir normalement.

<!--more-->

En premier lieu, l'album s'ouvre sur une intro dont on a l'habitude : une intrumentale qui fait monter la pression.
Classique, efficace. En tout cas ça marche à chaque coup avec moi.

Ça continue par .... hmmmm ... Un début de chanson qui tabasse !
Mais une suite qui flaire bon l'hommage au vieux heavy, voire power façon Manowar.
Meh.
Pourtant, la véritable patte d'Ensiferum est là, et représente la plus grosse partie du morceau.
Ces ajouts me laissent quand même perplexe.
Bon.
Attendons de voir la suite.
On enchaine sur une chanson un poil mélancolique, toujours avec la patte << classique >> du groupe, mais qui là encore lorgne sur un autre style qui ne m'intéresse pas des masses à certains moments. 

Après cette entrée en matière peu engageante de mon point de vue, on enchaine avec une chanson qui tente le conte épique, un peu à la Rhapsody of fire, mais en moins rapide.
Le groupe semble continuer de vouloir nous emmener sur ce terrain, composé d'épique mais un peu plus aérien qu'avant.
Alors que l'album semble tourner autour de l'océan[^1].
On sent moins le côté guerrier terre à terre et frappant.
On est plus dans le voyage, presque parfois la contemplation.
Cela dit, les sons m'inspirent plus le regard vers la mer en étant assis en haut de la falaise qu'en étant en train de voguer vers l'aventure à bord d'un langskip.
Impression qui se confirme dans la suite de l'album.
Avec, qui se rajoute petit à petit, une couche presque nostalgique, mélancolique. 

Tout à la fin de l'album, on touche presque au sacré : Cold Northland, 3e titre estampillé << Väinämöinen >>. Suite de Old Man et Little Dreamer.
Dangereux pari que d'apporter un morceau à un diptyque culte après autant de temps.
On rage directement et on jette tout ou bien on se pose et on approuve ?
Eh bien pour ma part, sur ce morceau, je retrouve tout ce que j'aime d'Ensiferum.
Avec en plus, l'apport du thème et des compos spécifiques à cet album qui s'intègrent correctement dans ce titre.
Pas certain par contre que l'estampiller ainsi soit pertinent, car on ne sent pas réellement de lien avec les 2 premières parties.
Un peu dommage tout de même.
Mais on a là une chouette chanson.

Bon. C'est la fin de l'album.
Je ne sais pas trop quoi en penser.
Globalement, l'arrivée de Pekka Montin pour les claviers est une bonne chose pour le groupe, ça leur évitera d'avoir recours à trop de samples à l'avenir.
Mais à mon sens pas certain que son apport sur les chants clairs soit réellement un plus.
En tout cas, c'est bizarrement amené sur cet album, surtout sur la chanson Andromeda.
Sur les autres ça fait plus sens je trouve, un peu comme une opposition de personnages sur une pièce d'opéra.
Sa voix répond bien aux chœurs que le groupe sait faire, étrange que sur certaines chansons ça soit bizarrement foutu en tentant de la faire répondre aux growls.
Du coup ça donne parfois un côté trop heavy / glam suraigües que je n'aime pas.
Heureusement, ça reste anecdotique, et principalement audible ce sont les deux chansons qui ont servi de single pour annoncer l'album (Andromeda ainsi que Rum, Women, Victory) que je n'aime vraiment pas.

En soi l'album est bon, on n'est pas en train de se dire qu'il y a des morceaux à jeter et d'autres, rares, qu'on peut garder parce que << ça passe >>.
Mais la direction prise par le groupe sur cet album m'intéresse moins par rapport à ceux passés.
C'est bien ficelé, le souci n'est pas là.
C'est plutôt qu'on a là une œuvre plus calme, alors qu'on ne s'y attend absolument pas, car on n'est pas du tout habitué à ça venant d'Ensiferum.
Ça tabasse vachement moins ... Ça va faire bizarre dans la fosse au prochain concert (s'il arrive ...).

Pour moi, c'est un résultat en demi-teinte.
Soit c'est un hommage mal assumé à d'autres groupes / styles.
Soit c'est une expérience incomplète.
On dirait que le groupe tente quelque chose (ce que je ne pourrai jamais reprocher) mais qu'il n'ose pas y aller totalement.
Bon, au moins on peut se consoler en se disant que c'est toujours mieux qu'un paquet de sorties récentes[^2] ...

J'espère qu'ils ne sont pas en train de s'adoucir avec le temps.
À voir ce que ça donnera au prochain, ou rendra en live.


[^1]: Regardez bien les titres des chansons.
[^2]: Non je ne cite personne, mais personnellement, depuis début 2019 je suis vachement déçu.


