+++
Categories = ["albums"]
title = "Regarde Les Hommes Tomber : Ascension"
date = 2020-03-09T17:52:25+01:00
draft = false
bands = ["Regarde Les Hommes Tomber"]
+++


Poids lourd Français du post-black métal, Regarde Les Hommes Tomber revient dans nos oreilles avec un 3è album.
Après deux œuvres que j'aime à qualifier de fracassantes et 5 ans d'attente, que peut on penser de ce nouvel opus ?

Attention, tout comme la capacité du groupe à captiver, ça va aller très vite !

<!--more-->

Le voyage commence avec une entrée en matière calme mais qui fait vite monter la pression avant de lancer le monstre.
On est tout de suite dans l'ambiance.
Le monstre arrive, on le sent, il est là quelque part et est à deux doigt de nous sauter à la gorge.
Fin de l'intro, début de la 2è piste, et là directement, c'est percutant.
Et ça sera ça jusqu'à la fin de l'album.
Avec également la présence de passage plus éthérés afin de pouvoir reposer sa concentration.
Mais le poids de l'univers proposé se fait toujours ressentir. Chapeau !

Et voilà, c'est terminé. Mince, c'est tout ? Oui.
Enfin, pour l'écoute en tout cas mais de mon point de vue on peut en tirer pas mal de petites choses (en dehors du plaisir de l'écoute).
Déjà on note rapidement des petits changements de sons par rapport à avant.
L'album a sa propre identité sonore par rapport aux 2 autres.
Il y a plus de nuances et d'octaves (de chanteurs ?) dans le chant par rapport à avant, ça pousse une dimension nouvelle et intéressante dans la composition.
Je retrouve ce jeu à la batterie qui me plait beaucoup, surtout les roulements en mode pont entre deux parties d'un morceaux.
Mention spéciale à la chanson en français, ça rend bizarrement mais c'est méga classe.

Bon, on voit tout de suite que je suis fan ou pas ?
Difficile d'être objectif sur cet album je l'admets.
En tout cas le groupe continue sur sa lancée pour nous proposer un univers sombre, épais et captivant qui prend au tripes dès les premières notes.
J'apprécie particulièrement les variations dans le son par rapport à l'album d'avant.
Le groupe progresse, teste, propose des nouveautés, tout en gardant son identité sonore.

C'est du lourd ! Ça valait l'attente ! Et le mot de la fin : Vivement leur tournée !

## Liens

- [Page sur l'Encyclopaedia Metallum](https://www.metal-archives.com/bands/Regarde_les_Hommes_Tomber/3540361855)

