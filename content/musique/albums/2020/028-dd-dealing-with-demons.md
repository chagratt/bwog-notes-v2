+++
Categories = ["albums"]
title = "DevilDriver : Dealing With Demons"
date = 2020-12-24T09:15:20+01:00
draft = false
bands = ["DevilDriver"]
+++

Quatre ans après Trust no One, deux après Outlaws 'till the end, DevilDriver revient en force avec un album studio qui s'annonce court mais intense.

J'ai mis un peu de temps pour me pencher dessus, car il est sorti le 2 octobre 2020.
Alors est-ce que l'attente valait le coup ?
Divulgâchis : oui ! Enfin, pour moi en tout cas.

<!--more-->

En vérité, cet article va être un peu court.
Comme l'album.
C'est un point qui fâche un peu car avec 39 minutes et 32 secondes de temps d'écoute je reste sur ma faim.

Mais pourquoi est-il si court cet album ?
Annoncé comme concept donc, cet album a été prévu il y a un long moment par le groupe qui avait à la base écrit 48 chansons.
C'est beaucoup, et pour cet album ils ont choisi de descendre à 20 ou 22[^1] et de prendre leur temps.

Le contexte, c'est bien, mais le contenu c'est mieux.
Qu'en est-il réellement ?
Et bien c'est du DevilDriver.
À peine le temps de niaiser que Dez et son groupe envoient le gros son, après une courte intro calme.
La signature sonore est toujours là, on sait qui on écoute.
Ça tabasse, ça balance, et des fois ça groove un peu même.
Je retrouve tout ce que j'aime chez eux, sans que ça soit un album qui copie les précédents.

Malgré la dénomination de concept par ces auteurs, ce CD ne nous les envoie pas à la gueule comme on pourrait le croire.
DevilDriver tente des choses depuis longtemps, et du coup les essais sont plus dans la subtilité.
Rarement de manière ostentatoire ou qui vient rompre la continuité sonore.
Les nuances, les différences sont là, sans trancher violemment avec le reste, mais assez audible pour qu'on les sente.
On reste dans la globalité : le plaisir d'écoute et la qualité ressentie.
Aucun doute, les deux sont là, au top niveau même !
Et heureusement, c'est le but de la musique !

Bref c'est du très très bon.
Chaussez vos meilleurs équipements d'écoute, et profitez !


[^1]:[Interview de Dez Fafara à propos de l'écriture de l'album](https://www.blabbermouth.net/news/devildriver-is-working-on-staggered-release-double-album-this-will-be-the-record-of-our-career/) (en anglais)

