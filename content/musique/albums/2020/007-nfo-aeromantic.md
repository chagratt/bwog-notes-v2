+++
Categories = ["albums"]
title = "The Night Flight Orchestra : Aeromantic"
date = 2020-03-05T22:29:46+01:00
draft = false
bands = ["The Night Flight Orchestra"]
+++

C'est l'histoire de Métalleux provenant de grands noms qui veulent se marrer un coup.
Après un début de projet qui a semblé peu remarqué en France, voilà qu'ils nous gratifient de leur 6è album.
Attachez vos ceintures, sortez vos plus beaux atours provenant des 80's, appelez votre personnel naviguant et profitez du voyage !

<!--more-->

Ce nouveau CD se trouve être très appréciable.
Parmi les éléments connus, on retrouve les voix enregistrées façon magnétophone ou communication radio en intro (ou outro) de certains morceaux.
L'univers aérien / spatial poussé par le groupe depuis Amber Galactic est également au rendez-vous.
De temps à autre on entend quelques effets de reverb sur le chant, savamment distillés.
Le reste du temps on retrouve l'appui des choristes sur la voix de Björn, qui semble bien mieux dosé qu'avant.
Globalement, le mixage est très bon et la compo rock / psyché / 80's est dynamique et dansante.

Au niveau des « nouveautés » (parlons plutôt d'évolution) on note que le synthé est plus présent qu'avant, ou en tout cas il m'a semblé être bien plus en avant que sur les anciens albums.
Même si le groupe nous en servait avant aussi, là il nous offre des sonorités façon « Tron »
et d'autres qu'on peut rapprocher de ce qu'ont pu faire Daft Punk ainsi que d'autres artistes assimilés ...
Les 80's quoi ! Ça rappelle même certains génériques de vieux dessins animés !

Dans l'ensemble l'album est très festif, malgré certaines chansons sensées être plus tristes / mélancoliques / ... [^1]
Petit bémol toutefois : les oreilles attentives remarqueront des bouts de compo piqués aux albums précédents de temps à autre.

Globalement j'ai passé un très bon moment à l'écoute.
Un petit voyage de l'esprit bienvenu presque en terrain connu, avec même une envie de repousser les frontières pour offrir quelques surprises[^2] !
Foncez, c'est de la bonne !
A moins d'être allergique à la nostalgie.

## Des liens sur le groupe

- [Le site officiel](https://thenightflightorchestra.com/)
- [La page Wikipedia](https://en.wikipedia.org/wiki/The_Night_Flight_Orchestra) (en Anglais)

[^1]: Je cherche encore le mot juste ...
[^2]: Je ne dirai pas plus que ceci : les chansons << Transmissions >> et << Carmencita Seven >> en contiennent une vraiment sympa !

