+++
title = "Archspire : Bleed the Future"
date = 2021-11-26T09:40:21+01:00
bands = ["Archspire"]
+++

Archspire est un groupe de Technical Brutal Death Metal originaire du Canada qui en est déjà à son 4e album.
Je n'avais pas encore écrit sur eux, d'une part parce que la découverte est assez récente pour moi,
mais surtout, parce qu'à chaque fois, c'est la bonne grosse claquasse pour moi.
C'est terrible, j'adore, et donc du coup, dur de prendre du recul pour écrire autre chose que ça.

Avec Bleed the Future donc, sorti le 29 octobre 2021 (après une espèce de hype incroyable, mais pas volée), je m'y frotte enfin.

<!--more-->

Comme pour l'album d'avant (Relentless Mutation), le groupe n'a pas le temps, ça tabasse direct dès la première chanson.
En même temps, on est là pour ça !
Et ça va durer tout au long de l'album.
Vous aviez un petit coup de mou ou bien des cervicales à décoincer ? Lancez l'album à fond !
Effet garanti.

On sent avec Bleed the Future que le groupe cherche encore à dépasser un palier par rapport à ce qu'ils avaient déjà fait.
Toujours aussi brütal, toujours autant de cassures dans la structure des morceaux, toujours autant de growls.
On est pas là pour boire le thé tranquillement, on est là pour en prendre plein les oreilles à un rythme effréné.
Pourtant, il y a ce petit quelque chose de planant en fond, ce petit truc que j'avais déjà remarqué à ma découverte du groupe, et qui est toujours là.

C'est toujours aussi impressionnant et plaisant, de bout en bout, et encore une fois, il n'y a aucun morceau à jeter.
Une chose extrêmement rare je trouve, car je ne pense pas ça souvent.
J'ai habituellement entre un et trois morceaux que j'aime moins par album, quel que soit l'artiste, et là non.
Tout au même niveau.
Je n'arrive pas à départager les morceaux entre eux, c'est juste fou !

Un autre tour de force, c'est qu'Archspire réussit à rendre chaque morceau unique malgré les contraitnes et les clichés imposés par le style proposé.
À la fin de l'album, deux sentiments se dégagent.
En vouloir encore plus car c'est jamais assez, et un coup de fatigue aussi.
C'était tellement intense faut dire !

Si vous aimiez déjà Archspire, vous n'aurez aucune déception, foncez !
Si vous aimez le Brutal Death sous des formes techniques et ne connaissez pas encore, foncez aussi.
Vous connaissez et aimez Allegaeon mais en voulez encore plus ? Foncez.

En bref, vous l'aurez compris : j'adore.
Mais ce n'est pas volé, Archspire est un excellent groupe, et cet album le confirme.

