+++
Categories = ["albums"]
title = "Lamb of God : Lamb of God"
date = 2021-02-25T10:27:23+01:00
bands = ["Lamb of God"]
+++

Groupe poids lourd dans le métal, ce groupe américain nous est revenu avec un 8e album sorti en juin 2020 sobrement éponyme.
C'est pour ça que jusqu'ici vous avez beaucoup lu « Lamb of God » en très peu de temps.

Habituellement, quand je les écoute, ce sont des morceaux piochés un peu au pif et en fond sonore, mais j'ai décidé de bousculer mes habitudes.
Alors, ça donne quoi ?

<!--more-->

Avant de commencer à y répondre, je rappelle que le groupe porte une grosse étiquette « Groove Metal » (entre autres), et je dois dire que je l'entend bien.
Ça me plait. Je peux faire un léger parallèle avec DevilDriver au niveau du son, mais la comparaison s'arrête là.

Fidèle aux morceaux que je laisse filer de temps à autre de manière aléatoire dans mes playlists, le groupe reste reconnaissable et propose des morceaux percutants.
Pas forcément fous d'inventivité, les morceaux sont tout de même bien construits, entrainants, mais surtout puissants.
Vu la construction de certains, je ne peux que m'imaginer qu'ils ont été composés en vue d'en proposer une performance en concert un jour[^1].

À noter qu'à certains moment, le groupe semble vouloir céder à la mode d'incorporer des éléments du djent dans sa compo, mais semble se raviser et les utilise plus comme ponctuation que comme le nouveau gimmick sonore à la mode.

Dans le chant, je remarque quelques variations par rapport à ce que je connais.
Est-ce que ça se place au niveau du mixage ou de la technique employée par Randy Blythe ? Je ne saurais pas le dire.
Cela dit, même si ça tranche avec d'autres albums que je connais, ça donne à l'actuel une certaine continuité entre les morceaux, bien que le rendu fasse un peu moins spécifique et un peu plus générique.
Mais en vrai, je chipote là dessus.

En bref, un bon album à écouter, vous passerez un moment puissant, qui donne la pêche.

[^1]: Ou alors je suis en grave manque de pogos ... Allez savoir.

