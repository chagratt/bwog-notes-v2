+++
Categories = ["albums"]
title = "Gojira : Fortitude"
date = 2021-04-30T15:40:02+02:00
draft = false
bands = ["Gojira"]
+++

J'ai toujours eu une relation compliquée avec Gojira.
Des fois j'adore, des fois je m'ennuie en les écoutant.
Je n'arrive pas non plus à avoir un album phare ou un titre que je place absolument au dessus des autres.
Il faut dire que ce n'est pas le groupe le plus accessible non plus.

Malgré tout, et après un refus personnel et catégorique d'écouter les singles en avance, me voici en train de tenter une écoute de ce dernier album, sorti aujourd'hui même.

<!--more-->

Après un début un peu « meh » qui a duré jusqu'à la moitié du 1er morceau, j'ai pu entrer dans l'univers proposé par l'album.
On y trouve beaucoup de sonorités invitant à l'onirisme, une légèreté qu'on peut qualifier d'aérienne surprenante par rapport à ce que le groupe sait poser comme base de puissance.
Rassurez-vous cependant, cette base revient très rapidement, pour le plus grand plaisir des fans de headbang.
Avec des morceaux de répit.
Ça me change de mes groupes habituels où ça tabasse du début à la fin.
J'ai trouvé une sorte de continuité entre les morceaux.
Je ne trouve pas ça désagréable au contraire, mais je me demande ce que ça va donner lors d'une écoute totalement aléatoire dans une grosse liste de lecture.
Continué donc, avec toutefois une surprise assez étrange en plein milieu de l'album,
en la présence du morceau intitulé Fortitude qui est en fait un prélude au morceau qui suit : The Chant.
Finalement, cette continuité est brisée à la fin.
Comme pour marquer la conclusion, que je trouve d'ailleurs très réussie.

Même si j'ai passé un très bon moment, j'ai noté quelques points négatifs.
Par exemple sur le morceau Another World.
Bien qu'étant une chanson sympa, je la trouve très répétitive.
Il y a très (trop ?) peu de variations et peu de couplets différents.
J'ai eu la désagréable impression de toujours entendre la même phrase pendant 5 minutes.
Dommage.
J'ai retrouvé ce même défaut sur Hold On, mais uniquement sur le chant.
Les parties instrumentales m'ont semblé suffisamment variées.
Enfin sur The Chant qui termine en fondu sur le volume.
J'ai vraiment du mal avec cette manière de terminer un morceau plutôt que de conclure de manière franche.

En bref, un album que j'ai apprécié écouter, mais pour lequel je ne retiendrai pas tout.
Pour la blague, j'ai été voir la liste des singles, et ils font partie des morceaux qui m'ont le moins marqué.
À part Amazonia et Into The Storm que j'ai aussi bien aimé que le reste de l'album.

Ça reste donc du bon. Voire très bon, en fonction de votre degré d'appréciation du groupe.
Si vous les avez déjà écoutés, laissez les singles de côté et essayez le reste de l'album. Sinon, tout d'une traite !
Vous devriez aimer.

