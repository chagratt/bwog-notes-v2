+++
title = "Uada : Crepuscule Natura"
date = 2023-09-27T07:49:28+02:00
bands = ["Uada"]
+++

Après plus d'un an sans écouter de nouvel album, et une pile de choses à chroniquer qui s'allonge,
voici le temps de m'y remettre !

Et pour se relancer, rien de moins que le dernier album de Uada !

<!--more-->

Sorti le 8 septembre 2023, ce nouvel album ne comporte que 5 morceaux.
Pas de panique, ce n'est pas EP : les morceaux sont tous longs.
Il y en a un qui dure 6 minutes 38, un autre 12 minutes.
Les trois autres font plus de 7 minutes.
Moins de morceaux donc, mais plus longs.
Est-ce une bonne chose ?
Découvrons tout ceci ensemble.

Sur chaque chanson, nous avons droit à une petite histoire mélodique qui a bien le temps de se développer.
Un growl plus lourd, plus profond.
Et que donnent les compos par rapport a avant ? Hé bien, cela dépend.
En avant pour un petit tour de chaque piste de cet album !

Sur le premier morceau,
la patte bourrine de Uada est immédiatement reconnaissable.
Puis rapidement, on le sent plus lancinant, trainant, moins death, plus black fatigué,
limite plus d'outre-tombe qu'avant.

Sur le second, l'ambiance change.
On revient à ce que l'on connaissait d'avant,
avec une espèce de dimension cosmique dans l'ambiance ressentie.
Un effet probablement voulu vu la jaquette de l'album.
Un peu comme si Cthulhu en avait assez de dispenser la folie de manière insidieuse, avait décidé d'être bien plus frontal et brutal.
Tout en prenant un peu de repos entre deux grosses claques cosmiques.

Avec le troisième morceau, j'ai eu une impression de froid profond et agressif.
Comme un blizzard qui recouvrirait rapidement une région, avant de se calmer pour donner un espoir,
puis reprendre de plus belle.
En même temps, le morceau est intitulé « The Dark (winter) », ce qui est très adapté je trouve.

Pour le quatrième, j'ai du mal à savoir décrire ce que j'ai ressenti.
Tantôt un vide mélancolique, tantôt une énergie épique.
Cette chanson mélange les ambiances rapidement et d'une manière très efficace.
Cela dit, on est très loin du mélange habituel de Uada.
On dirait presque une expérimentation du groupe,
avec de temps à autre des passages où lorsqu'on les connait on dira immédiatement « ah oui, je connais ! ».
Comme des jalons lors d'un voyage, pour ne pas se perdre.

Dernier morceau, le plus long.
Et celui que j'ai préféré.
Sur les premières secondes, j'ai l'impression de retrouver le Uada des autres albums.
Celui que je connais le plus, et que j'apprécie énormément.
Puis le tempo ralenti.
Le chant se fait plus caverneux, le batteur plus calme.
Pour mieux reprendre ensuite.
Cependant, ce morceau reste dans la continuité de l'album, avec de nombreux changements tout du long, ce qui est une bonne chose vu la longueur de la chanson.
Il n'y a pas de monotonie.
Elle est rapidement brisée après les ponts entre les couplets.
Puis, vers la fin, lorsque l'on commence à trouver ça long, c'est terminé.
Brutalement.
Pas d'outro à rallonge avec les guitares qui continuent de sonner dans le vide ni de fondu qui baisse le volume pendant que les musiciens continuent de jouer.
Seulement quelques notes, un sustain laissé là quelques secondes.
Puis rideau !

Globalement,
nous sommes face à un album que l'on va d'abord trouver moins bourrin que les précédents,
puis qui va se révéler percutant.
Ce n'est pas nécessairement mon préféré du groupe, mais il a largement sa place dans leur discographie.
Et dans mon audiothèque.

