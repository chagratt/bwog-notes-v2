+++
title = "Un artiste que tu adores, pourquoi ?"
date = 2024-11-14T13:33:26+01:00

[comments]
  show = false
  host = "pleroma.chagratt.site"
  id = "a_changer"

+++

En vrai, aucun.
Je n'adore pas d'artiste en tant que tel.
J'aime surtout certaines œuvres plus que l'artiste qui est derrière.

Et là, ça passe par des films ou des musiques, principalement.
Étrangement, pas spécialement de tableaux, de photographiques ou d'objets ...

- [Bloguidien, chaque jour un nouveau sujet pour votre blog](https://bloguidien.fr/)

