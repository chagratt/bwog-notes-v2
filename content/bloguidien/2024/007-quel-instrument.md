+++
title = "De quel instrument de musique aimerait tu jouer ?"
date = 2024-12-20T07:32:11+01:00

[comments]
  show = true
  host = "pleroma.chagratt.site"
  id = "ApEGYSrJQ0PxiaWYEK"

+++

Je pense que c'est la batterie.

À force de me concentrer dessus quand je joue de la basse,
j'ai inconsciemment entrainé mon oreille pour l'écouter plus que les autres instruments,
même lorsque j'écoute de la musique juste pour le plaisir.

Je trouve que c'est un instrument sous-côté,
et à part les pédants avec qui je n'aime pas discuter,
on ne la cite pas assez au quotidien lorsque l'on échange à propos de la musique.
Il n'y a guère qu'avec les batteurs que je peux en parler,
et en dehors du milieu musical que je fréquente,
il n'y a pas assez de gens qui en jouent pour avoir des discussions au quotidien sur ce sujet.

Alors certes c'est un instrument volumineux, difficile à prendre en main,
et qui demande (comme beaucoup en fait, mais bon) énormément de temps pour sortir quelque chose de satisfaisant.

Malgré tout ça, c'est réellement un instrument qui m'attire.
Quand j'écoute _encore et encore et encore_ un morceau que je connais déjà,
je ne peux pas détacher mon écoute pour cibler spécifiquement le jeu sur la batterie.
Je peux même taper sur des tomes et cymbales de temps à autre,
avec un plaisir non dissimulé (et un manque flagrant de connaissances de l'instrument).

Vivement que je puisse en avoir une et prendre des cours pour savoir en jouer.
En plus, ça me fera un second instrument pour quand la basse me gonfle[^1].

- [Bloguidien, chaque jour un nouveau sujet pour votre blog](https://bloguidien.fr/)

P.S: En autre côté positif que j'y vois, tu es encore plus loin/séparé du public lorsque tu es sur scène avec une batterie. :P

[^1]: Vous savez, ce moment où on bute sur un passage et qu'il faut absolument une pause avant d'y retourner.
