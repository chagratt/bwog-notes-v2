+++
title = "Tu peux te rendre dans le domicile de ton toi du futur. Que vas-tu regarder en premier, et pourquoi ?"
date = 2024-11-26T18:28:17+01:00

[comments]
  show = false
  host = "pleroma.chagratt.site"
  id = "a_changer"

+++

## Prompt complet

Tu peux te rendre dans le domicile de ton toi du futur.
Tu ne peux rien toucher, ni laisser aucune trace.
Toutes les portes sont ouvertes, il n'y a personne et tu as tout ton temps.
Que vas-tu regarder en premier, et pourquoi ?

## Réponse

Je pense que je vais aller voir en premier s'il y a un vrai coin musique, et pas juste ma basse posée sur un stand, et l'ampli vite fait accessible.

Pourquoi ?

Parce qu'avoir un _vrai_ coin musique, avec un meilleur ampli que mon Marshall 30 watts,
est quelque chose que j'aimerai vraiment avoir.
Sans aller jusqu'à un studio complet,
au moins de quoi accueillir quelqu'un d'autre pour jouer avec moi,
de quoi passer des backtracks pour jouer par-dessus,
un meilleur ampli pour la basse, voire un en plus pour une guitare de passage
et avoir un autre instrument avec lequel m'amuser (une basse 5 cordes déjà, et soyons fous, une batterie ?)
est quelque chose que j'aimerais beaucoup avoir chez moi dans le futur.

Soyons fous, pourquoi pas une petite table de mixage d'ailleurs et des enceintes pour mieux faire tourner tout ça ?
Le tout relié aussi au PC pour travailler les partitions et les corriger tout en jouant ?
Bon, il va falloir arrêter à un moment, car j'ai dit _pas un studio_, hu hu.

- [Bloguidien, chaque jour un nouveau sujet pour votre blog](https://bloguidien.fr/)

