+++
title = "Quel est le dernier concert que tu es allé voir ? As-tu apprécié ?"
date = 2025-01-06T06:37:29+01:00

[comments]
  show = false
  host = "pleroma.chagratt.site"
  id = "a_changer"

+++

Pour mon dernier concert,
je vais parler en fait de festival.
Mais ça compte quand même, vous verrez.

Ça c'est passé le dimanche 4 aout 2024, et c'était le 3è jour du Sylak à Saint Maurice De Gourdans dans l'Ain, pas très loin de Lyon.

{{< image src="affiche.jpg" caption="Affiche de l'édition 2024. Le chien qui est la mascotte du festival est sur un tricycle dans un couloir comme dans The Shining avec les jumelles au fond. Sur tout le reste du décor, plein de références musicales et pop-culture." >}}

Est-ce que j'ai aimé ? Ohhhhh que oui !

Pourquoi ?
Parce qu'il y avait le combo Septicflesh, Rotting Christ et Behemoth.
(Electric Wizard c'était un autre soir, pas de bol) et surtout plein, plein, **plein** de connaissances avec qui je passe toujours un très bon moment en concert.
Ajoutons à ça une très belle météo (après la pluie torrentielle reçue lors du Plane R Fest, ça fait du bien),
le fait que je n'y sois allé qu'en fin d'après-midi (donc après les heures les plus chaudes),
et la très bonne bière sans alcool à la pression,
cela donne un combo gagnant.

Je suis en train de regarder pour y aller les 3 jours cette année,
même si l'affiche n'a pas encore été révélée.
Ce festival est connu pour la donner très tard.
Mais franchement, pour moins de 100 € les 3 jours à une heure de route de chez moi et masse copines et copains sur place,
je sens que je ne vais pas hésiter longtemps !

- [Bloguidien, chaque jour un nouveau sujet pour votre blog](https://bloguidien.fr/)

