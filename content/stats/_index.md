+++
title = "Statistiques"
date = 2025-01-30
draft = false
+++

Si vous aimez les chiffres et les manipulations foireuses,
en voici un peu.

En résumé, ce site c'est :

{{< blog_stats_pw >}}

{{< image src="stonks-mini.jpg" caption="Stonks ! (ou pas ?)" >}}

Et pour plus de détails, vous pouvez vous amuser à lire tout ce qui suit.

## Bloguidien
{{< blog_stats type="bloguidien" >}}

## Informatique
{{< blog_stats type="informatique" >}}

## Musique
{{< blog_stats type="musique" >}}

## Les mots de la fin

_Ça sert à rien, mais j'ai trouvé ça joli !_

Merci à [Lord](https://lord.re/stats) pour l'idée et le code. :-)
