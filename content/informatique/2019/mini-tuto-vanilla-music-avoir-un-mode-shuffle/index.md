+++
tags = ["android", "logiciel libre"]
title = "Mini tuto Vanilla Music : avoir un mode shuffle"
date = "2019-06-24"
draft = false
+++

La plupart des lecteurs musicaux ont un mode aléatoire, et Vanilla Music ne fait pas exception.
Voici une astuce pour être plutôt du coté d'un mode "shuffle".

<!--more-->

## Un peu de contexte ##

La plupart des lecteurs musicaux ont un mode aléatoire, et Vanilla Music[^1] ne fait pas exception.
Le côté gênant, c'est qu'au cours d'une session d'écoute, un même morceau peut passer plusieurs fois.
Voici une astuce pour être plutôt du coté d'un mode "shuffle"[^2], qui va pouvoir empêcher ça.

## La solution que j'ai trouvée ##

Vanilla Music permet de construire une liste de lecture en fonction de ce qu'on veut : Artiste, Album, Dossier, etc.

Mon astuce consiste à se placer dans la vue "FICHIERS" et à cliquer sur "Tout lire".

{{< image src="tuto_vanilla_01.png" caption="La vue fichier avec le bouton « Tout lire » et la barre de lecture en bas" >}}

Ensuite, il faut faire glisser la barre de lecture vers le haut.
L'écran de la liste de lecture va apparaitre, avec les contrôles : précédent, pause/lecture, suivant, etc.

Il y a deux contrôles en plus des trois que je viens de citer :

- En 1er : Mélanger, ou non la liste
- En dernier : Comment lire la liste

Il faut cliquer une seule fois sur le mélangeur pour l'activer, ce qui va tout mélanger, puis activer le mode "lecture en boucle" sur le dernier bouton.
Après cette manipulation, on a donc ceci :

{{< image src="tuto_vanilla_02.png" caption="Liste de lecture paramétrée pour être en mode « shuffle »" >}}

Remarque sur le bouton de mélange, l'actionner une seconde fois va remélanger la liste, mais en groupant les morceaux (par artiste visiblement), et une 3è fois va désactiver le mode mélangé.

## Le mot de la fin ##

Ce réglage de lecture va être gardé en mémoire lorsque Vanilla Music sera relancé.
A noter que, pour remélanger la liste lors de futures écoutes on peut :

- Repasser par la vue "FICHIERS" et recliquer sur "Tout lire" ; c'est ce que je fais
- Rester sur la liste actuelle, et actionner le bouton de mélange (3 fois, sinon le mélange n'est pas bon)


[^1]: Vanilla Music est disponible sur F-Droid. J'en parle rapidement [dans cet article]({{< ref "applis-alternatives-pour-android.md" >}}).
[^2]: Petite explication des concepts [Random ou shuffle ?](https://sebsauvage.net/links/?40imOA) sur le shaarli de Sebsauvage

