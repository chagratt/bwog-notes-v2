+++
tags = ["aix", "shell"]
title = "AIX + ksh : guide de survie"
date = "2019-02-28"
modified = "2020-11-12"
draft = false
show_toc = "true"
+++

Des fois, on a pas le choix.
Quelques commandes utiles pour avoir un peu de confort, surtout si la version de ksh commence à dater.
<!--more-->

Il m'est arrivé de devoir bosser sur des versions de AIX ne proposant que ksh88.
Voici quelques astuces pour rendre le moment moins désagréable, sachant que les flèches ne sont pas utilisables.

## Mode Emacs

Pour l'activer, taper la commande suivante :

{{< highlight shell >}}
$ set -o emacs
{{< / highlight >}}

### Complétion

{{< keys "Échap" >}} 2 fois.

S'il y a plus d'un résultat possible, rien ne se passera.
Utiliser alors {{< keys "Échap" "=" >}} pour lister les éléments.


### Historique

Commande précédente : {{< keys "Ctrl" "p" >}}

Commande suivante : {{< keys "Ctrl" "n" >}}

### Déplacement

Pour éditer une ligne de commande avant d'appuyer sur Entrée.

{{< keys "Ctrl" "a" >}}, {{< keys "Alt" "a" >}}, {{< keys "Ctrl" "b" >}}, {{< keys "Ctrl" "f" >}}, {{< keys "Alt" "f" >}}, {{< keys "Ctrl" "e" >}} ça fonctionne \o/

### Édition de commande

Effacer la ligne entière : {{< keys "Ctrl" "u" >}} ou {{< keys "Ctrl" "c" >}}

Effacer de curseur à fin de ligne : {{< keys "Ctrl" "k" >}}

Suppr char sous le curseur : {{< keys "Ctrl" "d" >}}

Suppr char avant curseur : {{< keys "Ctrl" "h" >}}

Swap char + suivant : {{< keys "Ctrl" "t" >}}


## Mode vi

J'ai testé le mode vi aussi une fois, mais j'ai du mal avec le manque d'indice visuel sur le mode actuel (commande ou insertion).

Pour l'activer :

{{< highlight shell >}}
$ set -o vi
{{< / highlight >}}

### Complétion

{{< keys "Échap" "backslash" >}}

En clavier AZERTY Français il faut utiliser {{< keys "Échap" "Alt Gr" "backslash" >}}

S'il y a plus d'un résultat possible, rien ne se passera.
Utiliser alors {{< keys "Échap" "=" >}} pour lister les éléments.

### Historique

Commande précédente : {{< keys "k" >}}

Commande suivante : {{< keys "j" >}}

### Déplacement

Se placer en début de ligne : {{< keys "0" >}}

Se placer en fin de ligne : {{< keys "$" >}}

Se déplacer vers le droite : {{< keys "l" >}}

Se déplcer vers la gauche : {{< keys "h" >}}

### Édition de commande

Passer en mode remplacement : {{< keys "R" >}}

Passer en mode insertion (avant le curseur) : {{< keys "i" >}}

Passer en mode ajout (après le curseur) : {{< keys "a" >}}

Suppr char sous le curseur : {{< keys "x" >}}

---

C'est vraiment le minimum syndical, mais c'est suffisant si on a juste besoin d'aller éditer un fichier,
lancer une ou deux commandes pour relancer un service.
Notez qu'il y a quelques éléments communs avec bash.

