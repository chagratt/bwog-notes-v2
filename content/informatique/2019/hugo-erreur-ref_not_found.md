+++
Categories = [""]
tags = ["hugo"]
title = "Erreur REF_NOT_FOUND avec Hugo"
slug = "Erreur REF_NOT_FOUND avec Hugo"
date = 2019-10-15T07:33:37+02:00
draft = false
+++

Des fois, en utilisant le shortcode `ref "un-article.md"`, il arrive que Hugo nous gratifie d'un sympathique

{{< highlight text >}}
ERROR 2019/10/23 08:05:53 [fr] REF_NOT_FOUND: Ref "un-article.md": "/blabla/content/pages/autre-article.md:32:15": page not found
{{< / highlight >}}

Et ce, même si vous l'avez dans votre éditeur, et que vous l'avez déjà enregistré.
Alors, pourquoi ?

Réponse courte : sûrement parce que votre article est encore un brouillon (`draft = true`) ou bien la date dans le préambule est dans le futur.

<!--more-->

Pour développer un peu : Hugo est fainéant (ou malin), et il ne génèrera pas les pages qui n'ont pas lieu d'exister.
Donc, pas les brouillons, et pas les pages datées dans le futur.
Le lien vers lequel la référence pointe n'est donc pas valide.

Pour changer ce comportement, Hugo possède deux option :

- `-D` pour forcer le génération des brouillons
- `-F` pour forcer la génération des éléments avec une date de publication située dans le futur

On peut les utiliser à la génération, ou bien avec `hugo serve`.
Et hop !
C'est résolu !

