+++
Categories = [""]
tags = ["blog"]
title = "Remaniement du theme et des liens"
date = "2019-07-23"
draft = false
+++

Aujourd'hui, j'améliore encore le thème (enfin de mon point de vue) mais je casse aussi des liens, désolé !
<!--more-->

Ça fait un petit moment que je voulais améliorer l'aspect visuel.
C'est chose faite !

Par contre, et là je suis désolé, (comme ça c'est fait une bonne fois pour toutes),
j'en ai aussi profité pour réorganiser aussi les articles, dans un dossier portant leur année de création.
Si vous aviez des favoris, ça les aura cassés, désolé.

Mais les articles sont toujours là, et évidemment, d'autres viendront, ne vous en faites pas.

