+++
tags = ["android"]
title = "Applis alternatives pour Android"
date = "2019-06-21"
lastmod = "2025-03-10"
draft = false
+++

Voici une petite liste de mes applis Android principales.

- F-Droid (Forcément !)
    - avec le dépôt de base,
    - et [IzzyOnDroid](https://apt.izzysoft.de/fdroid/index/info).
- [DNSFilter](https://sebsauvage.net/wiki/doku.php?id=dnsfilter), grace à Sebsauvage
- [Element](https://f-droid.org/en/packages/im.vector.app) pour accéder à Matrix
- [Fossify Launcher](https://apt.izzysoft.de/fdroid/index/apk/org.fossify.home) en launcher
- [Gadgetbridge](https://f-droid.org/en/packages/nodomain.freeyourgadget.gadgetbridge) pour gérer ma [PineTime]({{< ref "010-pinetime" >}})
- [Imagepipe](https://f-droid.org/en/packages/de.kaffeemitkoffein.imagepipe/) pour recadrer, virer les données exif et réduire le poids des images à partager en toute simplicité.
- [KeePassDX](https://f-droid.org/fr/packages/com.kunzisoft.keepass.libre)
- [Magic Earth](https://www.magicearth.com/) Clair, simple, efficace, et surtout il fonctionne hors ligne. Il faut donc télécharger les cartes en avance.
- [Markor](https://f-droid.org/en/packages/net.gsantner.markor/) en outil de prise de notes. Simple, rapide, efficace, permet le markdown. Laisse le soin de la synchronisation à d'autres applications.
- [MPV](https://f-droid.org/en/packages/is.xyz.mpv/) pour lire des vidéos.
- [QKsms](https://f-droid.org/en/packages/com.moez.QKSMS/) en appli par défaut pour les SMS
- [SecScanQR](https://f-droid.org/en/packages/de.t_dankworth.secscanqr/) pour scanner des QR Codes
- [Thumb-key](https://apt.izzysoft.de/fdroid/index/apk/com.dessalines.thumbkey) pour le clavier. Il est bâti sur le même principe que feu MessageEase, avec quelques améliorations de placements, notamment pour le français.
- [Tusky](https://f-droid.org/fr/packages/com.keylesspalace.tusky) pour aller sur mastodon
- [Vanilla music](https://f-droid.org/fr/packages/ch.blinkenlights.android.vanilla/) comme lecteur audio
- Vivaldi pour le navigateur, en remplacement de Firefox (qui n'est hélas pas assez réactif sur mon appareil premier prix).

Pour le reste des applications de base (explorateur de fichiers, agenda, notes, galerie, etc.),
j'utilise celles de base livrées avec /e/OS que je trouve très bien.

Si vous avez un android classique et que vous souhaitez aller plus loin,
j'ai prit quelques notes quand je l'ai fait moi-même pour mon ancien téléphone.

[Ça se passe par ici]({{< ref "007-petite-passe-nettoyage-android.md" >}})

La liste bougera de temps à autre, au gré des installations / remplacements.

