+++
tags = ["shell", "réseaux"]
title = "Tester un flux réseau avec Bash"
date = "2019-07-18"
draft = false
+++

Des fois, il arrive qu'on ait pas d'outils installés sur un serveur tels que telnet ou netcat.
Voici donc une solution à base d'éléments pré-inclus dans Bash.
<!--more-->

Quand on a ni telnet, ni netcat, ni autre, il reste quand même possible de tester un flux réseau directement avec Bash.

{{< highlight shell >}}
$ echo > /dev/tcp/nom ou IP/port
{{< / highlight >}}

Si l'hote est joignable à partir de ces informations, rien ne s'affichera, et le code de retour sera 0.

{{< highlight shell >}}
$ echo $?
0
{{< / highlight >}}

En cas d'erreur, un retour semblable à l'exemple ci-dessous apparaitra :

{{< highlight shell >}}
$ echo > /dev/tcp/nom ou IP/port fermé
bash: connect: Connection refused
bash: /dev/tcp/nom ou IP/port fermé: Connection refused
{{< / highlight >}}

Evidemment, le code de retour sera différent de 0.

{{< highlight shell >}}
$ echo $?
1
{{< / highlight >}}

Remarque, pour tester en UDP, il faut utiliser `/dev/udp/...`.

L'extrait associé de la page de man :

> Bash handles several filenames specially when they are used in redirections, as described in the following table:
>
> (... snip ...)
>
>  `/dev/tcp/host/port`
>
>   If  host is a valid hostname or Internet address, and port is an integer port number or service name, bash attempts
>   to open a TCP connection to the corresponding socket.
>
>  `/dev/udp/host/port`
>
>   If host is a valid hostname or Internet address, and port is an integer port number or service name, bash  attempts
>   to open a UDP connection to the corresponding socket.
>
> A failure to open or create a file causes the redirection to fail.


En cas d'utilisation dans un script, pour ne pas afficher les erreurs, voir [cette réponse sur Super User](https://superuser.com/a/806331).

