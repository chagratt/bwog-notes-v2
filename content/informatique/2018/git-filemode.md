+++
Categories = [""]
tags = ["git"]
title = "Git : ignorer les droits d'un fichier"
date = "2018-07-26"
draft = false
+++


Devant parfois travailler sur Windows en plus de Linux, je me retrouve avec un soucis. Git pense que les droits d'un fichier sont différents par rapport à la valeur qui est enregistrée dans son index. La clef de configuration `core.filemode` permet de changer le comportement.
<!--more-->

La gestion des droits de fichiers est différents entre Linux et Windows, ce n'est un secret pour personne.

Du coup, quand on promène des sources via git entre les deux systèmes, il se peut qu'il y ait une différence 
entre les attributs enregistrés dans l'index, et ceux renvoyés par le système.

## Solution

Passer l'option `core.filemode` à `false` dans la configuration, permet de ne plus avoir de problèmes.

**Attention**, c'est toujours la configuration locale au dépôt qui prime.
Si jamais vous avez placé l'option dans votre configuration globale, mais que le problème persiste, vérifiez la configuration locale du projet.

{{< highlight shell >}}
cd PROJET/
git config -l
# ou bien
less .git/config
{{< / highlight >}}

## Source

[StackOverflow](https://stackoverflow.com/questions/6476513/git-file-permissions-on-windows/6476550#6476550)

Le manuel : `git config --help`, puis chercher « core.filemode »

