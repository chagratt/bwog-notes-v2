+++
Categories = [""]
tags = ["blog"]
title = "Le thème s'assombrit"
date = "2018-02-14"
draft = false
+++

Petit test de thème sombre pour voir sur le long terme si la lecture est plus confortable, surtout en fin de journée.
<!--more-->

Le thème du site devient sombre.
C'est normal. :)

Je voulais tester depuis un moment, pour voir si la lecture était plus agréable, surtout en fin de journée.

Ça fait un moment que j'y pense, car même avec des solutions comme `f.lux` ou `redshift`,
les sites ou programmes à fort contraste sont fatigants pour les yeux à la longue.

Il y aura peut-être des ajustements sur le style, mais plus rien de flagrant.

