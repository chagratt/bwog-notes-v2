+++
Categories = [""]
tags = ["ansible"]
title = "Ansible : enregistrer un dictionnaire dans un fichier yaml local"
date = "2018-11-05"
draft = false
+++

Il est possible d'enregistrer le contenu d'un dictionnaire (en mémoire du playbook en train de s'exécuter) dans un fichier yaml. Ce fichier sera écrit sur la machine maitre.
<!--more-->

Lors de l'exécution d'un playbook, il arrive qu'on utilise un dictionnaire pour garder des informations.
Il est possible de sauvegarder ce dictionnaire au format yaml dans un fichier local à la machine qui exécute le playbook.

Pour le faire, il faut utiliser la directive `local_action`, avec comme arguments :

- `copy`
- `content={{ le_dictionnaire | to_nice_yaml }}`
- `dest=chemin_vers_fichier.yaml`

A noter que si le playbook utilise `become` pour passer root, il faut ajouter à la tâche `become: no`

Exemple :

{{< highlight yaml >}}
---
- name: Get VM info to check compliance with install requirements
  hosts: all
  gather_facts: yes
  become: yes
  become_user: root
  become_method: sudo
  vars_files:
    - keys
  tasks:
    - name: Init env by setting facts
      set_fact:
        vms_stats: {}
        results_files: "{{ playbook_dir }}/reports/yaml"

    # globals checks
    - include: "vm_checks/{{ item }}.yml"
      with_items:
        - playbook_get_facts
        # [...snip...]
        - playbook_get_chage

    # specific to RHEL 6
    - include: "vm_checks/rhel6/{{ item }}.yml"
      with_items:
        - playbook_check_lca
        - playbook_check_nagios
      when: ansible_distribution_major_version|int == 6

    # specific to RHEL 7 (because of systemd)
    - include: "vm_checks/rhel7/{{ item }}.yml"
      with_items:
        - playbook_check_lca
        - playbook_check_nagios
      when: ansible_distribution_major_version|int == 7

    - name: Save execution results
      local_action: copy content={{ vms_stats|to_nice_yaml }} dest={{ results_files }}/{{ ansible_hostname }}_{{ inventory_hostname }}.yaml
      become: no
{{< / highlight >}}


Autre astuce, quand le playbook principal utilise des `include`, les playbooks ainsi appelés peuvent utiliser le dictionnaire global :

{{< highlight yaml >}}
---
- name: Extract default ipv4  from facts
  set_fact:
    vms_stats: >
      {{ vms_stats | combine( {'default_ipv4': ansible_default_ipv4} )}}
{{< / highlight >}}

