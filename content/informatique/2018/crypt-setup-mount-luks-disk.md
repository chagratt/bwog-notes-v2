+++
Categories = [""]
tags = ["luks", "cryptsetup"]
title = "Ouvrir un disque chiffré avec LUKS"
date = "2018-08-19"
draft = false
+++

Rapide résumé pour déverouiller un tel disque : `cryptsetup open` et `mount`. Démonter puis faire `cryptsetup close` une fois fini.
<!--more-->

Pour utiliser un disque chiffré avec LUKS, voici les commandes à utiliser (avec des droits root) :

{{< highlight bash >}}
cryptsetup open /dev/sdb1 volume01
mount /dev/mapper/volume01 /mnt/montage01

# C'est là qu'on bosse

umount /mnt/montage01
cryptsetup close volume01
{{< / highlight >}}

## Sources

[Page du wiki ArchLinux sur le chiffrementde disque](https://wiki.archlinux.org/index.php/Dm-crypt/Device_encryption#Unlocking.2FMapping_LUKS_partitions_with_the_device_mapper)

