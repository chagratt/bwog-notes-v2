+++
Categories = [""]
tags = ["blog"]
title = "Retag des articles"
date = "2018-11-07"
draft = false
+++

Petite passe sur les articles du blog pour simplifier les tags.
<!--more-->

Je fais une petite passe sur les articles du blog pour simplifier les tags pour rester global et sans trop en faire.

Je vais m'astreindre à une nouvelle règle simple pour les futurs tags : techno/soft/protocole.
Si ça croise plusieurs technos / sujets, j'en mettrai donc plusieurs (2 max normalement)

