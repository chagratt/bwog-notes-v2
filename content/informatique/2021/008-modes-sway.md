+++
Tags = ["sway"]
title = "Les modes dans Sway"
date = 2021-07-29T17:27:17+02:00
draft = false
+++

Sway s'utilise avec des raccourcis clavier.
Avec ceux-ci il est possible de lancer le terminal, ouvrir un menu pour lancer des applications, changer d'espace de travail,
bouger les fenêtres, etc.

On peut même en définir de nouveaux pour effectuer bien plus d'actions.
Cependant, comment faire pour éviter que les raccourcis ne se chevauchent ?

Réponse : avec les modes.

<!--more-->

Dans sa conf de base, Sway en propose deux :

- default : celui qui est actif dès son démarrage.
- resize : pour redimensionner les fenêtres. À activer avec {{< keys "Mod" "r" >}}.

Lorsque l'on regarde la configuration, on remarque que ces deux modes utilisent les flèches dans leurs raccourcis,
et pourtant leurs effets sont différents.
C'est grâce aux modes justement.
Les raccourcis deviennent uniquement ceux du mode en cours.
Les autres sont simplement ignorés.

Rien ne nous empêche d'en créer de nouveaux, pour nos propres utilisations.

Pour ce faire, rien de plus simple, il suffit d'ajouter ce squelette dans votre conf Sway :

{{< highlight sh >}}
# déclaration d'un nouveau mode, avec son nom.
mode "NOM_DU_MODE" {
    # un raccourci exclusif au mode
    # + retour au mode par défaut à la fin de l'action
    bindsym TOUCHE(S) UNE_ACTION; mode "default"

    # Entrée et Échap pour quitter ce mode sans effectuer d'action
    bindsym Return mode "default"
    bindsym Escape mode "default"

# fin du bloc décrivant le mode
}
# le raccourci pour activer ce mode
bindsym $mod+LETTRE mode "NOM_DU_MODE"
{{< / highlight >}}

Avec ça en poche, et en m'inspirant de Milka64, je me suis également concocté un mode pour ouvrir mes applis les plus utilisées :

{{< highlight sh >}}
mode "open" {
    bindsym f exec firefox; mode "default"
    bindsym d exec discord; mode "default"
    bindsym e exec element-desktop; mode "default"
    bindsym n exec $term -e /usr/bin/newsboat; mode "default"
    bindsym t exec $term; mode "default"

    # Return to default mode
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+o mode "open"
{{< / highlight >}}

Et hop, plus besoin de passer par le menu pour ces 5 là !
J'ai juste à faire {{< keys "$mod" "o" >}} puis la lettre et ... Magie !

À noter que je me suis même embêté à refaire le raccourci pour lancer un nouveau terminal, mais ça c'est totalement optionnel,
puisqu'il existe dans le mode par défaut.

## Petit bonus rigolo

Avec la ligne suivante :

{{< highlight sh >}}
    bindsym n exec $term -e /usr/bin/newsboat; mode "default"
{{< / highlight >}}

Lorsque le quitte _Newsboat_, le terminal associé est terminé également.
Alors que lorsqu'il est lancé depuis le shell, le terminal reste.
Ça peut être embêtant en fonction de votre utilisation,
mais dans mon cas c'est limite mieux puisque de toute façons ce terminal est placé dans un espace de travail dédié chez moi.

## Et voilà !

À vous maintenant la joie des raccourcis multiples mais sans collision !

Cela dit, multiplier les modes n'est peut-être pas la solution la plus pratique.
En y réfléchissant un petit peu, trois modes (défaut, redimensionner, ouvrir les programmes courants) ça semble suffisant,
mais ça c'est à vous de voir, et de laisser parler vos besoins et votre créativité.

## Liens

- [L'article de Milka64 dans lequel j'ai trouvé et compris les modes](https://blog.0w.tf/sway.html)
- [Le man 5 de Sway (En)](https://man.archlinux.org/man/sway.5.en)

