+++
Tags = ["matériel", "PineTime", "android"]
title = "Ma PineTime, Gadgetbridge et la musique sur android"
date = 2021-11-04T11:20:57+01:00
+++

Après avoir constaté [un machin inexplicable]({{< ref "010-pinetime#le-machin-inexplicable">}}) en ce qui concerne les contrôles musicaux depuis ma PineTime,
j'ai finalement réussi à trouver une piste, voire une solution.

Ce n'était ni à cause de la montre, ni à cause des connexion bluetooth entrantes intempestives.

Le coupable était ... _roulement de tambours_ : Android !
Enfin, surtout la surcouche EMUI dans mon cas, et sa gestion aggressive de la batterie.

<!--more-->

Ce que j'ai constaté ce matin en utilisant ma PineTime pour changer de chanson en marchant, c'est que plus rien ne se passait.
Bizarre, car dans d'autres cas ça fonctionnait bien.
J'ai voulu regarder sur mon téléphone pour comprendre pourquoi, j'ai appuyé sur le bouton, l'écran s'est allumé et PAF ! La chanson a été changée.

Il y avait donc un lien entre la réception de l'ordre et l'état de veille de mon téléphone.

En autorisant Gadgetbridge à ignorer les optimisations de consommation de batterie, j'ai pu résoudre ce soucis.
Les commandes musicales de la montre fonctionnent maintenant parfaitement.
C'est beau !

Je n'ai pas compris plus tôt d'où ça venait, car lors de mes utilisations précédentes, mon téléphone se faisait spammer de demandes d'appairage en bluetooth.
Ces demandes devaient provoquer des notifications et l'allumage de l'écran de manière régulière.
À ces moments Gadgetbridge avait donc certainement la main pour recevoir et traiter les commandes en provenance de la PineTime, et donc réagir.
Avec parfois un peu de délai.
D'où mon ancienne impression de latence dans la réponse.


