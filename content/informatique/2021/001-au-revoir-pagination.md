+++
Tags = ["blog", "hugo"]
title = "Au revoir la pagination"
date = 2021-02-14T11:46:51+01:00
modified = 2021-02-15
draft = false
+++

Voici un titre pas nécessairement explicite qui cache un changement que je trouve intéressant pour le blog : la suppression des liens de pagination.
Partout.
Il n'y en a plus, c'est fini.

Pourquoi ? Comment ?
D'où me vient une idée aussi saugrenue ?

Hier soir, il y a Lord qui tentait des choses pour virer la pagination, car 27 pages, c'est beaucoup trop.
J'ai proposé une pagination par année d'abord (on a vite reconnu que c'était pas génial sur le long terme), 
puis (comme sa question de base) une suppression pure et simple de la pagination à base de X derniers articles et de liens vers les pages de catégories.
Avec son idée d'une page bien présentée qui liste tous ses articles, ça me semblait cohérent.

Pour tester la viabilité de mon idée, je me suis mis à tenter de l'appliquer sur mon blog.
Et voilà.

<!--more-->

Concrètement, ces changements sont :

- Sur la page d'accueil : seulement les 6 derniers articles sont montrés ;
- un petit paragraphe en bas de cette page indique comment retrouver le reste des articles ;
- dans les pages des tags, plus de résumé et de lien « lire la suite » pour les articles. On en vient à la même chose que pour les archives : titre, date, tags ;
- c'est tout.

Au ressenti de navigation, on peut se concentrer sur l'essentiel : les articles les plus récents,
ou bien une navigation plus épurée sur les autres listes.
D'ailleurs, en cas d'une catégorie trop remplie, il y aura moins d'actions à effectuer car tout sera sur une seule page.

Certaines pourront devenir un peu longues, mais vu mon rythme de publication, je pense que j'ai de la marge avant que ça ne devienne invivable.
Et au pire, il restera la page de recherche.

Dans tous les cas, aucun article n'est perdu, et j'ai conservé les liens « article plus récent » et « article plus ancien » en bas des pages.
Aucun lien n'est perdu, aucun article n'est oublié. Facile avec Hugo, vu que je n'ai touché qu'au templates de listes.

Au niveau du site donc, cette idée économise pas mal de génération de fichiers.
Ça ne semble pas être grand-chose, mais en vérité sur les sites/blogs qui deviennent plus gros,
ce sont quelques dizaines de pages qui ne sont potentiellement plus générées.
Alors oui, c'est de l'optimisation façon économie de bouts de chandelles, mais j'aime bien l'idée.
Et accessoirement, je ne suis pas certain qu'il y ait beaucoup de personnes qui iraient se fader toutes les pages en scrollant à chaque fois,
puis en cliquant sur le lien « page précédente ».
La recherche, ou bien la page listant tous les articles est plus intéressante pour ce genre de choses.

Et pour être sûr que Hugo ne se mette plus à générer des pages de pagination inutilement,
il suffit de retirer les références à `.Paginator` et `.Paginate`.

Voilà voilà, bonne navigation épurée. :)

Et si jamais vous trouvez que je peux améliorer des choses, n'hésitez pas à m'en faire part.

_Mise à jour du 15/02:_ Et si vous voulez voir un autre exemple de ce que ça peut donner,
[Lord a mis à jour et expliqué de son côté aussi](https://lord.re/fast-posts/56-refonte-home/).

