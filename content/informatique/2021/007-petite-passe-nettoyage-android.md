+++
Tags = ["android"]
title = "Petite passe de nettoyage de mon Android"
date = 2021-07-15T10:48:07+02:00
draft = false
lastmod = "2021-07-20"
+++

Après m'être constitué une [liste d'applis alternatives]({{< ref "applis-alternatives-pour-android.md" >}}),
j'ai réussi à me motiver à passer par le shell ADB pour désinstaller celles que j'avais désactivées depuis longtemps,
mais aussi pousser un peu plus loin.

<!--more-->

À la base donc, je n'avais fait que désactiver des applis via les paramètres :

- bloc-notes
- chrome
- clavier swiftkey
- drive
- duo
- app d'emails par défaut
- facebook app manager
- gmail
- google
- google play films et séries
- google play musique
- HiCare
- Maps
- Météo
- google photos
- youtube

Et pour d'autres, j'étais passé par les options de masquage de mon launcher :

- agenda
- boite à outils sim
- enregistreur sonore
- galerie d'images de base
- lampe torche (j'ai un raccourci sur l'écran de verouillage)
- app des sms d'origine
- mise à jour système (c'est un raccourci indéboulonnable, y'a moyen via les paramètres d'y aller)
- radio FM
- raccourci vers les téléchargements
- thèmes
- lecteur vidéo d'origine
- lecteur musique d'origine

Après un petit coup d'adb, ça va tout de même un petit peu mieux.

Voici la liste que j'ai utilisée pour la désinstallation :

{{< fold "Applis désinstallées" >}}
{{< highlight text >}}
adb shell pm uninstall --user 0 com.android.calendar
adb shell pm uninstall --user 0 com.android.chrome
adb shell pm uninstall --user 0 com.android.dreams.phototable
adb shell pm uninstall --user 0 com.android.email
adb shell pm uninstall --user 0 com.android.managedprovisioning
adb shell pm uninstall --user 0 com.android.mediacenter
adb shell pm uninstall --user 0 com.android.providers.calendar
adb shell pm uninstall --user 0 com.android.providers.partnerbookmarks
adb shell pm uninstall --user 0 com.android.wallpaper.livepicker
adb shell pm uninstall --user 0 com.example.android.notepad
adb shell pm uninstall --user 0 com.facebook.appmanager
adb shell pm uninstall --user 0 com.facebook.system
adb shell pm uninstall --user 0 com.google.android.apps.docs
adb shell pm uninstall --user 0 com.google.android.apps.maps
adb shell pm uninstall --user 0 com.google.android.apps.photos
adb shell pm uninstall --user 0 com.google.android.apps.tachyon
adb shell pm uninstall --user 0 com.google.android.feedback
adb shell pm uninstall --user 0 com.google.android.gm
adb shell pm uninstall --user 0 com.google.android.googlequicksearchbox
adb shell pm uninstall --user 0 com.google.android.music
adb shell pm uninstall --user 0 com.google.android.printservice.recommendation
adb shell pm uninstall --user 0 com.google.android.videos
adb shell pm uninstall --user 0 com.huawei.android.chr
adb shell pm uninstall --user 0 com.huawei.android.FloatTasks
adb shell pm uninstall --user 0 com.huawei.android.FloatTasks
adb shell pm uninstall --user 0 com.huawei.android.FMRadio
adb shell pm uninstall --user 0 com.huawei.android.hsf
adb shell pm uninstall --user 0 com.huawei.android.mirrorshare
adb shell pm uninstall --user 0 com.huawei.android.thememanager
adb shell pm uninstall --user 0 com.huawei.android.totemweather
adb shell pm uninstall --user 0 com.huawei.android.wfdft
adb shell pm uninstall --user 0 com.huawei.geofence
adb shell pm uninstall --user 0 com.huawei.hidisk
adb shell pm uninstall --user 0 com.huawei.himovie
adb shell pm uninstall --user 0 com.huawei.hwdetectrepair
adb shell pm uninstall --user 0 com.huawei.iaware
adb shell pm uninstall --user 0 com.huawei.ihealth
adb shell pm uninstall --user 0 com.huawei.indexsearch
adb shell pm uninstall --user 0 com.huawei.indexsearch.observer
adb shell pm uninstall --user 0 com.huawei.phoneservice
adb shell pm uninstall --user 0 com.huawei.phoneservice
adb shell pm uninstall --user 0 com.huawei.screenrecorder
adb shell pm uninstall --user 0 com.huawei.vassistant
adb shell pm uninstall --user 0 com.huawei.videoeditor
adb shell pm uninstall --user 0 com.huawei.watch.sync
adb shell pm uninstall --user 0 com.huawei.watch.sync
adb shell pm uninstall --user 0 com.touchtype.swiftkey
{{< / highlight >}}
{{< /fold >}}

Au cas où pour ne pas tout péter, remplacez `uninstall` par `disable-user`.

Un petit redémarrage plus tard et pour le moment, tout va bien.

Comme pour la liste des applis, ici ça bougera de temps à autre, au gré des tests, et aussi si je change de tel un jour.

## Lien utiles

- [Épurer un téléphone Android](https://lord.re/posts/179-epurer-un-tel-android/) sur le blog de Lord
- [Désinstaller des applications système d'Android](https://etienne.depar.is/a-ecrit/Desinstaller-des-applications-systemes-d-android/index.html) sur le blog de Étienne Deparis
- [Une liste que j'ai utilisée pour savoir quoi retirer et quoi laisser (En)](https://forum.xda-developers.com/t/guide-list-of-bloatware-on-emui-safe-to-remove.3700814/)

