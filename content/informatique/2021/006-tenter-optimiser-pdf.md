+++
Tags = ["pdf"]
title = "Tenter d'optimiser des PDF"
date = 2021-06-25T22:42:58+02:00
draft = false
+++

_J'extrais cette petit astuce de la page « En vrac » pour la placer dans son article dédié._

Dans le cas de fichiers trop lourds (généralement en provenance de scanners), on peut tenter de jouer avec `pdf2ps` et `ps2pdf`.

{{< highlight shell >}}
pdf2ps "fichier.pdf" "fichier.ps"
ps2pdf -dPDFSETTINGS=/ebook "fichier.ps" "fichier-opti.pdf"
{{< / highlight >}}

<!--more-->

Et si jamais vous en avez plusieurs à convertir d'un coup :

{{< highlight shell >}}
for fichier in ./*.pdf; do pdf2ps "${fichier}" "${fichier}.ps"; done
for fichier in ./*.ps; do ps2pdf -dPDFSETTINGS=/ebook "${fichier}" "${fichier}-opti.pdf"; done
{{< / highlight >}}

J'ai vraiment bien réduit le poids des fichiers, sans altérer de manière visible la qualité :

{{< fold "le résultat (extrait de ls -lh)" >}}
{{< highlight txt >}}
-r--r--r-- 1 chagratt chagratt 1,8M 22 janv.  2021 'page 1.pdf'
-rw-r--r-- 1 chagratt chagratt 6,5M 22 janv. 10:16 'page 1.pdf.ps'
-rw-r--r-- 1 chagratt chagratt 302K 22 janv. 10:17 'page 1.pdf.ps-opti.pdf'
-r--r--r-- 1 chagratt chagratt 1,7M 22 janv.  2021 'page 2.pdf'
-rw-r--r-- 1 chagratt chagratt 5,6M 22 janv. 10:16 'page 2.pdf.ps'
-rw-r--r-- 1 chagratt chagratt 298K 22 janv. 10:17 'page 2.pdf.ps-opti.pdf'
-r--r--r-- 1 chagratt chagratt 2,0M 22 janv.  2021 'page 3.pdf'
-rw-r--r-- 1 chagratt chagratt 6,7M 22 janv. 10:16 'page 3.pdf.ps'
-rw-r--r-- 1 chagratt chagratt 341K 22 janv. 10:17 'page 3.pdf.ps-opti.pdf'
-r--r--r-- 1 chagratt chagratt 1,8M 22 janv.  2021 'page 4.pdf'
-rw-r--r-- 1 chagratt chagratt 5,9M 22 janv. 10:16 'page 4.pdf.ps'
-rw-r--r-- 1 chagratt chagratt 301K 22 janv. 10:17 'page 4.pdf.ps-opti.pdf'
{{< / highlight >}}
{{< /fold >}}

Forcément, le fichier PS intermédiaire va être bien plus lourd.

{{< fold "Les options de -dPDFSETTINGS" >}}
{{< highlight txt >}}
-dPDFSETTINGS=/screen   (screen-view-only quality, 72 dpi images)
-dPDFSETTINGS=/ebook    (low quality, 150 dpi images)
-dPDFSETTINGS=/printer  (high quality, 300 dpi images)
-dPDFSETTINGS=/prepress (high quality, color preserving, 300 dpi imgs)
-dPDFSETTINGS=/default  (almost identical to /screen)
{{< / highlight >}}
{{< /fold >}}

[Source de l'astuce sur Stackoverflow](https://stackoverflow.com/a/14384178).

