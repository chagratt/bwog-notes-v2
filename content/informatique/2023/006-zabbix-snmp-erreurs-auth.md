+++
Tags = ["zabbix","snmp"]
title = "Zabbix, le SNMP et les erreurs d'authentification"
date = 2023-09-13T18:32:27+02:00
draft = false
modified = 2024-04-24

[comments]
  show = true
  host = "pleroma.chagratt.site"
  id = "AZjLgdtQfcRwx9IZii"

+++

J'ai remarqué un truc intéressant sur le fonctionnement de Zabbix lorsque l'on veut superviser des équipements via SNMP :
il arrive que _tous_ les items SNMP d'un host passent en erreur, avec pour seul retour, un message d'erreur d'authentification.
Même si la configuration semble correcte (sauf sur un).
Ce problème a déjà été rencontré et confirmé par un collègue.

## Le contexte

J'étais en train de créer un nouveau template pour superviser un équipement en SNMPv3.
Je l'ai appliqué à un host.
Dans la liste, il y a un item qui était mal configuré au niveau du « Security Name ».
À partir de là, même les items correctement configurés se sont mis en erreur.

Précisons que cela peut se produire également en cas de mauvaise configuration de n'importe quel autre élément :
protocole d'authentification, de chiffrement, etc.

J'ai pu l'expérimenter avec les versions 4.0.x, 5.0.x et 6.0.x.
J'essaierai de mettre à jour si je constate la même chose sur des versions plus récentes.

## L'origine du problème

Les processus de Zabbix gérant la communication en SNMP avec les équipements mettent en cache les données d'authentification pour un host donné.
À partir de là, il suffit que le processus utilise les données d'un item mal configuré pour que l'erreur se propage à tous les items SNMP de ce host.
Une correction à chaud  sur l'interface web n'y changera rien.

À noter que le problème inverse peut également se produire :
Zabbix peut réussir à remonter les données d'un item mal configuré si le processus a d'abord pioché dans un qui l'est correctement.

## Les solutions

### Avant la version 5.0

Ici, il n'y en a qu'une : il faut stopper le service Zabbix Serveur, puis le relancer.
Ainsi, les processus qui vont s'occuper du SNMP seront tués puis relancés, ce qui videra leur cache, et les forcera à prendre les bons identifiants.

### Version 5.0 et +

Depuis la version 5.0, il y a une nouvelle commande au niveau du _runtime control_ : `snmp_cache_reload` :

```shell
zabbix_server -R snmp_cache_reload
```

Elle permet de recharger le cache des propriétés SNMP de tous les hosts, en le vidant d'abord, sans avoir à redémarrer le service.

[Voyez la doc officielle](https://www.zabbix.com/documentation/current/en/manpages/zabbix_server#runtime-control).

