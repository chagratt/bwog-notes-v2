+++
Tags = ["vim"]
title = "Prise de notes simple avec Vim"
date = 2023-07-27T18:52:44+02:00
modified = 2023-07-30

[comments]
  show = true
  host = "pleroma.chagratt.site"
  id = "AY7vwM9bBo0qouPzVI"

+++

La prise de notes sur ordinateur est un exercice tout à fait personnel qui peut être rapidement complexe.
Cela va d'un simple fichier texte à un gros carnet virtuel, voire un wiki local, habituellement géré par un logiciel spécialisé.

Ces logiciels sont très nombreux, et pour n'en citer que quelques-uns :

- Zim
- Joplin
- Zettlr
- Obsidian
- Votre éditeur de texte favori (avec ou sans plugins)
- OneNote

Au niveau professionnel, j'ai tout utilisé tour à tour, et je dois dire que j'ai maintenant un faible pour Zettlr,
après avoir passé de nombreuses années avec Zim.
Je suis cependant actuellement sur Joplin.
Zettlr refusant de fonctionner sur le PC pro sans que je trouve pourquoi.

Mais chez moi, mes besoins sont bien plus légers.
Utiliser de telles solutions ne me convient donc pas.
J'ai beaucoup utilisé un simple fichier unique, édité avec Vim et lancé par un alias Bash, mais il faut avouer que ça a fini par m'embêter.
Avec le temps j'ai accumulé **beaucoup** de notes et j'ai fini par ne plus m'y retrouver.

## À la recherche d'une solution

Étant un grand utilisateur de Vim, je me suis d'abord penché sur quelques solutions à base de plugins :

- OrgMode
- VimWiki
- Vimboy
- Vim-notes

Cependant j'ai rapidement abandonné.
Au-delà de ne pas avoir envie d'alourdir mon éditeur avec des plugins,
j'ai surtout eu la flemme d'apprendre de nouvelles commandes et une autre manière d'utiliser Vim.

Il me fallait quelque chose de plus simple.

## La solution qui me convient

Comme Vim sait gérer nativement le Mardown et le colore, je me suis naturellemnt dirigé vers ce format pour mes notes.

Au niveau de l'organisation, j'ai un fichier `index.md`, placé dans un dossier dédié que j'ouvre avec mon alias :

```bash
alias notes="$EDITOR ~/MesDocs/Textes/_notes/index.md"
```

Dedans, j'ai simplement réorganisé mon gros fichier en petites sections,
et dès qu'un sujet précis demande plus de deux lignes,
je fais un fichier séparé avec un lien dans l'index.

Pour ouvrir ce fichier (qu'il soit nouveau ou déjà existant), je me suis inspiré de la commande `gf` :
je me place sur le nom du fichier à ouvrir et j'utilise cette nouvelle commande que j'ai ajouté à mon `.vimrc` :

```vim
noremap <Leader>gf :tabnew <cfile><cr>
```

La petite astuce en plus,
pour rechercher des choses dans les différents fichiers avec `:grep` ou `:vimgrep`,
c'est d'utiliser une auto-commande pour se placer directement dans le bon dossier :

```vim
autocmd BufRead,BufNewFile *.md if expand("%:p:h") =~ '/_notes$' | silent! cd %:p:h | endif
```

Ainsi Vim recherchera depuis le dossier des notes,
et pas depuis celui d'où j'ai lancé mon alias.

## Et voilà !

J'ai une utilisation qui _me_ convient, pour un effort minimal.
Le tout sans dénaturer l'outil que j'utilise le plus au quotidien.

Ce ne sera peut-être pas la solution idéale pour vous, mais si ma manière de faire vous aide à en trouver une qui vous inspire, tant mieux.
Sinon, n'hésitez pas à consulter les quelques liens en bas de cet article.

## Liens

Les plugins Vim envisagés :

- [OrgMode](https://github.com/jceb/vim-orgmode)
- [VimWiki](https://vimwiki.github.io/)
- [Vimboy](https://github.com/blinry/vimboy)
- [Vim-notes](https://github.com/xolox/vim-notes)

Les logiciels de prise de notes :

- [Zim](https://zim-wiki.org)
- [Joplin](https://joplinapp.org/)
- [Zettlr](https://www.zettlr.com/)
- [Obsidian](https://obsidian.md/)

Des pistes qui m'ont aidé (en anglais) :

- [Konstantin du dev.to : « Taking notes in vim revisited »](https://dev.to/konstantin/taking-notes-in-vim-revisited-558k)
- [Le blog de Blinry : « Keeping a personal wiki »](https://blinry.org/keeping-a-personal-wiki/)
- [Aide de Vim : la commande gf](https://vimhelp.org/editing.txt.html#gf)

La suite de cette histoire :

- [Épisode 2 : Améliorations de mes prises de notes avec Vim]({{< ref "001-ameliorations-prises-notes-vim.md" >}})

