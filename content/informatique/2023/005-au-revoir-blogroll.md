+++
Tags = ["blog", "openring"]
title = "Au revoir le blogroll !"
date = 2023-09-05T17:23:03+02:00
draft = false

[comments]
  show = true
  host = "pleroma.chagratt.site"
  id = "AZSj5JSiYVEyN4s1Hk"

+++

En 2020, j'ai [ajouté un blogroll en bas des articles]({{< ref "008-arrivee-blogroll.md" >}}).
Je ne partageais que 3 liens (tirés au hasard) alors que j'ai bien plus que ça dans mes flux RSS.
Après 3 ans et avoir remarqué le truc lors de la mise à jour de mon blog, 
j'ai commencé à trouver ça un peu triste de limiter à si peu,
surtout que c'étaient souvent les mêmes qui étaient tirés au sort.

Après avoir vu [Lord supprimer le sien](https://lord.re/recap/49-aout-2023/#blog),
j'ai commencé à me demander si je ne ferais pas la même chose.
En plus, Openring est largement capable de récupérer plus que 3 articles.
Ni une, ni deux, et après de nombreux jours de flemme, voici une nouvelle page toute fraîche pour accueillir l'idée : [Ailleurs]({{< ref "ailleurs/_index.md" >}})

Pour cela, j'ai légèrement changé mon script de récupération :

```shell
#!/usr/bin/env bash
set -euo pipefail

readonly BASEDIR=$(pwd)
readonly OPENRING="${HOME}/MesDocs/Developpement/go/openring"
selected_feeds=""

feeds=( $(awk '/openring/ { print $1 }' "${HOME}/.newsboat/urls") )
nfeeds=${#feeds[@]}

for (( index=0; index<${nfeeds} ; index++))
do
  selected_feeds="-s ${feeds[$index]} $selected_feeds"
done

echo $selected_feeds

${OPENRING}/openring -n ${nfeeds} \
$selected_feeds \
< ${OPENRING}/in-webring.html \
> ${BASEDIR}/layouts/partials/webring.html
```

Comme avant, le fichier `layouts/partials/webring.html` est dans mon gitignore.

Le template `in-webring.html` de openring ressemble à ça, à date d'écriture de cet article :

```go-html-template
{{ range .Articles }}

<article class="card">
  <header>
    <h1><a href="{{ .Link }}">{{ .Title }}</a></h1>
    <p>
            Via <a href="{{.SourceLink}}">{{.SourceTitle}}</a>
            le {{.Date.Format "2006-01-02"}}
    </p>
  </header>
  <p>{{.Summary}}</p>
</article>

{{ end }}
```

Oui, il est beaucoup plus simple car se base sur les templates d'articles du blog.

Ensuite,
une nouvelle section vide pour générer la page dans le dossier `layouts` avec un appel à `{{ partial "webring.html" . }}`,
puis un nouveau lien en en-tête du site,
et le tour est joué.

Ah oui, j'ai tout de même viré les règles CSS qui ne servent plus.
Magie, pas besoin d'en rajouter vu cette structure de page !

Voici donc maintenant à disposition bien plus de liens vers d'autres sites et blogs.
Ceci est encore mieux dans la démarche de partager plus de contenu indépendant.
En guise de conclusion, je vais m'auto-citer, en utilisant le premier paragraphe de l'ancien post :

> Après tout, le but des blogs c'est le partage, alors renvoyer vers d'autres adresses ça fait partie du truc.
> Surtout si, de proche en proche, ça vous fait découvrir d'autres blogs qui vous intéressent.
> Et les blogs indépendants, c'est toujours cool.

