+++
Tags = ["blog"]
title = "Fuuuuuuuuuuuusion !"
date = 2024-11-14T11:12:40+01:00

[comments]
  show = false
  host = "pleroma.chagratt.site"
  id = "a_changer"

+++

Et voilà, les blogs séparés Pintes et Growls et Bwog-Notes n'en forment plus qu'un seul, à un nouvel endroit : Chagratt.site.

(Re)bienvenue !

Je n'ai maintenant plus qu'un seul blog à maintenir et sur lequel publier.
Les articles ont tous été rapatriés, sauf ceux concernant la vie de l'ancien Pintes et Growls.

J'ai essayé de séparer et classer proprement le contenu.
À voir ce que ça va donner dans le temps.
Si vous voyez quelque chose à améliorer, n'hésitez surtout pas !

A bientôt dans de nouvelles publications.

