+++
Categories = [""]
tags = ["python"]
title = "Faire un dépôt/miroir pip local"
date = "2016-04-25"
modified = "2020-02-19"
draft = false
+++

L'outil pip de python offre la possibilité de faire un dépôt local avec la sous-commande `download` et les options `--no-index` couplée à `--find-links`.
<!--more-->

Dans certains cas, on peut avoir besoin de conserver de manière particulière les dépendances de son projet.
Au delà du gel de la version, il y a aussi la question de l'accès, et il arrive que la machine cible soit totalement isolée.

Avec le langage python et plus particulièrement l'outil pip, la marche à suivre pour avoir un dépôt en local est simple :

- écrire un fichier `requirements.txt` contenant les dépendances à récupérer
- créer un dossier spécifique (ex `offline_repo`)
- utiliser la commande suivante pour récupérer les paquets : `pip download ./offline_repo -r requirements.txt`

A partir de là, le dossier `offline_repo` peut être archivé et copié sur la machine qui va avoir besoin de ces modules.
Ne pas oublier le fichier `requirements.txt`.
Les modules n'auront pas été installés sur le système, ce qui le laisse propre.

Pour installer ces dépendances sur la nouvelle machine (à l'aide du dossier précédemment créé), la commande est :

{{< highlight shell >}}
pip install --no-index --find-links=/home/USER/chemin/absolu/offline_repo -r requirements.txt
{{< / highlight >}}

Et voilà !

---

**Élément mis à jour**

La commande pour télécharger les paquets était `pip install --download ...` au moment de la publication.
Mais elle n'est plus valide [depuis fin Mars 2018](https://pip.pypa.io/en/stable/news/#b1-2018-03-31).

Truc marrant, cette façon de faire [était déjà dépréciée](https://pip.pypa.io/en/stable/news/#id293) quand j'ai rédigé cette note,
mais j'avais un environnement avec des versions de python et pip trop vieilles pour m'en rendre compte.

