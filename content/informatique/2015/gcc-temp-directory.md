+++
Categories = [""]
tags = ["gcc"]
title = "GCC et dossier temporaire"
date = "2015-02-03"
draft = false
+++

Par défaut, GCC utilise `/tmp` mais en déclarant `TMPDIR` on peut le changer.
<!--more-->

## Source

[Stack Overflow : /tmp folder and gcc](https://stackoverflow.com/questions/4874735/tmp-folder-and-gcc)
