+++
Categories = [""]
tags = ["jmeter"]
title = "Apache JMeter : ouverture de la catégorie, premières réflexions et liens"
date = "2015-04-23"
draft = false
+++

Ayant déjà dû utiliser Apache JMeter et prévoyant de travailler de nouveau avec dans le futur,
j'inaugure cette catégorie avec quelques liens et premières remarques.
<!--more-->

Pour tester les capacités de montée en charge et de tenue des infrastructures offrant des services web il existe plusieurs outils.
Les plus connus sont sans doute Gatling et Apache JMeter.
J'ai déjà dû utiliser JMeter et il se peut que je doive m'en resservir. 
J'ouvre donc cette nouvelle section qui contiendra mes notes et réflexions à propos de l'utilisation de l'outil.

## Étapes réalisées

- Faire un test de charge simple
- Tester le contenu pour vérifier la validité des réponses
- Faire un test de charge par paliers en mode console (point important pour les tests réels)
- Extraire les résultats avec SQLite

## Premier retour

JMeter est un outil puissant mas difficile à prendre en main.
Heureusement j'ai rapidement trouvé le blog de Milamber qui recense de nombreux articles à propos de JMeter, allant des débuts aux éléments poussés.

D'autres choses importantes sont à prendre en compte lors de l'écriture puis l'exécution de tests.
Tout d'abord il vaut mieux placer quelque temps de pause entre certaines requêtes pour simuler le comportement normal d'un utilisateur.
Ensuite, une fois le scénario correctement affiné, il _faut_ utiliser le mode console. 
Cela permet d'économiser de précieuses ressources et même d'utiliser l'injecteur de manière plus souples.
Enfin, il vaut mieux multiplier les injecteurs avec peu d'utilisateurs virtuels que de charger une seule machine.
Il est trop facile de surcharger la machine qui lancera les tests, ce qui altèrera fortement les résultats.

## Liens

[Les tutoriels JMeter sur MilamberSpace](http://blog.milamberspace.net/index.php/jmeter-pages)

[Apache JMeter](http://jmeter.apache.org/)

[Gatling](http://gatling.io)
