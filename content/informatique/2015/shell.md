+++
Categories = [""]
tags = ["shell"]
title = "Aide mémoire Shell"
date = "2015-05-11"
lastmod = "2024-12-31"
draft = false
show_toc = true
+++

Commandes shell usuelles (ou un peu moins) avec cas d'usage.
<!--more-->

_Attention_ : Je rappelle qu'au quotidien je manipule des systèmes Lignux.
Si je ne l'ai pas marqué explicitement, je n'ai pas testé sur un système UNIX.

Pour des astuces spécifiques à Bash vous pouvez [consulter son aide mémoire dédiée]({{< ref "bash-cheatsheet.md" >}}).

## awk

La commande à maintenant [sa page dédiée ici]({{< ref "shell-awk.md" >}}).

## cp

Pour sauvegarder un dossier en conservant ses attributs :

{{< highlight shell >}}
cp -avr <origine> <destination>
{{< / highlight >}}


_Remarque_ : cette commande est utile pour une copie temporaire en vue de faire un éventuel retour arrière.
Pour une sauvegarde régulière et plus poussée il vaut mieux se tourner vers des outils comme `rsync`.

## fallocate

Disponible dans le paquet `util-linux`, la commande `fallocate`
permet de créer d'un coup un fichier de taille conséquente
en préallouant les blocs

```shell
fallocate -l 10G mon_fichier
```

La taille peut être suffixée : GiB, TiB, PiB, EiB, ZiB et YiB (le « iB » est optionnel).
Lisez le manuel pour plus de détails.

_Remarque_ à priori ne fonctionne pas sur XFS.
Je n'ai pas encore (Novembre 2024) testé.

Il y a également `truncate` au cas où :

```shell
truncate -s 10G mon_fichier
```

## find

Pour rechercher un motif dans un ensemble de fichiers,
il est possible d'utiliser grep à l'aide de l'argument `exec`

{{< highlight shell >}}
find <chemin> -name "<fichiers>" -exec grep "<motif>" {} \; -print
{{< / highlight >}}

La sortie produite ressemblera à

{{< highlight txt >}}
contenu éventuel du grep sur fichier 1
fichier 1 correspondant à -name
fichier 2 correspondant à -name
contenu éventuel du grep sur fichier 3
fichier 3 correspondant à -name
fichier 4 correspondant à -name
contenu éventuel du grep sur fichier N
fichier N correspondant à -name
etc ...
{{< / highlight >}}

En plaçant l'argument `print` avant `exec`, la logique d'affichage s'inverse :
d'abord afficher le nom des fichiers puis le contenu éventuel.

**ATTENTION** Comme il est possible de lancer n'importe quelle commande avec `exec`,
il faut absolument être sûr des fichiers sur lesquels on veut agir.
Pour cela il faut utiliser `-ls` en tout premier essai.

{{< highlight shell >}}
find <chemin> -name "<fichiers>" -ls
{{< / highlight >}}

Il vaut mieux perdre quelques dizaines de minutes à relire une liste de fichiers que de perdre des heures ou des jours à restaurer une sauvegarde !

## grep

Plus efficace que le chainage avec `find` ou `cat`, il est possible d'utiliser `grep` seul :

{{< highlight shell >}}
grep <motif> <options> <fichier(s)>

# exemple :
grep "getLastTag" -rn *
Git/Script.pm:94:        $branch = $this->getLastTag;
Git/Script.pm:197:  my $to   = shift || $self->getLastTag;
MEP.pm:1362:    $h_return->{_info}->{version} = $self->git->getLastTag;
MEP.pm:1450:    my $version_generee = $self->git->getLastTag;

# ou dans un seul fichier :
grep "getLastTag" -n "Git/Script.pm"
Git/Script.pm:94:        $branch = $this->getLastTag;
Git/Script.pm:197:  my $to   = shift || $self->getLastTag;
{{< / highlight >}}

Les options intéressantes sont :

- `n` : afficher les numéros de ligne
- `r` : récursif, lors d'une recherche avec joker
- `i` : au besoin, pour le motif insensible à la casse

Et sinon grep sait aussi compter ! Plus besoin de chaîner à outrance avec `wc` :

{{< highlight shell >}}
netstat -an | grep -c 11211
{{< / highlight >}}

Autre astuce, quand on fait un `ps`, pour ne plus voir grep apparaitre et éviter de devoir rajouter un `| grep -v grep`,
l'astuce consiste à entourer le premier caractère de crochets.

Exemple :

{{< highlight shell >}}
ps -elf | grep [s]uperscript.sh
{{< / highlight >}}

_Remarque_ : Plutôt que de faire ceci, la commande `pgrep` pourrait se révéler plus utile.
[Voyez ce paragraphe]({{< ref "#pgrep" >}}).

## heredoc

Je vous redirige [vers la page dédiée]({{< ref "007-heredoc-variables.md" >}})

## ln

Les liens symboliques sont parfois pratiques et la forme la plus courante de la commande est la suivante :

{{< highlight shell >}}
ln -vs <fichier cible> <nom du lien>
# exemple utilisé pour faire croire à un exécutable qu'une bibliothèque se trouvait dans son répertoire :
ln -vs /usr/lib/libudev.so.1.6.2 ./libudev.so.0
{{< / highlight >}}

L'option `-s` permet de créer le lien de manière symbolique et non physique.

Un cas d'usage plus intéressant consiste à faire des liens des fichiers de configuration utilisateur,
pour les rassembler dans un seul dossier afin de faciliter leur sauvegarde.

Et en moyen mnémotechnique infallible pour retenir l'ordre des arguments, le manuel de FreeBSD[^1] dit :

> An easy rule to remember is that the argument order for ln is the same as for cp(1):
> The first argument needs to exist, the second one is created.

## lvresize

Cette commande faisant partie de la suite d'outils liés à LVM, j'ai créé une page spécifique que vous trouverez ici :
[Aide-mémoire LVM]({{< ref "005-lvm-cheatsheet.md" >}}).

## pgrep

Plutôt que de faire `ps -ef | grep process` voire même `ps -ef | grep [p]rocess` et autres chaînages parfelus, utiliser plutôt la commande `pgrep`.

Voici quelques exemples :

{{< highlight shell >}}
# ps -ef | grep [p]rocess
pgrep -fal process

# ps -ef | grep [p]rocess | awk '{print $1}'
pgrep process
{{< / highlight >}}

Et ne pas hésiter à aller voir le manuel de cette commande.
Mais à noter qu'en fonction du besoin, `ps` n'est pas à oublier non plus.


## ps

Pour trouver les processus bloqués, faire `ps -elfy |grep '^D'`.

L'état `D` correspond à un « uninterruptible sleep ».
C'est moche, mais parfois ça arrive dans la vie d'un processus.
Vous pouvez vous tourner [vers cet article](https://eklitzke.org/uninterruptible-sleep) (en anglais) qui l'expliquera bien mieux que moi.

Pour une vue plus complète des processus (en utilisant le style BSD) :

```bash
ps auwwxf
```

Qui affichera tous les processus en vue arborescente,
avec également la commande utilisée.
En prenant une largeur illimitée à l'affichage (le `ww`).

## read

Fichier complet :

{{< highlight shell >}}
while read ma_var; do
    echo "${ma_var}"
done < "mon_fichier"
{{< / highlight >}}

En séparant les champs de fichier :

{{< highlight shell >}}
while IFS=: read user pass uid gid full home shell
do
    echo -e "$full :\n\
    Pseudo : $user\n\
    UID :\t $uid\n\
    GID :\t $gid\n\
    Home :\t $home\n\
    Shell :\t $shell\n\n"
done < /etc/passwd
{{< / highlight >}}

Et si on ne veut pas tous les champs (la variable "x" contiendra le reste de la ligne):

{{< highlight shell >}}
while IFS=: read user pass uid x
do
    echo -e "Pseudo : $user\n\
    UID :\t $uid\n\  \n"
done < /etc/passwd
{{< / highlight >}}

## rsync

Dans le cadre d'une sauvegarde via SSH, la commande prend la forme suivante :

{{< highlight shell >}}
rsync -zvrt --delete -e ssh /un/chemin/important utilisateur_distant@<serveur>:/chemin/distant/de/destination
{{< / highlight >}}

Les arguments `zvrt` peuvent être remplacés par `auv` si les utilisateurs locaux et distants sont les mêmes.
Dans l'exemple les fichiers tranférés deviendront la propriété de `utilisateur distant`.

## sed

Pour insérer le contenu d'un fichier dans d'autres.
Exemple ici, en insérant le texte d'une licence logicielle en en-tête de scripts, ligne 2.

Mettons un fichier `license` contenant le texte à insérer,
et des scripts `.sh` (dans le même dossier) :

```shell
for f in *.sh; do sed -i -e "1r license" "$f"; done
```

_Remarque_ la version de sed employée est la version GNU, d'où le `-i`.
Avec une autre version de sed, il faudra passer par un fichier intermédiaire :

```shell
for f in *.sh; do sed -e "1r license" "$f" > "$f.tmp"; mv "$f.tmp" "$f"; done
```

## Un peu plus ?

J'ai aussi [des notes diverses à cet endroit](https://chagratt.site/partage/brouillon_notes_formation_linux.pdf).

Elles parlent notamment de `sed`, il y a des choses sur `awk`, sur les REGEX, les descripteurs de fichiers, ...

Il me faudrait les mettre au propre sur le site un jour, mais elles peuvent néanmoins être utiles.


## Remarques

Comme pour la liste de commandes VIM, celle-ci sera mise à jour de temps à autre.

[^1]: [Manuel de `ln` de FreeBSD](https://www.freebsd.org/cgi/man.cgi?ln#EXAMPLES)

