+++
Categories = [""]
tags = ["crunchbang","bunsenlabs"]
title = "Crunchbang Linux : première installation et utilisation"
date = "2014-11-07"
lastmod = "2018-11-07"
draft = false
+++

Rapide retour d'un passage de LinuxMint à Crunchbang sur un vieux PC : plus léger et plus rapide.

_MàJ_ : Projet arrêté. Tournez-vous plutôt vers [BunsenLabs](https://www.bunsenlabs.org/).
<!--more-->
Crunchbang n'est plus (depuis un bon moment d'ailleurs).
La distribution [BunsenLabs](https://www.bunsenlabs.org/) est la continuation communautaire de ce projet.

Je l'ai testée et approuvée l'an dernier pour monter rapidement un PC de dépannage sur une veille bécane.
Vous pouvez vous tourner vers cette distribution sans soucis.

---

Il y a quelques mois j'avais décidé de dépoussiérer un vieux PC portable (eeePC 1001) pour en faire une petite station musicale.
Le tout branché sur des enceintes et contrôlé à partir de mon PC principal avec un partage de bureau et un serveur SSH.

Au début j'y avais installé LinuxMint en version XFCE pour essayer de minimiser les ressources consommées, et le temps d'installation / maintenance.
Cela dit, la bête restait lente, aussi bien au démarrage qu'au temps de réponse ; elle est tout de même largement dépassée aujourd'hui.

Mon utilisation n'était pourtant pas énorme :

- Firefox (et le plugin flash) pour aller sur les sites de streaming musicaux
- mocp via le SSH pour écouter ma bibliothèque musicale
- rarement serveur de fichiers

Récemment quelqu'un m'a parlé de la distribution Crunchbang.
Le site promet une distribution rapide, simple, basée sur Debian et adaptée aux PC récents comme plus vieux.
Ayant enfin eu un peu de temps (et la motivation), j'ai décidé d'essayer.
En repartant de zéro pour avoir un point de comparaison, plutôt que simplement installer et configurer Openbox.

Je suis déjà satisfait et voici les points positifs en vrac :

- Boot et accès au bureau plus rapides qu'avant (Openbox)
- Le script de post-install lancé à la première utilisation (installation d'un serveur SSH est faite à ce point si désiré)

De base, le contenu logiciel est un peu plus allégé, mais le script cité précédemment permet d'ajouter si désiré quelques ajouts au minimum vital.
Pour compléter mon besoin : un serveur Vino, le lecteur en console mocp et voilà.

## En conclusion

Par rapport à LinuxMint, Crunchbang est plus légère.
Plus épurée aussi.
Elle semble bien plus adaptée à mon besoin que LinuxMint.
Je la conseillerai volontiers à ceux qui veulent une distribution de bureau minimale installée en un claquement de doigts.
Pour une première distribution cela dit, elle me semble moins adaptée que Mint.

## Liens

~~[Site officiel de Crunchbang](#)~~ : Site maintenant inopérant, lien désactivé.

[Site officiel de BunsenLabs](https://www.bunsenlabs.org/)
