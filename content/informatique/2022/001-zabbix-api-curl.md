+++
Tags = ["curl","zabbix"]
title = "Interroger l'API Zabbix avec curl"
date = 2022-03-08T16:21:05+01:00
modified = "2024-02-08T09:00:00+01:00"
draft = false
show_toc = true
+++

L'API Zabbix peut-être un bon moyen d'interagir avec son instance.
Pour cela, il y a moult outils permettant de s'en servir.
Mais, si vous n'avez qu'un Lignux sous la main et que vous voulez faire une requête basique,
pas de panique, il vous reste un atout.

Ce joker, c'est la commande `curl`.

<!--more-->

C'est bien connu, `curl` fait tout, et même plus (sauf le café, encore que ...).
Tout ce dont on a besoin ici sera : un shell avec curl, un lien réseau avec l'instance Zabbix,
des identifiants fonctionnels sur cette instance.

## À quoi ça va ressembler

Ce qui est important ici, plus que le contenu des requêtes qu'on passera avec l'option `-d` (et qui sont dispos sur la documentation),
c'est de passer le bon en-tête, et de spécifier la méthode `POST`.
Les options `-H` et `-X` vont nous y aider.

Voici à quoi ressembleront nos appels API :

{{< highlight shell >}}
curl -i -H "Content-Type: application/json-rpc" -X POST \
-d 'Le json de requête' \
http://url_instance_zabbix/api_jsonrpc.php
{{< / highlight >}}

_Remarque_ : `-i` sert à afficher en sortie de commande les en-têtes de réponse HTTP.
Pratique quand on teste la connexion ou de petites requêtes, mais fortement inutile si on veut ensuite traiter le json qu'on recevra.

## S'authentifier

### A l'ancienne

Tout d'abord, envoyer utilisateur et mot de passe pour récupérer un jeton :

```shell
curl -i -H "Content-Type: application/json-rpc" -X POST \
-d '{
    "jsonrpc": "2.0",
    "method": "user.login",
    "params": {
        "username": "login",
        "password": "mot de passe"
        },
    "id": 1,
    "auth": null 
}' \
http://url_instance_zabbix/api_jsonrpc.php
```

**ATTENTION** : Les identifiants pourraient bien se retrouver dans l'historique du shell !

L'API va répondre avec un jeton dans le champ `result`, qu'on pourra dès lors réutiliser :

```json
{ "jsonrpc": "2.0", "result": "0424bd59b807674191e7d77572075f33", "id": 1 }
```

### Via un jeton pré-généré

_À partir de la version 5.4_

Depuis votre compte, sur l'interface ouaibe, allez dans « User settings » puis « API tokens ».
De là vous pourrez générer un ou plusieurs jetons qui seront utilisables par la suite dans le champ `auth` des requêtes.

## Utiliser l'API

Maintenant que l'on a un jeton en poche, y'a plus qu'à,
en le mettant dans le champ `auth` qu'on avait laissé à null avant.

Ici un exemple qui permet de lister les hôtes et leurs templates :

```shell
curl -H "Content-Type: application/json-rpc" -X POST \
-d '{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": ["host","name","hostid"],
        "selectParentTemplates": [
            "templateid",
            "name"
        ]
        },
    "id": 1,
    "auth": "0424bd59b807674191e7d77572075f33" 
' \
http://url_instance_zabbix/api_jsonrpc.php
```

## Petit bonus

Si jamais on veut scripter des appels avec variables, toujours en mode outillage minimal,
il faudra faire attention à l'extrapolation des chaines et des variables.
Attention, ça va être sale !

```shell
while read hstgrp
do
    curl -H "Content-Type: application/json-rpc" -X POST \
    -d "{
    \"jsonrpc\": \"2.0\",
    \"method\": \"hostgroup.create\",
    \"params\": {
        \"name\":\"${hstgrp}\"
        },
    \"id\": 1,
    \"auth\": \"0424bd59b807674191e7d77572075f33\" 
    " \
    http://url_instance_zabbix/api_jsonrpc.php
    sleep 1
done < hosts_groups.txt
```

Pour faire bien plus propre, on peut passer par un Heredoc.
Il y aura toutefois un petit changement dans l'ordre des arguments envoyés à `curl` :

```shell
while read hstgrp
do
    curl -H "Content-Type: application/json-rpc" -X POST http://url_instance_zabbix/api_jsonrpc.php \
    -d @- <<- EOF
	{
	"jsonrpc": "2.0",
	"method": "hostgroup.create",
	"params": {
		"name":"${hstgrp}"
	},
	"id": 1,
	"auth": "0424bd59b807674191e7d77572075f33"
	}
EOF
    sleep 1
done < hosts_groups.txt
```

**Attention** J'ai remarqué qu'avec la version 6.0 de Zabbix,
le json envoyé était considéré comme invalide en faisant une indentation avec des espaces.
Dans l'exemple ci-dessus, l'indentation est faite avec des tabulations.
Je vous renvoie vers [mon article sur le Heredoc]({{< ref "007-heredoc-variables.md" >}}) pour plus de détails si vous le souhaitez.

Sinon, si ça ne vous dérange pas, l'exemple suivant est tout aussi valide, bien que moins lisible :

```shell
while read hstgrp
do
    curl -H "Content-Type: application/json-rpc" -X POST http://url_instance_zabbix/api_jsonrpc.php \
    -d @- << EOF
{
"jsonrpc": "2.0",
"method": "hostgroup.create",
"params": {
"name":"${hstgrp}"
},
"id": 1,
"auth": "0424bd59b807674191e7d77572075f33"
}
EOF
    sleep 1
done < hosts_groups.txt
```

Notez bien que l'opérateur de redirection perd son caractère `-` juste après l'ouverture du Heredoc dans ce cas.

## Le mot de la fin

Évidemment, avec un véritable outil ou langage plus adapté, la vie sera plus facile et les scripts beaucoup plus propres et robustes.
Par exemple en python il y a la bibliothèque PyZabbix,
ou bien les utilitaires python publiés par Zabbix.

Mais, pour dépanner ou des interrogations ponctuelles de l'API, c'est toujours bon à savoir.

## Liens

- [La documentation API de Zabbix](https://www.zabbix.com/documentation/current/en/manual/api)
- [PyZabbix](https://pypi.org/project/pyzabbix/)
- [Article du blog Zabbix sur leur bibliothèque python](https://blog.zabbix.com/python-zabbix-utils/27056/)
- [Les sources de la bibliothèque Python Zabbix](https://github.com/zabbix/python-zabbix-utils)

