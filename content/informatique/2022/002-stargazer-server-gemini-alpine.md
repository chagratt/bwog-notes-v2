+++
Tags = ["gemini", "openrc"]
title = "Stargazer, un server Gemini sur Alpine"
date = 2022-03-21T19:29:51+01:00
draft = false
+++

Alpine Linux est une distribution que je découvre depuis peu, et qui vous sert déjà cet humble espace sur internet.
J'ai aussi voulu y migrer ma capsule Gemini.

Ma seule difficulté a été de creuser dans la doc d'OpenRC (le système d'init et de gestion des services) pour gérer le binaire Stargazer, qui sert la capsule.

<!--more-->

Alors, _difficulté_ j'exagère un peu, il me fallait juste trouver comment faire tourner un simple binaire en service, 
pour pouvoir le démarrer, l'arrêter, le lancer automatiquement au démarrage du système.

En fait, c'est tout bête car OpenRC est bien fichu et donc malin.
Pas besoin de s'embêter à écrire des fonction `start`, `stop` et compagnie :

{{< highlight shell >}}
#!/sbin/openrc-run

name="Gemini server : $SVCNAME"
command="/usr/bin/$SVCNAME"
command_background="true"
command_user="gemini:gemini"
pidfile="/run/${RC_SVCNAME}.pid"

depend() {
    need net
}
{{< / highlight >}}

Tout ça dans un fichier `/etc/init.d/stargazer` et hop !

## Petit complément

Une fois le binaire récupéré et déposé dans `/usr/bin/`, j'ai ajouté un user :

{{< highlight shell >}}
   adduser -D -g 'gemini' gemini
{{< / highlight >}}

je lui ait créé `/var/lib/gemini/certs` puis :

{{< highlight shell >}}
   chown -R gemini:gemini gemini/
{{< / highlight >}}

Et enfin pour finir :

{{< highlight shell >}}
rc-update add stargazer
rc-service stargazer start
{{< / highlight >}}

et PAF ! La capsule Gemini sur un serveur tout beau, tout neuf !

## Liens

- [La page de OpenRC](https://wiki.gentoo.org/wiki/OpenRC)
- [La page de Stargazer](https://git.sr.ht/~zethra/stargazer)

