+++
Tags = ["vim"]
title = "Se passer de gestionnaire de plugin dans Vim"
date = 2022-12-10T10:41:25+01:00
+++

Suite à mon aventure où [j'ai mit en place de quoi coloriser les templates Hugo]({{< ref "009-coloration-syntaxique-templates-hugo-vim" >}}),
je me suis dit que j'allais pouvoir aller plus loin en retirant mon gestionnaire de plugins.

## Pourquoi

Via [Vim-Plug, un gestionnaire léger mais suffisant](https://github.com/junegunn/vim-plug), je n'utilise que deux plugins :

- [DelimitMate](https://www.vim.org/scripts/script.php?script_id=2754)
- [Grammalecte](https://github.com/dpelle/vim-Grammalecte) (et avant [LanguageTool](https://www.vim.org/scripts/script.php?script_id=3223))

Du coup je me suis demandé pourquoi je garderai un gestionnaire pour si peu,
alors que depuis vim 8 on a de quoi installer et isoler correctement des plugins.

## Comment

Vim-Plug dépose les dossiers des plugins dans `.vim/plugged`.
Il suffit de déplacer ces répertoires dans `.vim/pack/<foo>/start/`.
Chez moi c'est : `.vim/pack/common/start/` (je ne me suis pas foulé pour le nom).
J'ai donc juste eu à faire :

```shell
mv .vim/plugged/* .vim/pack/common/start/
```

Ensuite, il faut retirer le code de Plug : `rm .vim/autoload/plug.vim` et retirer l'appel dans son `.vimrc`.
À titre d'exemple, je n'avais que ces lignes à retirer :

```vim
""" plugin management
call plug#begin('~/.vim/plugged')

Plug 'vim-scripts/delimitMate.vim'
Plug 'vim-scripts/LanguageTool'

call plug#end()
```

Facile, et rapide.

## Réflexions

Même si c'est pratique et que je comprends totalement l'utilité des gestionnaires de plugins,
je n'utilise finalement que trop peu de greffons pour que ça ait un intérêt chez moi.
Je n'ai appelé le gestionnaire que 2 fois depuis son installation (en 2017 environ !),
et je ne descends pas ma conf sur tant de machines différentes que ça.

Du coup, vu que c'est limite du code mort qui traine chez moi, alléger mon éditeur me semble être une décision pertinente.
D'autant qu'en identifiant plus clairement les quelques plugins sur mon dépôt dotfiles public,
je pourrais récupérer plus facilement ces éléments,
et le partage est potentiellement plus facile.
Surtout pour copier la conf sur des serveurs pros.
Il y a de nombreux cas où une connexion directe vers l'extérieur n'est pas possible.

Par contre, si ça fonctionne très bien de votre côté et que vous l'utilisez intensivement,
ne vous cassez pas la tête et conservez votre gestionnaire de plug-ins et vos habitudes.
Ça n'aurait aucun intérêt de tout péter, juste pour dire que « les gestionnaires c'est d'la merde », alors que tout fonctionne bien.
Point bonus pour garder votre configuration actuelle si vous avez un environnement spécifique qui vous empêche de passer aux dernières versions de vim.

## Liens

- [(En anglais) un article de Shapeshed sur le même sujet](https://shapeshed.com/vim-packages/)
- [Présentation de Vim-Plug sur NixCraft](https://www.cyberciti.biz/programming/vim-plug-a-beautiful-and-minimalist-vim-plugin-manager-for-unix-and-linux-users/)
- [Page Github de Vim-Plug](https://github.com/junegunn/vim-plug)
- [Page vim.org de DelimitMate](https://www.vim.org/scripts/script.php?script_id=2754)
- [Page github du plugin grammalecte pour vim](https://www.vim.org/scripts/script.php?script_id=2754)
- [Page vim.org de LanguageTool](https://www.vim.org/scripts/script.php?script_id=3223)
- [La doc Vim sur les packages](https://vimhelp.org/repeat.txt.html?#packages)

