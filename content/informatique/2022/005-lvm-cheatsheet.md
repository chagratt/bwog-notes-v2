+++
Tags = ["lvm"]
title = "Aide-mémoire LVM"
date = 2022-06-09T10:46:14+02:00
modified = 2023-05-10
draft = false
show_toc = true
+++

Voici une n-ième page sur internet comportant des commandes liées à LVM.
Et des liens.
Mon petit bloc-notes, ma petite aide-mémoire quoi.

Je ne vous ferai pas l'affront de vous présenter l'outil, ni de vous expliquer son fonctionnement.
[d'autres pages le font bien mieux que moi](https://en.wikipedia.org/wiki/Logical_Volume_Manager_(Linux)).

<!--more-->

Rapide rappel :

- Volume physique (PV, _Physical volume_) : disque, partition, etc. pourvu que ça soit du stockage.
- Groupe de volumes (VG, _Volume group_) : Groupement de PVs vu comme un espace unique et qui va contenir des LV.
- Volume logiques (LV, _Logical volume_) : « partition » virtuelle reposant dans un VG qui va contenir un système de fichiers (à monter dans l'OS pour être utilisable).

## Voir ses volumes

Ainsi que diverses informations utiles comme la taille actuelle, et la taille disponible si on veut agrandir des LV par exemple.

### Physiques

- `pvs`
- `pvscan`
- `pvdisplay`

### Groupes

- `vgs`
- `vgscan`
- `vgdisplay`

### Logiques

- `lvs`
- `lvscan`
- `lvdisplay`

_Remarque_ : Les commandes `*display` prennent en argument optionel le nom du volume pour n'afficher que celui demandé au lieu de tous les afficher.

## Volumes physiques

### Créer : pvcreate

Utiliser `pvcreate` sur le disque (ou une partition)
pour indiquer à LVM que l'on va l'utiliser comme espace de stockage.
Cela marche également pour les disques rajoutés plus tard.

```shell
# un disque entier
pvcreate /dev/sdc
# ou bien une nouvelle partition
pvcreate /dev/sdd3
```

On pourra ensuite l'ajouter à un groupe puis passer aux opérations sur les volumes logiques.

_Remarque_ : lors de l'ajout d'un nouveau disque, pour le détecter à chaud,
il faut d'abord relancer un scan du bus SCSI avant de pouvoir utiliser `pvcreate` :

```shell
echo "- - -" > /sys/class/scsi_host/hostX/scan
```

Avec `X`, le numéro de bus.

### Agrandir : pvresize

Après avoir agrandi une partition, utiliser simplement `pvresize /dev/partition`. Exemple :

```shell
pvresize /dev/sda2
```

Il est possible que le système n'ai pas détecté le changement de taille,
chose courante dans une augmentation à chaud sur une machine virtuelle.
Dans ce cas, il faut forcer le scan SCSI :

```shell
echo "1" > /sys/class/block/sdX/device/rescan
# ou bien, avec les numéros de bus SCSI :
echo "1" > /sys/class/scsi_device/2:0:0:0/device/rescan
```

Pour identifier ses disques :

- `lsblk` pour avoir les étiquettes classiques type `/dev/sdX`.
- `lsblk -S`, `lsscsi` ou `lshw` pour les étiquettes SCSI.

À noter, pour rescanner les disques,
il y a aussi la commande `rescan-scsi-bus.sh --forcerescan` sous Red Hat, via le paquet `sg3_utils`.

## Groupes de volumes

### Création d'un groupe : vgcreate

Avant de créer des volumes logiques,
il faut d'abord créer un groupe en lui donnant un nom puis en indiquant un volume (ou une partition).

```shell
vgcreate nom_du_vg /dev/sdX
```

Ce volume doit d'abord avoir été déclaré comme étant un volume physique via `pvcreate`.

On peut maintenant y créer des volumes logiques, qui seront rattachés à ce groupe,

### Ajouter un volume à un groupe : vgextend

Après avoir créé un nouveau volume physique (via `pvcreate`),
on peut l'attacher à un groupe existant pour en étendre les capacités :

```shell
vgextend nom_du_vg /dev/sdX
```

On peut maintenant y étendre les volumes logiques qui sont rattachés à ce groupe,
ou en créer de nouveaux.

## Volumes logiques

### Créer : lvcreate

Il faut d'abord s'assurer que le volume group contient assez d'espace.
Ensuite

```shell
# En donnant une taille définie
lvcreate -LxxG -n nom_nouveau_lv nom_du_vg
# En prenant toute la place disponible
lvcreate -l 100%FREE -n nom_nouveau_lv nom_du_vg
```

Puis on formate le volume logique :

```shell
mkfs -t [type de FS] /dev/mapper/le_volume_qui_correspond
```

À partir de là le nouveau volume peut être monté, à la main pour tests, puis rajouté dans `/etc/fstab`

### Retailler : lvresize

On peut facilement les étendre à chaud (en tant que root) :

```shell
# Identifier les volumes
df # ou bien : df -h, df -Pm, ...
# Vérifier que l'on dispose d'assez de place dans le groupe :
vgs
# Augmenter la taille du volume
lvresize -r -L +XXG /dev/mapper/le_volume_qui_correspond
# Augmenter le volume en prenant toute la place disponible
lvresize -r -l +100%FREE /dev/mapper/le_volume_qui_correspond
```

L'option `-r` (`--resizefs`) est là pour retailler le volume pour prendre en compte la nouvelle taille immédiatement.
Sans ça, il faut passer manuellement un coup de `resize2fs` (si vous êtes en ext) ou de `xfs_growfs` (dans le cas d'un système de fichiers XFS) après coup sur le volume modifié.

L'argument `-L` attend la taille qu'on veut avoir.
Préfixer avec `+` pour rajouter de l'espace, au lieu d'avoir à calculer à la main la taille totale finale.
Rajouter un suffixe pour l'unité.
Souvent cela sera `G`.

La véritable forme du `-L` est (extrait du manuel de lvresize) :

>       -L|--size [+|-]Size[m|UNIT]
>              Specifies the new size of the LV.  The --size and
>              --extents options are alternate methods of specifying
>              size.  The total number of physical extents used will be
>              greater when redundant data is needed for RAID levels.
>              When the plus + or minus - prefix is used, the value is
>              not an absolute size, but is relative and added or
>              subtracted from the current size.

À noter que `resize2fs` et `xfs_growfs` s'utilisent de la même manière :

```shell
resize2fs /dev/mapper/le_volume_qui_correspond
xfs_growfs /dev/mapper/le_volume_qui_correspond
```

Reportez-vous aux manuels correspondants pour les éventuelles options.
La forme basique suffit dans la plupart des cas.


## Liens

- [La page LVM sur le site de Vincent Liefooghe](https://www.vincentliefooghe.net/content/utilisation-lvm-logical-volume-manager-sous-linux)
- [LVM sur le wiki Arch Linux (en Anglais)](https://wiki.archlinux.org/title/LVM)

Pour approfondir encore plus, le manuel de LVM sera une bonne source.
Sinon le wiki de votre distribution.
Enfin, d'autres sites que vous pourrez trouvez sur internet.

