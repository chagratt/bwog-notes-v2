+++
Tags = ["logiciel libre"]
title = "imv, une visionneuse d'images en ligne de commande"
date = 2022-05-03T08:30:15+02:00
+++

J'aime bien les outils minimalistes.
Depuis longtemps, j'utilisais `ristretto` qui fait très bien son boulot, à un détail près :
elle ne gère pas le webp.
Et se traîne quelques dépendances à des bibliothèques XFCE.

En cherchant de quoi la remplacer, je suis tombé sur quelque chose de sympa, et spécifiquement pensé pour les tilling WM.
Voici donc `imv`.

<!--more-->

Pour l'installation, il y a de grandes chances qu'il y ait un paquet pour votre distribution.
Étape suivante !

Pour l'utiliser, c'est tout simple : on se place dans le dossier que l'on veut, et on lance `imv .` ou bien `imv-folder`.
Il est même possible que votre gestionnaire de fichier propose des intégrations pour la lancer directement (`pcmanfm` le fait nativement par exemple).
Étape suivante !

Une petite fenêtre s'ouvre et la première image s'affiche.
C'est tout.
Son interface est tout simple : il n'y en a pas.
Il faut maintenant jouer avec des raccourcis clavier.
Pas d'étape suivante !

Voici les premiers raccourcis à connaître pour profiter :

- La flèche droite pour passer à la photo suivante
- La flèche gauche pour revenir à la photo précédente
- `gg` pour revenir à la première photo
- `G` pour aller directement à la dernière
- la flèche haut pour zoomer
- la flèche bas pour dézoomer
- `d` pour afficher l'overlay, qui contient quelques infos comme le nombre de photos de la session courante, le zoom, le mode, et sont chemin
- `q` pour quitter

C'est réactif, c'est minimaliste, j'aime bien.
Je l'adopte !

## C'est tout ?

Non, je vous présente juste le minimum vital.
En vrai il y a bien plus de raccourcis et d'option disponibles.
Par exemple lancer un diaporama avec choix de la vitesse de défilement.

Il est même possible de personnaliser tout ceci, mais je vous laisse aller voir la documentation
(que je n'ai pas encore fouillée), car je ne pense pas avoir besoin de bidouiller dans l'immédiat.

La page du projet sur sourcehut contient déjà des petits exemples sympas.

Sinon, vous avez le man, car il y a les pages 1 et 5 fournies avec le paquet. :)

## Liens

- [Page du projet](https://sr.ht/~exec64/imv/)
- [Une liste de visionneuses sur le wiki Arch Linux](https://wiki.archlinux.org/title/List_of_applications/Multimedia#Graphical_image_viewers)

