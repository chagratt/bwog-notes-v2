+++
Tags = ["blog"]
title = "Une page d'accueil plus sobre"
date = 2022-07-15T08:09:20+02:00
+++

Alors qu'avant je présentais un résumé ou un préambule pour chaque billet de ce blog,
je me suis décidé à alléger encore plus la page d'accueil.

Pourquoi ?

Pour la blague je pourrais dire : pour alléger encore plus les données qui vous sont envoyées.
En vrai, parce qu'à force, je me suis rendu compte que ça me forçait à rédiger dans un style peu naturel.
Il me fallait faire une intro que je tentais de rendre accrocheuse ou tout du moins assez pertinente,
puis faire le reste du contenu, qui se devait d'avoir un vrai lien,
vu que le template d'article complet reprend l'intro et le contenu.

J'aurais pu modifier le template pour ne pas afficher l'intro sur la page d'un article,
mais j'aurais toujours été face à ce souci de rédaction qui me laissait une impression étrange.

Du coup, plutôt que de me prendre la tête, je vire les intros !
Je pourrais désormais me concentrer sur le contenu pur.
Nous sommes sur un blog technique après tout, ça me semble donc pertinent comme décision.

Pour ce qui est des articles déjà écrits, je ne les « corrigerait » que si j'y pense lors d'une mise à jour.
Je ne vois pas de raison d'aller m'échiner à altérer le contenu maintenant malgré ce changement.

Par contre, vu que la page est moins longue, j'ai passé la limite de 6 à 10 pour le nombre d'articles présents.
