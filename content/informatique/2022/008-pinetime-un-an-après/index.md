+++
Tags = ["matériel", "PineTime"]
title = "Pinetime, un an après"
date = 2022-10-07T06:36:23+02:00
+++

Il y a un an [je recevais ma PineTime]({{< ref "010-pinetime">}}).
Depuis je la porte quotidiennement.
Je ne la retire que pour le sport, dormir, et me doucher.

Bon, ma vie c'est bien, mais le fonctionnement de la montre depuis que je l'ai, c'est mieux.
Alors, qu'en est-il par rapport à l'an dernier ?

## Le logiciel

Parlons technique, mais pas trop longtemps.

À la réception, j'ai installé le dernier firmware du moment, le 1.6.0.
Aujourd'hui, elle est en 1.10.0.
Et à date de rédaction, la [1.11.0 est en préparation](https://github.com/InfiniTimeOrg/InfiniTime/milestone/11).

Quelles ont été les améliorations ?
Outre des optimisations diverses on trouve principalement :

- le réglage de la date et l'heure (arrivé avec la 1.7.0)
- un appairage en Bluetooth sécurisé
- un simulateur de la montre (pour les devs)
- un écran d'accueil en mode terminal
- la possibilité de désactiver le Bluetooth
- et [d'autres choses que je vous laisse découvrir](https://github.com/InfiniTimeOrg/InfiniTime/releases)

J'utilise pour le moment [une version alternative, qui embarque un écran d'accueil différent](https://github.com/InfiniTimeOrg/InfiniTime/pull/1122).

{{< image src="IMG_20221007_081619.jpg" caption="Cet affichage est un hommage assumé à la Casio G7710" >}}

## Le lien Bluetooth avec Gadgetbridge

J'ai un téléphone qui tourne sous /e/ depuis quelques mois maintenant,
et je n'ai plus [les soucis que j'ai rencontré en fin d'année dernière]({{< ref "011-pintetime-gadgetbridge-musique" >}}).

Malgré les nombreuses améliorations faites par les développeurs du projet InfiniTime,
je pense que le gros des soucis de synchronisation viendra toujours de l'éventuelle surcouche android du téléphone.

## La batterie

Elle tient toujours aussi bien.
Je la recharge tous les 6 ou 8 jours.
Je trouve ça acceptable,
et je suis même impressionné que ça n'ait pas diminué.

Pourvu que ça dure !

## La solidité

Comme dit en début de billet,
je la retire pour le sport.
Pour la moto aussi.
Je la retire aussi avant de faire de la vaisselle.
Elle n'est donc pas soumise à de fortes contraintes ni chocs avec moi.

Je ne peux donc pas dire grand chose là dessus.
Si ce n'est un matin en camping, j'ai oublié de la retirer avant de commencer à prendre ma douche.
Elle n'a pas subit longtemps le jet d'eau, et je l'ai essuyée très rapidement.
Des fois elle prend un peu la pluie aussi.
Jusqu'ici, ça va, elle tient le coup.

Je croise quand même les doigts car [de nombreuses personnes ont un soucis avec l'accéléromètre qui a cessé de fonctionner](https://forum.pine64.org/showthread.php?tid=16182&page=2).
C'est quand même dommage car l'intérêt de la montre tombe alors à zéro.

## Les trucs qui manquent

Par rapport à mon utilisation, il manque l'historique en local des mesures des pas.
Ça rendrait la montre encore plus autonome et de mon point de vue c'est toujours mieux.

En tout cas, [c'est prévu](https://github.com/InfiniTimeOrg/InfiniTime/issues/788).
Ça arrivera un jour.
Peut-être.

## Le réglage que j'ai changé

J'ai tout désactivé pour le réveil de l'écran.
Je ne me sers plus que du bouton.

Le réglage d'appui simple est pratique, mais s'il pleut, que votre manche est mouillée, ou que vous étendez une lessive encore humide,
et bien l'écran s'allumera et la montre sera manipulée et défilera dans les menus.
Pas terrible ...

J'avais quand même testé le réglage « Raise wrist » mais je n'aime pas.

## Pour finir

Pour un objet à ce prix, je suis agréablement surpris qu'il tienne aussi bien.
Les améliorations faites par le projet InfiniTime sont chouettes, et il y en a encore d'autres à venir.
J'espère que l'historique des pas va vite arriver.
Et je touche du bois pour qu'elle tienne encore longtemps.

En tout cas, pour le prix où je l'ai payée, je ne regrette rien.
Sauf peut-être le manque de rechargement solaire.

Petite subtilité maintenant : il y a [une antenne de Pine64 en europe](https://pine64eu.com/).
C'est plus cher[^1], mais les délais de livraison et la garantie sont meilleurs.

## Liens

- [le site de Pine64 (en anglais)](https://www.pine64.org/)
- [le github d'InfiniTime (l'OS de la montre)](https://github.com/InfiniTimeOrg/InfiniTime)
- [la page F-Droid de Gadgetbridge](https://f-droid.org/fr/packages/nodomain.freeyourgadget.gadgetbridge/)
- [Et sinon Pine64 est sur mastodon](https://fosstodon.org/@PINE64)
- [Pine64 europe (sur mastodon aussi)](https://fosstodon.org/@pine64eu)

[^1]: Il faudra comparer avec les frais de port qui sont compris dans le prix sur le site Europe, mais pas sur l'autre.
