+++
Tags = ["hugo"]
title = "Avoir un thème Hugo dans un dossier séparé"
date = 2020-12-21T18:53:38+01:00
draft = false
+++

Avec le générateur de sites Hugo, il est possible de changer l'apparence de son site en mettant tout dans le dossier `layouts/`
ou bien de créer un thème (voire d'en récupérer un sur internet) dans le dossier idoine.

Mais contrairement aux exemples ou instructions d'installation, il n'est pas obligé de résider dans le sous-dossier `themes/` de votre projet.

<!--more-->

En vérité, même si cette info est peu évidente à trouver, c'est très simple à faire.
En plus du paramètre `theme` à mettre dans votre fichier `config.toml`, [il existe aussi](https://gohugo.io/getting-started/configuration/) `themesDir`.

La magie de ce paramètre, en tout cas pour la version 0.79, c'est que ce chemin peut être relatif à votre projet.

Exemple :

{{< highlight toml >}}
themesDir = "/home/chagratt/dev/web/hugo-themes"
# ou bien, et c'est ce que moi j'utilise
themesDir = "../hugo-themes"
theme = "stone-cold-metal"
{{< / highlight >}}

Fini de s'embêter à jouer avec la commande `git submodule`, d'inclure le thème dans le projet ou de l'exclure avec `.gitignore` pour le versionner autrement, etc.

