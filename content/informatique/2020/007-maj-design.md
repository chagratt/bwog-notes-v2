+++
Tags = ["blog"]
title = "Mise à jour du design"
date = 2020-08-31T19:39:15+02:00
draft = false
+++

Hello !
Aujourd'hui je change la palette de couleurs du site.
Des couleurs inspirées par le [thème Nord](https://www.nordtheme.com/).
J'en profite pour me séparer de l'image de fond.
Elle n'apportait pas grand chose et c'est toujours des octets de moins à vous envoyer.

<!--more-->

Et le détail inutile de plus en moins, les 2 icônes qui étaient sur les liens Masto et Gitlab de la page « À propos » disparaissent également.


