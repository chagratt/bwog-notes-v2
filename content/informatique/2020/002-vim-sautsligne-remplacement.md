+++
tags = ["vim"]
title = "Vim et les sauts de ligne en recherche & remplacement"
date = 2020-03-21T14:55:10+01:00
draft = false
+++

Il est courant de vouloir rechercher et remplacer des choses dans Vim.
La commande `:%s/pattern/remplacement/g` est là pour ça et tout se passe habituellement pour le mieux.

Sauf pour le cas particulier des sauts de ligne.
J'en ai eu la surprise (amusante) il y a peu en voulant rajouter des caractères autour des sauts de ligne avec `:%s/\n/,"\n"/g`.
Les sauts de ligne on bien été trouvés et remplacée, mais pas comme je voulais.

<!--more-->

Pour résoudre ce soucis, il faut savoir que Vim traite `\n` et `\r` différemment en fonction de si on recherche,
ou si on lui demande de l'écrire dans le buffer.

En recherche `\n` est bien un saut de ligne et `\r` correspond à un retour chariot (`CR`, `Ctrl-M`, `^M`).
Jusque là rien d'inhabituel.

Au remplacement par contre, `\r` est un saut de ligne et `\n` est un caractère nul (`^@`, `0x00`).

Pour bien remplacer ses sauts de ligne, il convient donc d'écrire (par rapport à mon exemple): `:%s/\n/",\r"/g`.

## Liens pour approfondir techniquement

- [Le post stack overflow sur lequel je suis tombé](https://stackoverflow.com/questions/71323/how-to-replace-a-character-by-a-newline-in-vim),
qui m'a permis d'entrevoir le mécanisme en jeu.
- [Un post stack overflow qui pousse un peu plus loin](https://stackoverflow.com/questions/71417/why-is-r-a-newline-for-vim)
- [Une doc sur Vim qui résume ça](https://vim.fandom.com/wiki/Search_and_replace#Details)

