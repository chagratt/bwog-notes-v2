+++
Tags = ["intel", "archlinux"]
title = "Virer xf86-video-intel et activer le Kernel mode setting avec un Intel HD 620 (sur Core i5 7200U) sous Archlinux"
date = 2021-01-21T10:02:10+01:00
draft = false
+++

Récemment, sur mon Acer Aspire E 15 (E5-575-52G6) j'ai commencé à avoir des soucis graphiques de type freeze total de la machine.
L'image est totalement figée, mais le son continue d'être joué.
Le système ne semble pas totalement sur les genoux, mais aucune entrée clavier n'est possible : le serveur X est donc planté.
Tellement qu'il me faut couper l'alimentation de la machine.
Aie !

Après une rapide investigation (à base de `journalctl -b-1` et de recherches sur les forums de ma distribution),
le coupable est le paquet `xf86-video-intel` dans sa version supérieure à `1:2.99.917+913+g9236c582-1`.

Il y a donc deux solutions : revenir à cette version ou utiliser le modesetting.
J'ai tenté les deux et j'ai finir par garder la seconde.

<!--more-->

Les ennuis ont commencé à partir de la version `1:2.99.917+914+ga511f22c-1`.
Problème rapidement identifié vu que de nombreux sujets ont été créés à ce sujet dès début janvier.
La solution proposée était d'ailleurs simple : retirer le paquet `xf86-video-intel` car le support est maintenant directement dans le noyau.

Bon, pourquoi pas me dis-je.
Ni une, ni deux, je vire le paquet et redémarre.
Échec : sans ce paquet, Xorg ne parvient plus à trouver l'écran et donc plus à afficher quoi que ce soit.

{{< fold "/var/log/Xorg.0.log" >}}
{{< highlight text >}}
(... snip ...)
[     9.431] (II) LoadModule: "intel"
[     9.433] (WW) Warning, couldn't open module intel
[     9.433] (EE) Failed to load module "intel" (module does not exist, 0)
[     9.433] (II) LoadModule: "modesetting"
[     9.434] (II) Loading /usr/lib/xorg/modules/drivers/modesetting_drv.so
[     9.475] (II) Module modesetting: vendor="X.Org Foundation"
[     9.475] 	compiled for 1.20.10, module version = 1.20.10
[     9.475] 	Module class: X.Org Video Driver
[     9.475] 	ABI class: X.Org Video Driver, version 24.1
[     9.475] (II) LoadModule: "fbdev"
[     9.475] (WW) Warning, couldn't open module fbdev
[     9.475] (EE) Failed to load module "fbdev" (module does not exist, 0)
[     9.475] (II) LoadModule: "vesa"
[     9.475] (WW) Warning, couldn't open module vesa
[     9.475] (EE) Failed to load module "vesa" (module does not exist, 0)
[     9.475] (II) modesetting: Driver for Modesetting Kernel Drivers: kms
[     9.475] (WW) xf86OpenConsole: setpgid failed: Operation not permitted
[     9.475] (WW) xf86OpenConsole: setsid failed: Operation not permitted
[     9.476] (EE) open /dev/dri/card0: No such file or directory
[     9.476] (WW) Falling back to old probe method for modesetting
[     9.476] (EE) open /dev/dri/card0: No such file or directory
[     9.476] (EE) Screen 0 deleted because of no matching config section.
[     9.476] (II) UnloadModule: "modesetting"
[     9.476] (EE) Device(s) detected, but none match those in the config file.
[     9.476] (EE) 
Fatal server error:
[     9.476] (EE) no screens found(EE) 
(... snip ...)
[     9.476] (EE) Server terminated with error (1). Closing log file.
{{< / highlight >}}
{{< /fold >}}

## Solution 1 : garder l'ancienne version

Vu que je n'avais pas trop envie de creuser, et que surtout je ne comprenais pas grand chose à ma génération de puce graphique,
ni aux éventuels pilotes liés, je me suis rabattu sur la facilité.
C'est à dire réinstaller la version antérieure :

{{< highlight shell >}}
pacman -U /var/cache/pacman/pkg/xf86-video-intel-1:2.99.917+913+g9236c582-1-x86_64.pkg.tar.zst
{{< /highlight >}}

Et après un redémarrage, tout revient comme avant.

Cela dit, à la prochaine mise à jour, le paquet sera de nouveau changé pour la version fautive, ou une autre encore plus récente,
sans garantie de résolution du soucis[^1].
J'ai donc du rajouter le paquet dans `/etc/pacman.conf`, à la ligne `IgnorePkg=`.

Ça fait le boulot, mais c'est sale.
En plus les mises à jour partielles, c'est pas dans la philosophie de la distribution.

## Solution 2: le modesetting

Après avoir eu du mal à identifier la génération de ma puce, pour confirmer qu'elle était bien prise en charge,
je me suis décidé à retenter de passer au modesetting.

Et en creusant un peu, je vois qu'une initialisation est possible tôt dans le noyau en ajoutant en module le pilote i915.
Ça flaire bon la solution à utiliser car j'ai vu passer cette référence plusieurs fois sur ma machine
en passant les commandes trouvée ça et là pour identifier la puce et le pilote qui est utilisé pour la contrôler.

Pour le faire il faut modifier `/etc/mkinitcpio.conf` pour rajouter :

{{< highlight txt >}}
MODULES=(... i915 ...)
{{< /highlight >}}

Puis reconstruire le noyau :

{{< highlight shell >}}
mkinitcpio -P
{{< /highlight >}}

Et enfin virer les paquets inutiles :

{{< highlight shell >}}
pacman -R xf86-video-vesa
pacman -R xf86-video-intel
{{< /highlight >}}
(oui j'avais encore vesa qui trainait dans un coin)

Au cas où, j'installe quelques paquets en plus pour l'accélération graphique :

{{< highlight shell >}}
pacman -Sy libva-intel-driver lib32-libva-intel-driver libvdpau-va-gl
{{< /highlight >}}

Et on reboote.

Cette fois, Xorg se lance bien.
Je peux toujours lire les vidéos HD, je n'ai aucun ralentissement sur mes programmes courants.
À la limite, un peu de montée en charge du processeur sur de trop grosses vidéos, mais rien d'inquiétant.

## Bilan

Ça fonctionne donc bien.
Ou presque, j'ai des soucis de rafraichissement sur des applis openGL maintenant.
Il va falloir creuser un peu plus donc.
Mais au moins, plus de paquet masqué pour les mises à jour, et une solution préconisée par le plus grand nombre.

Le gros point noir finalement c'est probablement le temps que ça m'a prit pour finalement réaliser qu'il fallait aller jouer avec `mkinitcpio`
avant de virer le pilote.
Il faut dire que même si la doc contient les infos, elle part un peu dans tous les sens à mes yeux.
Le sujet est plus compliqué qu'il n'y parait, surtout quand on a comme moi une puce graphique qui commence à dater un peu
et qu'on y connait rien en terme de générations, noms de code et dates des dites puces.

## Liens

- [Wiki Archlinux : Intel Graphics](https://wiki.archlinux.org/index.php/Intel_graphics)
- [Wiki Archlinux : early KMS start](https://wiki.archlinux.org/index.php/Kernel_mode_setting#Early_KMS_start)
- [Wiki Archlinux : Acer aspire E5-575](https://wiki.archlinux.org/index.php/Acer_Aspire_E5-575) qui m'a permis d'identifier le pilote i915
- [La page Wikipedia](https://en.wikipedia.org/wiki/Kaby_Lake) (en anglais) qui m'a permis d'identifier la génération de ma puce HD 620

[^1]: D'ailleurs, une nouvelle version est sortie depuis, et dans mon cas elle ne corrige pas le soucis.

