+++
tags = ["blog", "apache-httpd"]
title = "Compresser ses pages : Le retour"
date = 2020-09-21T13:01:27+02:00
draft = false
+++

[Dans un ancien article]({{< ref "compresser-ses-pages-c-est-le-bien.md" >}}),
je parlais de la mise en place de la compression sur ce blog, sans entrer dans les détails[^1].

Aujourd'hui, je vais en dire un peu plus, car l'astuce a changé pour le blog.
Et vu que sous Apache, c'est un peu plus chiant que sous Nginx, un petit retour d'expérience pourrait intéresser du monde.

<!--more-->

Après le crash du serveur survenu il y a quelques jours, mon hébergeur a choisi de remplacer Nginx par Apache.
L'astuce glanée sur le blog de Lord concernant Nginx n'étant plus d'actualité, je me suis mis en quête de la manière de procéder avec Apache.

## La solution « officielle » ?

Première étape, la doc du projet sur le module `mod_deflate`.
Bon, à la base, il est surtout prévu pour compresser à la volée.
Mais un peu plus bas, une astuce pour ruser et lui faire distribuer des fichiers déjà compressés.
Honnêtement, elle ne me plaisait pas trop, car il faut renseigner les types de fichiers à la main.
Et faire plusieurs fois les mêmes expressions régulières pour faire un lien entre le nom des fichiers et leur version compressée.
Meh.

Au cas où je teste quand même mais ... Raté.
J'ai sûrement loupé un truc, mais quoi ?
Aucune idée.
Mais je me dis que quelqu'un d'autre a réussi, quelque part.

## La solution artisanale

Après quelques recherches supplémentaires, je suis tombé sur un post daté de 2018 sur le blog de Damien Pobel, qui avait le même problème que moi.
Impeccable !
C'est juste un peu subtil, car par rapport à la solution préconisées par Apache,
il faut aussi jouer avec `mod_mime` en déclarant le type MIME de chaque type de fichier que l'on compresse.
Par contre, on écrit les extensions qu'une seule fois, et ça, ça me plait.
Et on vire aussi la redondance d'expressions.
On conserve néanmoins, et c'est normal, le jeu avec les en-têtes et la règle de réécriture.

Donc, ça me semble plus efficace, c'est plus lisible également, et moins pénible à écrire.
Je teste, ça fonctionne. Youpi !

Voici donc ce que j'ai ajouté à mon virtual host, si jamais vous en avez besoin :

{{< highlight "apache" >}}
  RemoveType .gz
  AddEncoding x-gzip .gz

  # Mapping foo.suffix.gz => Type
  AddType "text/html" .html.gz .htm.gz
  AddType "text/css"  .css.gz
  AddType "text/plain" .txt.gz
  AddType "text/xml" .xml.gz
  AddType "application/javascript" .js.gz
  AddType "application/json" .json.gz
  AddType "image/svg+xml" .svg.gz
  # Depending on what you compress, some more might be needed

  # Proxy configuration
  Header append Vary Accept-Encoding

  RewriteEngine On

  RewriteCond %{HTTP:Accept-Encoding} gzip
  RewriteCond %{DOCUMENT_ROOT}/%{REQUEST_FILENAME}.gz -s
  RewriteRule ^(.*)$ %{DOCUMENT_ROOT}/%{REQUEST_FILENAME}.gz [E=no-gzip,L]
{{< / highlight >}}

Un redémarrage de Apache, et c'est parti !

## Liens

- [La doc apache officielle de mod_deflate](https://httpd.apache.org/docs/current/mod/mod_deflate.html#precompressed)
- [L'astuce trouvée sur le blog de Damien Pobel](https://damien.pobel.fr/post/precompress-brotli-gzip-static-site/) (en Anglais)
- [L'article de Lord](https://lord.re/posts/178-compression-gzip-static-nginx/) si vous avez Nginx


[^1]: J'étais encore sur le service de pages de Framagit à ce moment là, d'où le peu d'informations techniques.
