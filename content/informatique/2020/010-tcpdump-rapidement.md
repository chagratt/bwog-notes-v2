+++
Tags = ["tcpdump", "réseaux"]
title = "Tcpdump, rapidement"
date = 2020-11-09T17:32:58+01:00
modified = 2020-11-17
draft = false
+++

Cet outil permet de faire énormément de choses avec les communications réseaux des machines.
Voici quelques commandes explicatives qui peuvent être utiles pour découvrir si la faute vient de votre serveur.

<!--more-->

En pré-requis, disposez de droits root et installez l'utilitaire.

Pour écouter tout ce qui passe : `tcpdump`.

C'est bourrin et vite illisible.
Heureusement on peut rajouter des filtres :

{{< highlight shell >}}
# Un host en particulier
root@server:~# tcpdump host <ADDR_IP>
# Et un port utilisé par ce client
root@server:~# tcpdump host <ADDR_IP> and src port 161
# Et un protocole
root@server:~# tcpdump host <ADDR_IP> and src port 161 and udp
# Un port en écoute sur mon serveur
root@server:~# tcpdump port 80
{{< / highlight >}}

Rien qu'avec ça vous pourrez vite savoir si la machine dont vous avez passé l'IP arrive à envoyer des données jusqu'à votre serveur.

Pensez quand même à vérifier les règles de votre pare-feu, au cas où.

_Remarque_ : Le dernier exemple qui ne fait que filtrer sur `port 80` et est extrêmement bourrin.
Il est juste là pour illustrer la différence entre `port` (local à votre serveur) et `src port` (le port source de la trame, celui employé par le client).

## Petits bonus

On peut limiter aussi à une seule interface, utile quand on a un serveur avec plusieurs cartes, avec `-i <interface>` :

{{< highlight shell >}}
root@server:~# tcpdump -i eth0 host <ADDR_IP> and udp
root@server:~# tcpdump -i eth0 host <ADDR_IP> and udp and (src port 161 or 162)
{{< / highlight >}}

En option sympa vous pouvez aussi passer `-n` ou `-nn` pour désactiver les résolution de noms et de ports pour accélérer la capture.

Pour aller plus loin, voyez le manuel ou les différents sites qui en parlent déjà.

Notez qu'en entreprise vos recherches peuvent être limitées à cause des proxies car cet outil est associé à la sécurité informatique.

