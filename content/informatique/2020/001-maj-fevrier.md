+++
tags = ["blog"]
title = "Mise à jour de Février"
date = 2020-02-19T18:21:17+01:00
draft = false
+++

Hello !
Petit post pour dire que non, le blog n'est pas mort, et que 2 articles ont été mis à jour.

<!--more-->

Une nouvelle astuce à propos de la commande `ps` est arrivée sur la page [d'aide mémoire shell]({{< ref "/informatique/2015/shell.md">}}),
et une petite mise à jour sur l'utilisation de la commande `pip` (pour faire un dépôt hors ligne) a été faite
[sur la page dédiée à ce sujet]({{< ref "python-pip-offline.md" >}}).


