+++
title = "Recherche"
date = 2020-07-10T19:10:40+02:00
draft = false
+++

Pour rechercher du contenu, c'est par ici :

{{< recherche >}}


-----

C'est la seule page du site qui nécessite du javascript.
Toute la recherche se fait en local sur votre navigateur, rien n'est remonté vers le serveur (vous pouvez vérifier :p ).

La recherche ne fonctionne actuellement que sur les mots complets.
Ce n'est d'ailleurs pas garanti que ça trouve correctement ... je préviens juste au cas où.

L'astuce du joker, avec le caractère `*` en fin de mot semble fonctionner.

Si ça vous intéresse vous pouvez [lire cet article]({{<ref "006-recherche.md">}}),
qui détaille la mise en œuvre.

